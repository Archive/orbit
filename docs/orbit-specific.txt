
This file is intended to capture all ORBit-specific features; i.e.,
features above and beyond the CORBA spec. Bugs should go elsewhere.

Object URLs
-----------
ORBit supports the required URL formats: "IOR", "corbaloc" and "corbaname".
In addition, it supports the optional URL format: "file".
All URL formats other than IOR should be regarded as prototypes;
please report any bugs.


Object Forwarding
-----------------
CORBA specifies protocol mechanisms by which an object may be
forwarded to another location, and specifies aspects that the
client and server must honour. It leaves open exactly when/why
a server responds with a forward location. ORBit has a custom 
mechanism that allows server application code to configure the ORB
with forwarding information. The API is ORBit_ORB_forw_bind().
This is a prototype, and should not be regarded as a stable API.


Fixed IPv4 Port
---------------
As an extention, ORBit defines the command line argument "-ORBIPv4Port"
to specify the IP port that ORBit binds and listens for IIOP requests.
It expects an integer argument.  This is primarily useful in conjunction
with PERSISTENT POAs, or with INS.  See ORBit/test/run_via_ns for
an example.


Random Object Keys (Security Measure)
-------------------------------------
When an application gives an object reference to another ORB,
it is communicated as a "profile", which embeds an "object-key".
Roughly, an object-key identifies a POA and an object within that POA.

As a security measure, ORBit will pad object-keys for objects that
it manages with random digits that form a token. ORBit then requires
that attempts to invoke methods on that object contain the same random
token. Essentially, this random token makes it very difficult for a hacker
to "guess" the name of one of your objects, and then maliciously invoke
methods on it. Instead, you must explicit give out references to your
objects.  All this happens under-the-covers, without any changes required
to client or server, and works with any remote ORB (not just ORBit).

ORBit supports random tokens for both the POA portion and object portion
of the object-key. By default, in TRANSIENT POAs both tokens are each 8
bytes long, while in PERSISTENT POAs they both default to zero length.
This may be configured via the ORBit-specific "Okeyrand" policy.  Such a
policy is created as:

	ORBit_PortableServer_OkeyrandPolicy                                 
	ORBit_PortableServer_POA_create_okeyrand_policy(
			PortableServer_POA poa,
	                CORBA_short poa_rand_len,
			CORBA_short obj_rand_len,
			CORBA_Environment *ev);
The argument {poa_rand_len} specifies the length, in bytes, of the
POA portion, and {obj_rand_len} specifies the length, in bytes, of the
object portion. Due to alignment issues, values that are multiples of
4 are highly recommended (other values may work).

Performance issues. Combining a non-zero {obj_rand_len} with the USER_ID
policy forces the POA to allocate extra memory per-object; futhermore,
this per-object memory cannot be released until the POA itself is released.
Thus is it is strongly suggested that applications use the SYSTEM_ID policy.

The End
-------
