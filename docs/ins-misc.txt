
This discusses misc issues related to the Interoperable Name Service,
otherwise known as INS, as specified by OMG's ptc/99-12-03.

INS specifies behavior in 3 components:
1) The COSNaming module. The behavior of the module is largely
   unchanged from the original spec. Mainly, a new interface,
   COSNaming_NamingContextExt is added, which aids in handling
   the new URL-based object references.
2) Changes to the core ORB with respect to string_to_object and
   resolve_initial_references. Again, these add handing of URL-based
   object references.
3) An implied, but unspecified component that can mediate requests
   associated with corbaloc URLs. I call this a corbaloc server,
   and is discussed below.

corbaloc server
---------------
The corbaloc URL (and by extention the corbaname URL) allows objects to
be referenced by (hostname,tcp_port,object_key).  The spirit of the spec
is that the object_key by an human readable string. In particular, the
predefined ORB object ids (e.g., "InterfaceRepository", "TradingService")
should be valid object_keys. 

The requirements of a corbaloc server are:
1) The user may/must specify at startup the tcp_port that is used.
2) The server must handle IIOP requests in either (or both) of two ways:
   a) Respond with a IIOP LOCATION_FORWARD message. It must be possible
      to configure the corbaloc server (in some proprietary way) with
      the forwarded location.
   b) Invoke appropriate servant, which will send a normal IIOP response.

The crobaloc server could be a small stand-alone program, could be
part of the standard ORB, or part of some larger service such as
the Name Service.

Kennard
