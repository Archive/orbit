Next rewrite needs:
	recursive data types
	sharing marshallers for specific types between multiple uses.
	bounds checking on input data
	OBV? (Need to find C mapping for this)
	Cleaner way of doing optimizations.

The data structures that could be involved are:
	Maybe an AST more useful than IDL_tree...?

	VarContext - represents the "brace set" that temporary variables
	would be allocated in.

	MarshallingContext - represents the thing that static alignment etc.
	would be computed in, and would define the scope of dynamically
	allocated memory.

	MarshallingItem - represents something to be marshalled

	MarshallingInstruction - roughly represents a line of code
	to be output, but specific to our thing.

To start the process, we would need a MarshallingContext and a
MarshallingItem, and VarContext would be created as needed.

Lots of effort in simply being able to refer to a datum correctly - should
simplify this. The reason why genmarshal generates four trees for the same
IDL is because of in/out trees and is_skels. is_skels could definitely be
gotten rid of if the nptrs thing were gotten rid of or simplified.

Not worth it to bother with multi-target-language support - things are too
C-specific to even try.
