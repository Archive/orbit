Smaller projects might be:
  -	Add IIOP 1.2 support
  -	Read flick papers, and improve orbit stub/skel code along
  	those lines. (This can be as big or small a project as
	you want).
  -	Move skel exception handling into POA core (both user and system).
  -	Store objrefs in packed CDR format, rather than unpacked
        profiles. This allows transparent relaying of objrefs.
  -	Reorg typecode-related files.
  -	What is a DynAnyFactory?
  -     Move location forwarding handling from stubs to ORB core.
  -     Combine IOR processing code in orb.c with objref/marshalling code.
  -    	Generate more of the files in src/orb/*.h via PIDL compilation
  	(in particular, IOP and POA). Fix PIDL-related issues discussed
	in change3.txt.
  -    	Expose servant reference counting.
  -	Move death watch stuff out of stubs. Move data out of POAObj
  	and store in seperate hash table.
  -	Add minor code support within ORBit's system exceptions.

Medium projects might be:
  -	Add client-side of fault-tolerant spec to orb core.
  -	Add client-side portions of messaging spec (e.g., timeouts) to core.
  -	Implement a space-efficient IR, that uses USER_ID and DEFAULT_SERVANT
  	POA policies.
  -	Profile memory/time, and improve hot spots.
  -	Get locally-queued requests to work.
  -	Support POA creation on demand and PERSISTENT POAs.

Big projects might be:
  -	Valuetypes.
  -	Interceptors (prob. requires some of the above changes).
  -	Add code-set conversion.
  -	Implement some portion of the security spec., esp. SSL
  	or other transport-level security mechanisms.
