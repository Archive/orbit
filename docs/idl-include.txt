
This note describes various ways that the orbit-idl compiler
supports #include files.

Written by kennard 2000/Jul/14.

Introduction
------------
The CORBA spec. requires that an IDL compiler support the #include
pre-processor directive, and orbit does.

Within the C mapping, an IDL compiler generates header definitions (e.g.,
type declarations, extern statements, macros) and C code (e.g., TC defs,
stub functions, skel functions). The generated C code is then compiled
and turned into object code. 

By default, when orbit-idl encounters a #include statement, the
contents of the included file is processed directly as though
it is part of the file that included it. However, this simple
approach can be problematic, as shown by the next section.


Simple Inheiritance Example
---------------------------
Consider a program built from 3 idl files:

	// base.idl
	typedef long myint;
	interface base {
		void	xxx(myint x);
	};

	// top1.idl
	#include "base.idl"
	interface top1 : base {
		void	yyy(myint x);
	};

	// top2.idl
	#include "base.idl"
	interface top2 : base {
		void	zzz(myint x);
	};

These are then compiled and linked:
	orbit-idl top1.idl
	orbit-idl top2.idl
	gcc top1-stub.c top1-common.c top2-stub.c top2-common.c mycode.c

The above link will fail because the stub function base_xxx() is defined
in both top1-stub.c and top2-stub.c.  Also, TC_myint will be declared
in both top1-common.c and top2-common.c.

There are many solutions to the above problem.

orbit-idl --onlytop solution
----------------------------
This solution uses the --onlytop option to orbit-idl. This option instructs
the compiler to only produce declartions for interfaces defined in the
top-level file, and not for those within include files.

	orbit-idl --onlytop base.idl
	orbit-idl --onlytop top1.idl
	orbit-idl --onlytop top2.idl
	gcc base-stub.c base-common.c \
	    top1-stub.c top1-common.c \
	    top2-stub.c top2-common.c mycode.c

In this case, base_xxx() is only defined within base-stub.c,
and TC_myint is only defined within base-common.c.

other solutions
---------------
ORBit supports two pragmas: inhibit and include_defs. These can
be used to provide very fine control. See documentation elsewhere...

details on --onlytop
--------------------
This option inhibits all definitions within include files. This means
that types from include files are available, but no declarations will
be generated. Also, for every IDL file included within the top level file,
there must be a corresponding .h file.

When the --onlytop options is specified, the location of #include directives
within the IDL file is restricted. Loosely, they can only occur where 
an "interface" definition would be allowable. Thus, you cannot have
a #include include directive in the middle of a struct declaration.
If you need such, don't use the --onlytop directive.
