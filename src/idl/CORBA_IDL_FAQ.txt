                    ?Compilable? CORBA IDL for CORBA 2.3

                         Frequently Asked Questions

Table of Contents

   * What is ?Compilable CORBA IDL??
   * Who should use this IDL? How should it be used?
   * Is it really the same as in the CORBA document?
   * What is in the package?
   * What happened to PIDL? This looks like all IDL.
   * Will the package really compile? I thought you couldn?t compile PIDL.
   * What is the relationship between the IDL in this package and that
     previously published separately by OMG?
   * How is the package organized?



What is ?Compilable CORBA IDL???

?Compilable CORBA IDL? is a package of files that faithfully represents the
nine IDL modules in the CORBA 2.3 document. The nine modules have been
tested for syntactic correctness and completeness (by the OMG elves).

Back to top



Who should use this IDL? How should it be used?

If you are an application writer, you may need up to four of the nine
modules defined in this package. The other five are internal to the ORB and
are used to define protocol interoperability, a subject best left to the
trolls who live under the (interoperability) bridge. If you are an ORB
provider, you and your fellow trolls need all of it.

If you are writing an application, you may need operations and data
definitions from the CORBA, DynamicAny, BiDirPolicy, or PortableServer
modules. You may need their operations and data definitions, but you may not
need the IDL itself. For three of the four, DynamicAny, BiDirPolicy, and
PortableServer, you will not use the IDL directly. That is, you won?t
include their files into your own IDL file in order to use their data
definitions or interfaces in your own IDL declarations. You will use
language specific header files in your application programs for all four of
these modules. Your ORB provider will have crafted those language header
files using the IDL declarations in the modules. In a sense, the ORB
provider has already compiled these four modules (perhaps by hand),
producing stub-equivalents in those header files. You need to know what the
IDL is for all four so that you can use your language?s mapping of the IDL,
but you don?t use the IDL directly for three of them.

The CORBA module is different. It contains definitions of data types and
interfaces that you can use in your own IDL files. OMG has specified that
the name of the file containing the CORBA module should be <orb.idl>. Your
ORB provider makes this file available in the system library. By using a
#include directive to bring it into your IDL file, you can use its data
types and inherit from its interfaces. The ORB provider uses the definitions
in the CORBA document to populate that orb.idl file. This package simply
formalizes the somewhat informal description or the CORBA module in the
CORBA document for the benefit of ORB providers so they can make it
available to you. That means that you don?t have to extract the IDL from
this package to use it yourself. Indeed, you shouldn?t use any file other
than that supplied by your ORB provider because that file also contains any
special restrictions and extensions that your provider has documented.

So, if you?re an application developer, you?ll only use one of the nine IDL
modules directly. You won?t even take that one IDL module from this package.
Application developers don?t need this package except for its documentation
value. ORB providers use it only as a summary of what?s already in the CORBA
document.

Back to top



Is it really the same as in the CORBA document?

To repeat the answer to the first question, this package ?faithfully
represents? the IDL in the CORBA document. That is a very carefully chosen
phrase. If you compare the CORBA module in this package with the IDL in the
CORBA 2.3 document line-by-line, there are differences.

Most of the IDL in the document is presented pedagogically rather than as a
file intended to be compiled. This is especially true for the CORBA module,
which is quite large and which is explained in several chapters.

�        When describing the CORBA::Object interface in Section 4.3, the
statement ?typedef string InterfaceId? appears in the IDL. When describing
the Interface Repository, the same typedef is repeated in Section 10.5.1.
It?s repeated because it?s easier to understand the data types being used
when the definition is close at hand. However, if you were to blindly
extract the IDL fragments and try to compile them exactly as given, an IDL
compiler would complain about multiple definitions of the InterfaceId
identifier. (The elves tried it.) The IDL in the compilable package deletes
this repeated definition, and another like it (Flags), leaving only one of
each.

�        There are ?forward references? to the same interfaces in multiple
chapters. IDL compilers complain about this. (Again, ask the elves or try it
yourself.) You could attempt to avoid forward references by arranging the
interface definitions in a different order. However, Professor Somebody?s
Syntax Graph Theorem says it?s not always possible to avoid a forward
reference; that?s why it?s a part of IDL. A stylistic choice was made for
the compilable IDL to list all interfaces at the beginning of the CORBA
module as forward references. This avoids the need for any deep analysis
about which forward references are needed and, as a by-product, helps
document the CORBA module.

�        Some IDL for the CORBA module (the declaration of NamedValue) is
listed in two chapters, Chapter 4 (as kind of a forward reference needed by
CORBA::Object) and the Chapter 7 (where it is explained in association with
the CORBA::Request object). Because of ordering constraints, the full
declaration was moved from the later chapter?s file (CORBA_Request.idl) to
the earlier chapter?s file (CORBA_Object.idl); since both are CORBA module,
this is a valid change.

�        A few typos were corrected. These corrections will be fed back into
the editing process for the next version of the CORBA document.

These changes ?faithfully represent? the intent of the document. All the IDL
in the package is the same as that listed in the CORBA text, but in
different relative locations than in the text.

Back to top



What is in the package?

The CORBA 2.3 document specifies nine modules. This package contains the IDL
that makes up those nine modules. There are more than nine IDL files in the
package because the definitions of the modules are spread across several
chapters. Some modules are defined entirely within one chapter and are
listed in this package in one file. Pieces of the IOP module appear in two
chapters and pieces of the CORBA module appear in about ten chapters. This
package keeps the chapter structure of the IDL definitions relatively intact
by putting each chapter?s contribution to a module into a separate file and
then using #include on that file. If a chapter defines multiple interfaces,
they are usually put into a separate file. There are 27 IDL files.

This separation of a module into multiple files is strictly for convenience
of reference. It is likely (but not necessary) that an ORB provider would
physically merge the files. Note that the CORBA IDL Style Guide only partly
applies to files that do not contain a full module definition. For example,
file names can?t be taken from the module name (since only one file can have
the module name). File names are part of the CORBA standard, but only for
the first level files, those that contain a module statement.

Back to top



What happened to PIDL? This all looks like IDL.

In truth, PIDL is not really a different language from IDL, at least in the
way it?s currently used in OMG. PIDL is not formally specified but, in
practice, it follows the same syntax as IDL. It?s simply easier that way for
the people inventing the CORBA specifications. When the CORBA document
designates something as PIDL, it means that the normal language mapping
rules don?t necessarily apply. The language mapping may have to do something
special for that construct; on the other hand, the language mapping might
not do something different than usual, in which case it?s useful to have
PIDL be the same as IDL.

Back to top



Will the package really compile? I thought you couldn?t compile PIDL.

Three of the four modules that can be referenced in applications contain
valid IDL and compile without problems, assuming the compiler is CORBA 2.3
compliant. Another four modules also compile because they contain valid IDL,
even though they?re not used directly by applications.

Of the nine modules in the CORBA document, therefore, seven compile without
problems. The two remaining modules, CORBA and PortableServer, are the
subjects of the rest of this section. The discussion gets a little detailed,
so feel free to skip it. Language purists will have fun and you know what
they?re like.

But first, a reminder: only one module (the CORBA module subset in orb.idl)
ever really sees an IDL compiler, so only that module must compile correctly
from a strictly application viewpoint. Having the other eight compile
correctly is nice but, since applications never see them, whether they
compile or not doesn?t affect applications. What is more important is that
that the content of the modules is unambiguous so that their internal use by
an ORB provider will be correct. The easiest way of guaranteeing this is by
making sure the modules are syntactically correct, that is, that they
compile.

The CORBA module contains both PIDL and IDL definitions. The orb.idl file
supplied in this package contains only the IDL part because the orb.idl file
is visible to applications and an IDL compiler must be able to parse it.
Since it has only IDL, that subset of the CORBA module compiles just fine.

So what about the PIDL pieces? Even though PIDL is not formally defined and
it need not be compilable, this package verifies that the PIDL parts of the
CORBA module are syntactically equivalent to valid IDL statements. (Note:
this doesn?t say that the PIDL can be used as IDL, only that it is
syntactically well-formed according to the IDL rules.) Showing this
well-formedness is not difficult because almost all OMG PIDL is equivalent
to OMG IDL. It does take a trick or two, however, because ?almost all? isn?t
?all?.

One other facet of IDL causes compilation problems for conforming compilers.
Both the CORBA and PortableServer modules use the native keyword. This
keyword was designed to be part of IDL as a notational convenience for OMG
documents that need a way of specifying that an entity needs special
language mapping. Languages must map every native type individually. In one
sense, the native keyword is a kind of formalized PIDL. IDL compilers
available to application programmers do not usually allow applications to
use the native keyword to define a data type. If they did allow it, they
wouldn?t know what to generate for stubs and skeletons because the
definition is supplied by a language binding. As a result, most IDL
compilers don?t accept native at all. Only an ORB provider deals with the
native keyword; the provider must supply a language mapping to them by hand.
Since native is part of the IDL in this package, it can only be compiled by
tricking an IDL compiler.

Since native and PIDL aren?t intended to be compiled, a language mapping
specifies what the PIDL corresponds to in a header file. An ORB provider
supplies that language header file with the language mapping results.
Applications include that header file in their language files and get the
definitions they need.

To validate syntax correctness of the PIDL, the package file pseudo_orb.idl
includes all the statements designated as PIDL. This file will compile -
with the aid of two tricks. The first trick is necessary because three of
the objects defined in PIDL have names that collide, intentionally, with IDL
keywords: Object, ValueBase, and context. Feeding the PIDL definition for
the similarly named interfaces directly into an IDL compiler would be an act
of madness. An IDL compiler will reject each of the interfaces so defined.
(The elves tried it and failed - they?ve never claimed to be sane.)

The trick uses the IDL preprocessor to change what the IDL compiler sees.
For the conflict with the Object keyword, for example, the pseudo_orb.idl
file has the statement, ?#define Object OObject?. That hides the keyword
conflict but still allows valid syntax checking. The other keywords are
treated the same way.

The second trick involves the native keyword, which, as explained above, is
not usually accepted by an IDL compiler. Since it appears in CORBA and
PortableServer modules, they use the preprocessor again to change what the
compiler sees. They contain ?#define native typedef long?. After
preprocessing, the compiler sees an innocuous type definition for the
identifier being declared native. That is sufficient to allow the compiler
to continue checking syntax. Since these files don?t generate stubs, the
trickery doesn?t matter.

Back to top



What is the relationship between the IDL in this package and the IDL file
previously published separately by OMG?

The two files have different intended uses.

OMG regularly publishes a summary file that contains all the ?code? parts of
an OMG formal document (CORBA, Services, and Domains). Every formal document
line that is IDL, PIDL, or language code is included in the summary file.
The reason for such a listing is to give readers an electronic version of
the ?code? so that they can extract pieces of it. Readers might want to test
an example, include it in their own code, or use it for documentation
purposes. Having the code lines available electronically means there is less
likelihood of a transcription error.

For CORBA 2.3, the summary file is OMG document ?formal/99-08-01.idl?. Since
that document contains code from multiple languages and often contains the
same code segment multiple times, it can?t possibly compile as given. That
isn?t its purpose.

The creator of this compilable package (the OMG elves) used that summary
file as it was intended, as a starting point for something else. The package
contains extracts from the summary file, leaving behind, for example, the
C++ and Java code lines as well as repeated lines.

Back to top



How is the package organized?

It is a .zip file containing 28 files, 27 of which are IDL and the other is
this file. The .zip file has paths (directories) so you can unzip one of two
ways: into one directory or into four subdirectories. The four
subdirectories are organized as follows.

�        CORBA - Real IDL files for the CORBA module

�        orb.idl - The ?main? file; it has the module statement and has
#include statement to bring in the others.

�        CORBA_Current.idl

�        CORBA_CustomMarshal.idl

�        CORBA_DomainManager.idl

�        CORBA_InterfaceRepository.idl

�        CORBA_Policy.idl

�        CORBA_StandardExceptions.idl

�        CORBA_Stream.idl

�        CORBA_PIDL - PIDL files for the CORBA module

�        pseudo_orb.idl - The ?main? file; it has the module statement and
has #include statement to bring in the others.

�        CORBA_Context.idl

�        CORBA_Interceptor.idl

�        CORBA_NVList.idl

�        CORBA_Object.idl

�        CORBA_ORB.idl

�        CORBA_ORB_init.idl

�        CORBA_Request.idl

�        CORBA_ServerRequest.idl

�        CORBA_ValueBase.idl

�        Interop - Files associated with protocol issues. All but
IOP_DCE.idl are independent modules; that is part of the IOP module.

�        BiDirPolicy.idl

�        CONV_FRAME.IDL

�
GIOP.idl

�
IIOP.idl

�        IOP.idl

�        IOP_DCE.idl

�        SendingContext.idl

�        misc - Files that don?t fit the above categories. Both are
independent modules.

�
DynamicAny.idl

�        PortableServer.idl



   Last revised: November 12, 1999 (by King Elf, of Elf Enterprises, PLC)
