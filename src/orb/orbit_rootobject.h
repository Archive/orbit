/*
 *  Copyright (C) 1998 Richard H. Porter
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifndef _ORBIT_ROOTOBJECT_H_
#define _ORBIT_ROOTOBJECT_H_

typedef enum {
	ORBIT_ROT_NULL,
	ORBIT_ROT_OBJREF,
	/* below here are psuedo-objects */
	ORBIT_ROT_ORB,
	ORBIT_ROT_POA,
	ORBIT_ROT_POAMANAGER,
	ORBIT_ROT_POLICY,
	ORBIT_ROT_TYPECODE,
	ORBIT_ROT_REQUEST,
	ORBIT_ROT_SERVERREQUEST,
	ORBIT_ROT_CONTEXT,
	ORBIT_ROT_DYNANY,
	ORBIT_ROT_POAOBJECT,
	ORBIT_ROT_ORBGROUP,
	ORBIT_ROT_POACURRENT
} ORBit_RootObject_Type;


typedef void (ORBit_RelFnc)(gpointer obj);
typedef void (ORBit_FreeFnc)(gpointer obj);
struct ORBit_RootObject_Interface_struct
{
	ORBit_RootObject_Type	type;
	ORBit_FreeFnc*		free_fnc;
};

struct ORBit_RootObject_struct {
	const struct ORBit_RootObject_Interface_struct* interface;
	gint refs;
};

#define ORBIT_REFCNT_STATIC -10

#define ORBIT_ROOT_OBJECT(obj) ((ORBit_RootObject)(obj))
#define ORBIT_ROOT_OBJECT_gettype(obj) \
	( ORBIT_ROOT_OBJECT(obj)->interface->type )


typedef struct ORBit_RootObject_struct *ORBit_RootObject;
typedef struct ORBit_RootObject_Interface_struct ORBit_RootObject_Interface;

extern void ORBit_RootObject_init(ORBit_RootObject obj,
			      const ORBit_RootObject_Interface* obj_epv);
extern gpointer ORBit_RootObject_dup(gpointer obj);
extern void	ORBit_RootObject_release(gpointer obj);

#endif /* !_ORBIT_ROOTOBJECT_H_ */
