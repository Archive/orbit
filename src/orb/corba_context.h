#ifndef CORBA_CONTEXT_H
#define CORBA_CONTEXT_H 1

#include "orbit_rootobject.h"

typedef struct {
  CORBA_unsigned_long len;
  const CORBA_char *str;
} ORBit_ContextMarshalItem;

struct CORBA_Context_type {
  struct ORBit_RootObject_struct parent;
  GHashTable *mappings;
  GSList *children;

  char *the_name;

  CORBA_Context parent_ctx;
};

void ORBit_Context_marshal(CORBA_Context ctx, ORBit_ContextMarshalItem *mlist,
			   CORBA_unsigned_long nitems, GIOPSendBuffer *buf);
void ORBit_Context_demarshal(CORBA_Context parent, CORBA_Context initme, GIOPRecvBuffer *recv_buffer);
void ORBit_Context_server_free(CORBA_Context ctx);

#endif
