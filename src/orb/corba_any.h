/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  ORBit: A CORBA v2.2 ORB
 *
 *  Copyright (C) 1998 Richard H. Porter
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Dick Porter <dick@cymru.net>
 *
 */

#ifndef _ORBIT_CORBA_ANY_H_
#define _ORBIT_CORBA_ANY_H_

#include "orbit_types.h"
#include "corba_typecode.h"

#include <unistd.h>

#define CORBA_any__alloc CORBA_any_alloc
extern CORBA_any *CORBA_any_alloc(	void);
extern gpointer CORBA_any__freekids(	gpointer mem, 
					gpointer func_data);
extern void		CORBA_any_set_release( CORBA_any *, CORBA_boolean);
extern CORBA_boolean	CORBA_any_get_release( CORBA_any *);

extern size_t	ORBit_gather_alloc_info(	CORBA_TypeCode tc);
extern gint	ORBit_find_alignment(	CORBA_TypeCode tc);
extern CORBA_TypeCode
		ORBit_get_union_tag(	CORBA_TypeCode union_tc,
				   	gconstpointer *val, 
					gboolean update);
extern gpointer	ORBit_copy_value(	gconstpointer src, 
					CORBA_TypeCode tc);
extern void	ORBit_copy_value_core(	gconstpointer *src, 
					gpointer *dst, 
					CORBA_TypeCode tc);

extern void	CORBA_any__copy(CORBA_any *out, CORBA_any *in);

extern void	ORBit_TypeCode_demarshal_value(
					CORBA_TypeCode tc,
					gpointer *val,
					GIOPRecvBuffer *buf,
					gboolean dup_strings,
					CORBA_ORB orb);

extern int	ORBit_TypeCode_cmpvals(	CORBA_TypeCode tc,
  					gconstpointer val1, 
					gconstpointer val2, 
					int cmp_flags);

extern int 	ORBit_any_cmp(		CORBA_any *any1, 
					CORBA_any *any2, 
					int cmp_flags);

#endif /* !_ORBIT_CORBA_ANY_H_ */
