
#include "orbit.h"



CORBA_Identifier CORBA_ServerRequest_operation(CORBA_ServerRequest req, CORBA_Environment *env)
{
	return CORBA_string_dup(req->rbuf->message.u.request.operation);
}

CORBA_Context
CORBA_ServerRequest_ctx(CORBA_ServerRequest req, CORBA_Environment *env)
{
	if(!req->params || req->did_ctx) {
		CORBA_exception_set_system(env, ex_CORBA_BAD_INV_ORDER,
					   CORBA_COMPLETED_NO);
		return NULL;
	}

	return NULL;
}

void
CORBA_ServerRequest_arguments(CORBA_ServerRequest req,
			      CORBA_NVList *parameters,
			      CORBA_Environment *env)
{
	int i;

	if(req->params) {
		CORBA_exception_set_system(env, ex_CORBA_BAD_INV_ORDER,
					   CORBA_COMPLETED_NO);
		return;
	}

	req->params = parameters;

	for(i = 0; i < parameters->list->len; i++) {
		CORBA_NamedValue *cur;

		cur = &g_array_index(parameters->list, CORBA_NamedValue, i);

		if(cur->arg_modes & CORBA_ARG_OUT) continue;
		cur->argument._value = ORBit_demarshal_arg(req->rbuf,
							   cur->argument._type,
							   TRUE,
							   (CORBA_ORB)req->orb);
		CORBA_any_set_release(&cur->argument, TRUE);
	}
}

void
CORBA_ServerRequest_set_result(CORBA_ServerRequest req,
			       const CORBA_any *value,
			       CORBA_Environment *env)
{
	if(req->sbuf) {
		CORBA_exception_set_system(env, ex_CORBA_BAD_INV_ORDER,
					   CORBA_COMPLETED_NO);
		return;
	}

	req->sbuf = giop_send_reply_buffer_use(GIOP_MESSAGE_BUFFER(req->rbuf)->connection,
					       NULL,
					       req->rbuf->message.u.request.request_id,
					       CORBA_NO_EXCEPTION);
	if(!req->sbuf) {
		CORBA_exception_set_system(env, ex_CORBA_COMM_FAILURE,
					   CORBA_COMPLETED_NO);
		return;
	}
		
	ORBit_marshal_arg(req->sbuf, value->_value, value->_type);
}

void
CORBA_ServerRequest_set_exception(CORBA_ServerRequest req,
				  CORBA_exception_type major,
				  const CORBA_any *value,
				  CORBA_Environment *env)
{
	if(req->sbuf) {
		CORBA_exception_set_system(env, ex_CORBA_BAD_INV_ORDER,
					   CORBA_COMPLETED_NO);
		return;
	}

	req->sbuf = giop_send_reply_buffer_use(GIOP_MESSAGE_BUFFER(req->rbuf)->connection,
					       NULL,
					       req->rbuf->message.u.request.request_id,
					       major);
	if(!req->sbuf) {
		CORBA_exception_set_system(env, ex_CORBA_COMM_FAILURE,
					   CORBA_COMPLETED_NO);
		return;
	}

	req->did_exc = TRUE;

	/* XXX do we really need to copy the repo_id into the
	   send buffer? Or is there a way to assume that the CORBA_TypeCode
	   value->_type will be around until after we send the message? */
	{
		CORBA_unsigned_long slen;
		slen = strlen(value->_type->repo_id) + 1;
		giop_send_buffer_append_mem_indirect_a(req->sbuf, &slen,
						       sizeof(slen));
		giop_send_buffer_append_mem_indirect(req->sbuf,
						     value->_type->repo_id,
						     slen);
	}
		
	ORBit_marshal_arg(req->sbuf, value->_value, value->_type);
}


static const ORBit_RootObject_Interface ORBit_ServerRequest_epv = {
	ORBIT_ROT_SERVERREQUEST,
	0
};


/* POA-related DSI stuff */
static void
dynamic_impl_skel(PortableServer_DynamicImpl *_ORBIT_servant,
		  GIOPRecvBuffer *_ORBIT_recv_buffer,
		  CORBA_Environment *ev,
		  PortableServer_DynamicImplRoutine invoke)
{
	/* here the magic occurs... */
	struct CORBA_ServerRequest_type sr;

	ORBit_RootObject_init(&sr.parent, &ORBit_ServerRequest_epv);
	ORBit_RootObject_dup(&sr);	/* make sure no-one tries to free it */
	sr.rbuf = _ORBIT_recv_buffer;
	sr.orb = GIOP_MESSAGE_BUFFER(_ORBIT_recv_buffer)->connection->orb_data;

	_ORBIT_servant->vepv->PortableServer_DynamicImpl_epv->invoke(_ORBIT_servant,
								     &sr);

	if(sr.sbuf) {
		int i;
		for(i = 0; i < sr.params->list->len; i++) {
			CORBA_NamedValue *cur;
			
			cur = &g_array_index(sr.params->list, CORBA_NamedValue, i);
			
			if(cur->arg_modes & CORBA_ARG_IN) continue;

			ORBit_marshal_arg(sr.sbuf, cur->argument._value,
					  cur->argument._type);
		}

		giop_send_buffer_write(sr.sbuf);
		giop_send_buffer_unuse(sr.sbuf);
	} else
		g_warning("Yo, your DSI code is messed up! You forgot to set_result|set_exception");

	CORBA_NVList_free(sr.params, ev);
}

static ORBitSkeleton 
dynamic_impl_get_skel(PortableServer_DynamicImpl * servant,
		      GIOPRecvBuffer * _ORBIT_recv_buffer,
		      gpointer * impl)
{
	*impl = (gpointer)servant->vepv->PortableServer_DynamicImpl_epv->invoke;

	return (ORBitSkeleton)dynamic_impl_skel;
}

void
PortableServer_DynamicImpl__init(PortableServer_Servant servant,
				 CORBA_Environment *ev)
{
	static PortableServer_ClassInfo class_info =
	{(ORBitSkeleton (*)(PortableServer_ServantBase *, gpointer, gpointer *))
	 &dynamic_impl_get_skel, "IDL:CORBA/Object:1.0", NULL};
	
	PortableServer_ServantBase__init(servant, ev);
	ORBIT_SERVANT_SET_CLASSINFO(servant,&class_info);
}

void PortableServer_DynamicImpl__fini(PortableServer_Servant servant,
				      CORBA_Environment *ev)
{
	PortableServer_ServantBase__fini(servant, ev);
}
