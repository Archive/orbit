#include "orbit.h"
#include "sequences.h"
#include <ctype.h>


void CORBA_sequence_set_release(void *seq, CORBA_boolean flag)
{
	struct CORBA_Sequence_type *sequence;

	g_assert(seq!=NULL);

	sequence=(struct CORBA_Sequence_type *)seq;

	if(flag==CORBA_TRUE) {
		sequence->_release |= CORBA_ANYFLAGS_RELEASE;
	} else {
		sequence->_release &= ~CORBA_ANYFLAGS_RELEASE;
	}
}

CORBA_boolean CORBA_sequence_get_release(void *seq)
{
	struct CORBA_Sequence_type *sequence;

	g_assert(seq!=NULL);

	sequence=(struct CORBA_Sequence_type *)seq;

	if(sequence->_release & CORBA_ANYFLAGS_RELEASE)
		return(CORBA_TRUE);
	else
		return(CORBA_FALSE);
}

gpointer CORBA_sequence__freekids(gpointer mem, gpointer func_data)
{
    struct CORBA_Sequence_type *seq = mem;
    if(seq->_release)
    	CORBA_free(seq->_buffer);
    return seq + 1;
}

inline CORBA_sequence_octet*
ORBit_sequence_octet_init(CORBA_sequence_octet*dst, int buflen)
{
	dst->_length = dst->_maximum = buflen;
	dst->_buffer = CORBA_octet_allocbuf(buflen);
	dst->_release = CORBA_TRUE;
	return dst;
}

inline void
ORBit_sequence_octet_fini(CORBA_sequence_octet*dst)
{
  	if(dst->_release)
    		CORBA_free(dst->_buffer);
}

inline CORBA_sequence_octet*
ORBit_sequence_octet_create(int buflen)
{
    	CORBA_sequence_octet* dst = CORBA_sequence_octet__alloc();
	ORBit_sequence_octet_init(dst, buflen);
	return dst;
}

inline CORBA_sequence_octet*
ORBit_sequence_octet_copy(CORBA_sequence_octet* dst, CORBA_sequence_octet* src)
{
	ORBit_sequence_octet_init(dst, src->_length);
	memcpy(dst->_buffer, src->_buffer, src->_length);
	return dst;
}

CORBA_sequence_octet*
ORBit_sequence_octet_dup(CORBA_sequence_octet* src)
{
    	CORBA_sequence_octet* dst = CORBA_sequence_octet__alloc();
	return ORBit_sequence_octet_copy(dst, src);
}

guint
ORBit_sequence_octet_hash(const CORBA_sequence_octet *so)
{
	const char *s = (char*)so->_buffer;
  	const char *p, *e = ((char *)so->_buffer) + so->_length;
  	guint h=0, g;

  	for(p = s; p < e; p ++) {
    		h = ( h << 4 ) + *p;
    		if ( ( g = h & 0xf0000000 ) ) {
      			h = h ^ (g >> 24);
      			h = h ^ g;
    		}
  	}
  	return h;
}

/*
 * Returns negative,zero, positive in collating order. Zero means equal.
 * This is NOT a boolean return.
 */
gint
ORBit_sequence_octet_cmp(	const CORBA_sequence_octet *s1, 
				const CORBA_sequence_octet *s2)
{
	int	difflen = s2->_length - s1->_length;
	int	diffval;
	if( difflen )
		return difflen;

	diffval = memcmp(s1->_buffer, s2->_buffer, s1->_length);
	return diffval;
}

/*
 * Returns TRUE if s1 and s2 are the same, FALSE otherwise.
 * Note that this is what the glib hash module wants.
 */
gint
ORBit_sequence_octet_equal(	const CORBA_sequence_octet *s1, 
				const CORBA_sequence_octet *s2)
{
    int	same;
    same = s1->_length == s2->_length 
      && memcmp(s1->_buffer,s2->_buffer,s1->_length)==0;
    return same;
}


/* copied from orb.c */
#define hexdigit(n) (((n)>9)?(n+'a'-10):(n+'0'))

static void	/* you can un-static this if you have any need for it */
ORBit_sequence_octet_to_hexstr(const CORBA_sequence_octet *src, char *dst) 
{
    char *ctmp = dst;
    int	i;
    for (i = 0; i < src->_length; i++) {
	*(ctmp++) = hexdigit((((src->_buffer[i]) & 0xF0) >> 4));
	*(ctmp++) = hexdigit(((src->_buffer[i]) & 0xF));
    }
    *ctmp = '\0';
}

CORBA_char*
ORBit_sequence_octet_to_hexstrdup(const CORBA_sequence_octet *src) {
    CORBA_char *dst = CORBA_string_alloc(src->_length*2);
    ORBit_sequence_octet_to_hexstr(src, dst);
    return dst;
}

CORBA_boolean
ORBit_hexbuf_to_buf(const CORBA_char *hexbuf, int hexlen, char *buf)
{
    int	j;
    if ( (hexlen & 1) )
    	return CORBA_FALSE;
    for (j = 0; j < hexlen; j+=2) {
	int dig1 = HEXDIGIT(hexbuf[j+0]);
	int dig2 = HEXDIGIT(hexbuf[j+1]);
	if ( (dig1&~0xF)!=0 || (dig2&~0xF)!=0 )
	    return CORBA_FALSE;
    	*buf++ = dig1<<4 | dig2;
    }
    return CORBA_TRUE;
}

CORBA_sequence_octet*
ORBit_sequence_octet_create_from_hexstr(const CORBA_char *src)
{
    int hexlen = strlen(src);
    CORBA_sequence_octet *dst = ORBit_sequence_octet_create(hexlen/2);
    if ( !ORBit_hexbuf_to_buf(src, hexlen, dst->_buffer) ) {
	CORBA_free(dst);
    	return NULL;
    }
    return dst;
}

CORBA_char**
CORBA_string_allocbuf(CORBA_unsigned_long len)
{
    CORBA_char **retval;
    retval = (CORBA_char**)ORBit_alloc(sizeof(CORBA_char*), len,
      CORBA_string__freekids);
    memset( retval, 0, sizeof(CORBA_char*)*len);
    return retval;
}
