#ifndef _ORBIT_ORBIT_TYPECODE_H_
#define _ORBIT_ORBIT_TYPECODE_H_

#include "orbit_types.h"

extern const ORBit_RootObject_Interface ORBit_TypeCode_epv;
void ORBit_encode_CORBA_TypeCode(CORBA_TypeCode tc, GIOPSendBuffer* buf);
void ORBit_decode_CORBA_TypeCode(CORBA_TypeCode* tc, GIOPRecvBuffer* buf);

#define ORBit_TypeCode_dup(tc) \
	ORBit_RootObject_dup(&(tc)->parent)
#define ORBit_TypeCode_release(tc) \
	ORBit_RootObject_release(&(tc)->parent)


extern gpointer ORBit_alloc_tcval(CORBA_TypeCode tc, guint nelements);
extern gpointer ORBit_TypeCode_freekids(CORBA_TypeCode tc, gpointer mem);


/*
 * below is used/defined in ir.c, which should be renamed typecode-ir.c
 */
typedef struct ORBitTcKindInfo {
    short	kind;	/* mainly for debug */
    short	flags;	/* see ORBit_TckiF_ below ... */
} ORBitTcKindInfo;

#define ORBit_TckiF_Empty	(1<<0)
#define ORBit_TckiF_Simple	(1<<1)
#define ORBit_TckiF_Complex	(1<<2)
#define ORBit_TckiF_HasRepoId	(1<<3)
#define ORBit_TckiF_HasMembers	(1<<4)

extern const ORBitTcKindInfo ORBitTckiTbl[];

#define ORBit_CmpF_UnAlias		(1<<0)
#define ORBit_CmpF_RepoSufficient	(1<<1)
#define ORBit_CmpF_RepoSkip		(1<<2)

#define ORBit_CmpF_Equal		0
#define ORBit_CmpF_Equivalent		\
  (ORBit_CmpF_UnAlias|ORBit_CmpF_RepoSufficient|ORBit_CmpF_RepoSkip)

extern int ORBit_TypeCode_cmp(	CORBA_TypeCode tc1_in, 
				CORBA_TypeCode tc2_in, 
				int cmp_flags);

#endif
