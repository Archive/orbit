/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  ORBit: A CORBA v2.2 ORB
 *
 *  Copyright (C) 1998 Richard H. Porter
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Dick Porter <dick@cymru.net>
 *  Author: Kennard White <kennard@berkeley.innomedia.com>
 *
 */

#include <stdlib.h>
#include <assert.h>

#include "orbit.h"


/*
   V2.3 sec 10.7.1

    The funcs declared here are spec'd as part of the interface
    repository, even though they have relatively little to do
    with it. This file should be renamed something like "typecode-ir.c".

    Functions in this module should operate solely on typecodes: no values;
    that is, funcs which manipulate values based upon typecodes 
    (e.g., CORBA_any) belong elsewhere.
*/


const ORBitTcKindInfo ORBitTckiTbl[CORBA_tk_last]={
    {CORBA_tk_null,	ORBit_TckiF_Empty},
    {CORBA_tk_void,	ORBit_TckiF_Empty},
    {CORBA_tk_short,	ORBit_TckiF_Empty},
    {CORBA_tk_long,	ORBit_TckiF_Empty},
    {CORBA_tk_ushort,	ORBit_TckiF_Empty},
    {CORBA_tk_ulong,	ORBit_TckiF_Empty},
    {CORBA_tk_float,	ORBit_TckiF_Empty},
    {CORBA_tk_double,	ORBit_TckiF_Empty},
    {CORBA_tk_boolean,	ORBit_TckiF_Empty},
    {CORBA_tk_char,	ORBit_TckiF_Empty},
    {CORBA_tk_octet,	ORBit_TckiF_Empty},
    {CORBA_tk_any,	ORBit_TckiF_Empty},
    {CORBA_tk_TypeCode,	ORBit_TckiF_Empty},
    {CORBA_tk_Principal,ORBit_TckiF_Empty},
    {CORBA_tk_objref,	ORBit_TckiF_Complex|ORBit_TckiF_HasRepoId},
    {CORBA_tk_struct,	
    	ORBit_TckiF_Complex|ORBit_TckiF_HasRepoId|ORBit_TckiF_HasMembers},
    {CORBA_tk_union,	
        ORBit_TckiF_Complex|ORBit_TckiF_HasRepoId|ORBit_TckiF_HasMembers},
    {CORBA_tk_enum,	
        ORBit_TckiF_Complex|ORBit_TckiF_HasRepoId|ORBit_TckiF_HasMembers},
    {CORBA_tk_string,	ORBit_TckiF_Simple},
    {CORBA_tk_sequence,	ORBit_TckiF_Complex},
    {CORBA_tk_array,	ORBit_TckiF_Complex},
    {CORBA_tk_alias,	ORBit_TckiF_Complex|ORBit_TckiF_HasRepoId},
    {CORBA_tk_except,	
        ORBit_TckiF_Complex|ORBit_TckiF_HasRepoId|ORBit_TckiF_HasMembers},
    {CORBA_tk_longlong,	ORBit_TckiF_Empty},
    {CORBA_tk_ulonglong,ORBit_TckiF_Empty},
    {CORBA_tk_longdouble,ORBit_TckiF_Empty},
    {CORBA_tk_wchar,	ORBit_TckiF_Empty},
    {CORBA_tk_wstring,	ORBit_TckiF_Simple},
    {CORBA_tk_fixed,	ORBit_TckiF_Simple}
};


/**
   FIXME: Right now this function doesn't record whether or not it has
   already visited a given TypeCode.  I'm not sure if every recursive
   type will have a tk_recursive node in it; if not, then this will
   need to be reworked a bit.  

    With no flags, the comparison is very strict and requires "equality"
    between the two types codes in the sense of CORBA_TypeCode_equal().
    This declares them equal if and only if all CORBA_TypeCode_foo() methods
    would have the same result for either one. The flags below
    broaden the definition of equivalence:
	UnAlias		An alias is considered equivalent to its
			underlying type.
	RepoSufficient	The repo_id is a sufficient description
			of equivalence: if the repo_ids are both non-empty
			and equal, then the types are equivalent.
			This allows the structure check to be skipped.
	RepoSkip	If either repo_ids is empty, then
			dont use the repo_id to determine equivalence.
			Without this flag, equivalence require the repo_ids
			to ether both be empty, or to be the same string.
   	Both Repo flags only apply to types which have repo_ids: objref,
	struct,enum,alias,except).

    Note that I've setup this function as a compare (ala strcmp) as
    not as a boolean predicate incase we want to use some binary-tree like
    mechanism to index typecodes. Arguably, a hash would make more sense,
    but defining a hash func on typecodes seems even more challenging
    that a defining total ordering.
*/
int
ORBit_TypeCode_cmp(CORBA_TypeCode tc1_in, CORBA_TypeCode tc2_in, int flags)
{
    CORBA_TypeCode tc1 = tc1_in, tc2 = tc2_in;
    int res, i;

    if ( tc1 == tc2 )
    	return 0;

    if ( flags & ORBit_CmpF_UnAlias ) {
	while ( tc1->kind == CORBA_tk_alias ) {
	    tc1 = tc1->subtypes[0];
	}
	while ( tc2->kind == CORBA_tk_alias ) {
	    tc2 = tc2->subtypes[0];
	}
    }

    if ( (res=(tc1->kind - tc2->kind)) != 0 )
	return res;

    if ( (ORBitTckiTbl[tc1->kind].flags & ORBit_TckiF_HasRepoId) ) {
	const char *tc1id = tc1->repo_id ? tc1->repo_id : "";
	const char *tc2id = tc2->repo_id ? tc2->repo_id : "";
	res = strcmp(tc1id,tc2id);
	if ( (flags&ORBit_CmpF_RepoSufficient)
	  && tc1id[0] && tc2id[0] && res==0 )
	    return res;
	if ( (flags&ORBit_CmpF_RepoSkip)==0 && res!=0 )
	    return res;
    }

    /* Below here is structural check, minus repo_id check */

    switch (tc1->kind) {
    case CORBA_tk_wstring:
    case CORBA_tk_string:
	return tc1->length - tc2->length;
    case CORBA_tk_objref:
	break;
    case CORBA_tk_except:
    case CORBA_tk_struct:
	if ( (res=(tc1->sub_parts - tc2->sub_parts)) != 0 )
	    return res;
	for (i = 0; i < tc1->sub_parts; ++i) {
	    if ( (res=ORBit_TypeCode_cmp(tc1->subtypes[i],
				tc2->subtypes[i], flags))!=0 )
		return res;
	}
	break;
    case CORBA_tk_union:
	if ( (res=(tc1->sub_parts - tc2->sub_parts)) != 0 )
	    return res;
	if ( (res=ORBit_TypeCode_cmp(tc1->discriminator,
				tc2->discriminator, flags))!=0 )
	    return res;
	if ( (res=(tc1->default_index-tc2->default_index)) != 0 )
	    return res;
	for (i = 0; i < tc1->sub_parts; ++i) {
	    if ( (res=ORBit_TypeCode_cmp(tc1->subtypes[i],
				tc2->subtypes[i], flags))!=0 )
		return res;
	    if ( (res=ORBit_any_cmp(&tc1->sublabels[i],
				&tc2->sublabels[i], flags))!=0 )
		return res;
	}
	break;
    case CORBA_tk_enum:
	if ( (res=(tc1->sub_parts - tc2->sub_parts)) != 0 )
	    return res;
	for (i = 0; i < tc1->sub_parts; ++i) {
	    if ( (res=strcmp(tc1->subnames[i], tc2->subnames[i])) !=0 )
		return res;
	}
	break;
    case CORBA_tk_sequence:
    case CORBA_tk_array:
	if ( (res=(tc1->length - tc2->length)) != 0 )
	    return res;
	return ORBit_TypeCode_cmp(tc1->subtypes[0], tc2->subtypes[0], flags);
    case CORBA_tk_alias:
	return ORBit_TypeCode_cmp(tc1->subtypes[0], tc2->subtypes[0], flags);
    case CORBA_tk_recursive:
	return tc1->recurse_depth - tc2->recurse_depth;
    case CORBA_tk_fixed:
	if ( (res=(tc1->digits-tc2->digits)) != 0 )
	    return res;
	return tc1->scale - tc2->scale;

    default:
	    /* Everything else is primitive.  */
	    break;
    }

    return 0;
}

CORBA_boolean CORBA_TypeCode_equal(CORBA_TypeCode tc1, CORBA_TypeCode tc2, 
  CORBA_Environment *ev)
{
	g_return_val_if_fail(tc1!=NULL, CORBA_FALSE);
	g_return_val_if_fail(tc2!=NULL, CORBA_FALSE);
	return ORBit_TypeCode_cmp(tc1, tc2, ORBit_CmpF_Equal)==0?1:0;
}


CORBA_boolean CORBA_TypeCode_equivalent(CORBA_TypeCode tc1, CORBA_TypeCode tc2, 
  CORBA_Environment *ev)
{
	g_return_val_if_fail(tc1!=NULL, CORBA_FALSE);
	g_return_val_if_fail(tc2!=NULL, CORBA_FALSE);
	return ORBit_TypeCode_cmp(tc1, tc2, ORBit_CmpF_Equivalent)==0?1:0;
}

CORBA_TCKind CORBA_TypeCode_kind(CORBA_TypeCode obj, CORBA_Environment *ev)
{
	return obj->kind;
}

static void bad_kind (CORBA_Environment *ev)
{
	CORBA_TypeCode_BadKind *err;
	err = g_new (CORBA_TypeCode_BadKind, 1);
	if (err == NULL) {
		CORBA_exception_set_system (ev, ex_CORBA_NO_MEMORY,
					    CORBA_COMPLETED_NO);
	} else {
		err->dummy = 23;
		CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
				     "IDL:omg.org/CORBA/TypeCode/BadKind/1.0",
				     err);
	}
}

CORBA_RepositoryId CORBA_TypeCode_id(CORBA_TypeCode obj, CORBA_Environment *ev)
{
    	if ( obj->kind >= CORBA_tk_last 
	  || (ORBitTckiTbl[obj->kind].flags & ORBit_TckiF_HasRepoId)==0 ) {
		bad_kind (ev);
		return NULL;
	}
	return (CORBA_RepositoryId) obj->repo_id;
}

CORBA_Identifier CORBA_TypeCode_name(CORBA_TypeCode obj, CORBA_Environment *ev)
{
    	if ( obj->kind >= CORBA_tk_last 
	  || (ORBitTckiTbl[obj->kind].flags & ORBit_TckiF_HasRepoId)==0 ) {
		bad_kind (ev);
		return NULL;
	}

	return (CORBA_Identifier) obj->name;
}

CORBA_unsigned_long CORBA_TypeCode_member_count(CORBA_TypeCode obj, CORBA_Environment *ev)
{
    	if ( obj->kind >= CORBA_tk_last 
	  || (ORBitTckiTbl[obj->kind].flags & ORBit_TckiF_HasMembers)==0 ) {
		bad_kind (ev);
		return 0;
	}
	return obj->sub_parts;
}

static void bounds_error (CORBA_Environment *ev)
{
	CORBA_TypeCode_Bounds *err;
	err = g_new (CORBA_TypeCode_Bounds, 1);
	if (err == NULL) {
		CORBA_exception_set_system (ev, ex_CORBA_NO_MEMORY,
					    CORBA_COMPLETED_NO);
	} else {
		err->dummy = 23;
		CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
				     "IDL:omg.org/CORBA/TypeCode/Bounds/1.0",
				     err);
	}
}

CORBA_Identifier CORBA_TypeCode_member_name(CORBA_TypeCode obj, CORBA_unsigned_long index, CORBA_Environment *ev)
{
    	if ( obj->kind >= CORBA_tk_last 
	  || (ORBitTckiTbl[obj->kind].flags & ORBit_TckiF_HasMembers)==0 ) {
		bad_kind (ev);
		return NULL;
	}
	if (index >= obj->sub_parts) {
		bounds_error (ev);
		return NULL;
	}
	return (CORBA_Identifier) obj->subnames[index];
}

CORBA_TypeCode CORBA_TypeCode_member_type(CORBA_TypeCode obj, CORBA_unsigned_long index, CORBA_Environment *ev)
{
    	if ( obj->kind >= CORBA_tk_last 
	  || (ORBitTckiTbl[obj->kind].flags & ORBit_TckiF_HasMembers)==0 ) {
		bad_kind (ev);
		return NULL;
	}
	if (index >= obj->sub_parts) {
		bounds_error (ev);
		return NULL;
	}
	return obj->subtypes[index];
}

CORBA_any *CORBA_TypeCode_member_label(CORBA_TypeCode obj, CORBA_unsigned_long index, CORBA_Environment *ev)
{
	if (obj->kind != CORBA_tk_union) {
		bad_kind (ev);
		return NULL;
	}
	if (index >= obj->sub_parts) {
		bounds_error (ev);
		return NULL;
	}
	return &obj->sublabels[index];
}

CORBA_TypeCode CORBA_TypeCode_discriminator_type(CORBA_TypeCode obj, CORBA_Environment *ev)
{
	if (obj->kind != CORBA_tk_union) {
		bad_kind (ev);
		return NULL;
	}
	return obj->discriminator;
}

CORBA_long CORBA_TypeCode_default_index(CORBA_TypeCode obj, CORBA_Environment *ev)
{
	if (obj->kind != CORBA_tk_union) {
		bad_kind (ev);
		return 0;
	}
	return obj->default_index;
}

CORBA_unsigned_long CORBA_TypeCode_length(CORBA_TypeCode obj, CORBA_Environment *ev)
{
	if (! (obj->kind == CORBA_tk_string || obj->kind == CORBA_tk_wstring
	  || obj->kind == CORBA_tk_array || obj->kind == CORBA_tk_sequence)) {
		bad_kind (ev);
		return 0;
	}
	return obj->length;
}

CORBA_TypeCode CORBA_TypeCode_content_type(CORBA_TypeCode obj, CORBA_Environment *ev)
{
	if (! (obj->kind == CORBA_tk_sequence || obj->kind == CORBA_tk_array
	       || obj->kind == CORBA_tk_alias)) {
		bad_kind (ev);
		return NULL;
	}
	g_assert (obj->sub_parts == 1);
	return obj->subtypes[0];
}

CORBA_unsigned_short CORBA_TypeCode_fixed_digits(CORBA_TypeCode obj, CORBA_Environment *ev)
{
	if (obj->kind != CORBA_tk_fixed) {
		bad_kind (ev);
		return 0;
	}
	return obj->digits;
}

CORBA_short CORBA_TypeCode_fixed_scale(CORBA_TypeCode obj, CORBA_Environment *ev)
{
	if (obj->kind != CORBA_tk_fixed) {
		bad_kind (ev);
		return 0;
	}
	return obj->scale;
}

CORBA_long CORBA_TypeCode_param_count(CORBA_TypeCode obj, CORBA_Environment *ev)
{
	g_assert(!"Deprecated");
	return(0);
}

CORBA_any *CORBA_TypeCode_parameter(CORBA_TypeCode obj, CORBA_long index, CORBA_Environment *ev)
{
	g_assert(!"Deprecated");
	return(NULL);
}
