/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  ORBit: A CORBA v2.2 ORB
 *
 *  Copyright (C) 1998 Richard H. Porter
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Dick Porter <dick@cymru.net>
 *
 */

#ifndef _ORBIT_CORBA_ORB_TYPE_H_
#define _ORBIT_CORBA_ORB_TYPE_H_

typedef struct ORBit_OrbGroup_type ORBit_OrbGroup;
struct ORBit_genrand_type;
struct ORBit_POAInvokation_struct;


struct CORBA_ORB_type {
	struct ORBit_RootObject_struct parent;
	ORBit_OrbGroup	*group;
	struct ORBit_genrand_type *genrand;
	CORBA_ORBid orb_identifier;
	CORBA_RepositoryId repoid;
	CORBA_boolean use_poa;
	int life_flags;

	GHashTable *irefs;	/* of string --> ORBit_Iref */
	struct {
		GIOPConnection *ipv4;
		gboolean ipv4_isPersistent;
		GIOPConnection *ipv6;
		GIOPConnection *usock;
	} cnx;

	GHashTable *objrefs;
	GHashTable *srv_forw_bindings;	/* of objkey --> ORBit_SrvForwBind */

	PortableServer_POA	root_poa;
	GPtrArray 		*poas;	/* of PortableServer_POA */
	struct ORBit_POAInvokation_struct *poa_current_invokations;

	CORBA_Context default_ctx;
};

#define CORBA_ORB_CAST(orb) ((CORBA_ORB)orb)


struct CORBA_DomainManager_type {
	struct ORBit_RootObject_struct parent;
};


struct CORBA_ConstructionPolicy_type {
	int fill_me_in;
};


#endif /* !_ORBIT_CORBA_ORB_TYPE_H_ */
