/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  ORBit: A CORBA v2.2 ORB
 *
 *  Copyright (C) 1998 Richard H. Porter
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Dick Porter <dick@cymru.net>
 *
 */

#ifndef _ORBIT_CORBA_SEQUENCES_H_
#define _ORBIT_CORBA_SEQUENCES_H_


#define _CORBA_sequence_NameValuePair_defined
typedef struct CORBA_sequence_NameValuePair CORBA_NameValuePairSeq;
typedef struct CORBA_sequence_CORBA_any_struct CORBA_AnySeq;

typedef CORBA_sequence_octet PortableServer_ObjectId;


/* Moved from orbit_types.h */
#ifndef HAVE_CORBA_PRINCIPAL
#define HAVE_CORBA_PRINCIPAL 1
typedef CORBA_sequence_octet CORBA_Principal;
#endif
typedef CORBA_sequence_octet CORBA_DynAny_OctetSeq;
typedef CORBA_sequence_octet CORBA_DynFixed_OctetSeq;
typedef CORBA_sequence_octet CORBA_DynEnum_OctetSeq;
typedef CORBA_sequence_octet CORBA_DynStruct_OctetSeq;
typedef CORBA_sequence_octet CORBA_DynUnion_OctetSeq;
typedef CORBA_sequence_octet CORBA_DynSequence_OctetSeq;
typedef CORBA_sequence_octet CORBA_DynArray_OctetSeq;

#endif /* !_ORBIT_CORBA_SEQUENCES_H_ */
