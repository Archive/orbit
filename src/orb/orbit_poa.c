/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  ORBit: A CORBA v2.2 ORB
 *
 *  Copyright (C) 1998 Richard H. Porter and Red Hat Software
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Phil Dawes <philipd@parallax.co.uk>
 *	    Elliot Lee <sopwith@redhat.com>
 *	    Kennard White <kennard@berkeley.innomedia.com>
 *
 */

/*
 *   ORBit specific POA funcitons.
 *
 */

#include <string.h>
#include "orbit.h"
#include "orbit_poa_type.h"
#include "orbit_poa.h"
#include "genrand.h"



/****************************************************************************
 *
 *			PortableServer_ClassInfo
 *
 ****************************************************************************/

static CORBA_unsigned_long ORBit_class_assignment_counter = 0;
static GHashTable *ORBit_class_assignments = NULL;

CORBA_unsigned_long
ORBit_classinfo_lookup_id(const char *repo_id) {
	PortableServer_ClassInfo* ci;
	if ( ORBit_class_assignments == 0 )
		return 0;
	if ( (ci=g_hash_table_lookup(ORBit_class_assignments, repo_id))==0 )
		return 0;
	return *(ci->class_id);
}

/* XXX not thread-safe */
void
ORBit_classinfo_register(PortableServer_ClassInfo *class_info)
{
	if(!ORBit_class_assignments)
		ORBit_class_assignments = g_hash_table_new(g_str_hash, g_str_equal);
	if ( *(class_info->class_id) != 0 )
		return;	/* already registered! */

	/* This needs to be pre-increment - we don't want to give out
	   classid 0, because (a) that is reserved for the base Object class
	   (b) all the routines allocate a new id if the variable
	   storing their ID == 0 */
	*(class_info->class_id) = ++ORBit_class_assignment_counter;
	g_hash_table_insert(ORBit_class_assignments, 
	  (gpointer)class_info->class_name, class_info);
}

static void
ORBit_classinfo_cleanup(gpointer key, gpointer value, gpointer user_data)
{
	PortableServer_ClassInfo *ci = value;
	if ( ci->vepvmap ) {
		g_free(ci->vepvmap);
		ci->vepvmap = 0;
		ci->vepvlen = 0;
	}
	*(ci->class_id) = 0;
}

void
ORBit_classinfo_fini()
{
	if ( ORBit_class_assignments == 0 )
		return;
	g_hash_table_foreach( ORBit_class_assignments, 
		ORBit_classinfo_cleanup, /*user_data*/0);
	g_hash_table_destroy( ORBit_class_assignments);
	ORBit_class_assignment_counter = 0;
}

/***************************************************************************
 *
 *			built-in POAManager
 *
 ***************************************************************************/

/**
    Note that while a POAManager and its POAs hold cross-linked
    pointers, it is the POAManager's reference count that is
    controled by the POA. Thus when the last POA unrefs the manager
    (and the application has no outstanding refs to the POAManager),
    then the manager is released. Note that this allows the
    application to hold a manager alive (and re-use it for later
    POAs) even though it may have no POAs at any given time.
**/

static void
ORBit_POAManager_free_fn(gpointer obj_in)
{
  	PortableServer_POAManager poa_mgr = obj_in;
	g_assert( poa_mgr->poa_collection == 0 );
	g_free(poa_mgr);
}


static const ORBit_RootObject_Interface CORBA_POAManager_epv = {
	ORBIT_ROT_POAMANAGER,
	ORBit_POAManager_free_fn
};

PortableServer_POAManager 
ORBit_POAManager_new(CORBA_ORB orb, CORBA_Environment *ev)
{
	PortableServer_POAManager poa_mgr;

	poa_mgr = g_new0(struct PortableServer_POAManager_type, 1);
	if(poa_mgr == NULL) {
		CORBA_exception_set_system(ev, ex_CORBA_NO_MEMORY, CORBA_COMPLETED_NO);
		return NULL;
	}

	ORBit_RootObject_init(&poa_mgr->parent, &CORBA_POAManager_epv);

	poa_mgr->poa_collection = NULL;
	poa_mgr->orb = orb;
	poa_mgr->state = PortableServer_POAManager_HOLDING;
	return poa_mgr;
}


static void
ORBit_POAManager_register_poa(PortableServer_POAManager poa_mgr, 
			      PortableServer_POA poa,
			      CORBA_Environment *ev)
{
	g_assert( g_slist_find(poa_mgr->poa_collection, poa) == 0 );
	poa_mgr->poa_collection = 
		g_slist_append(poa_mgr->poa_collection, poa);
	g_assert(poa->the_POAManager == 0 );
	poa->the_POAManager = ORBit_RootObject_dup(poa_mgr);
}

static void
ORBit_POAManager_unregister_poa(PortableServer_POAManager poa_mgr, 
				PortableServer_POA poa,
				CORBA_Environment *ev)
{
	poa_mgr->poa_collection = g_slist_remove(poa_mgr->poa_collection, poa);
	g_assert(poa->the_POAManager == poa_mgr);
	poa->the_POAManager = 0;
	ORBit_RootObject_release(poa_mgr);
}


/***************************************************************************
 *
 *		Lookup Functions 
 *
 ***************************************************************************/


PortableServer_POA
ORBit_POA_okey_to_poa(	/*in*/PortableServer_POA root_poa,
		        /*in*/CORBA_sequence_octet *okey) {
    long		poanum;	/* signed */
    PortableServer_POA	poa;
    if ( okey->_length < sizeof(ORBit_POA_Serial) )
    	return NULL;
    poanum = *(ORBit_POA_Serial*)(okey->_buffer);
    if ( poanum < 0 || poanum >= root_poa->orb->poas->len )
    	return NULL;
    poa = g_ptr_array_index(root_poa->orb->poas, poanum);
    if ( okey->_length < poa->poa_key._length )
    	return NULL;
    /* XXX: Maybe should leave the key validation until later? */
    if ( memcmp( okey->_buffer, poa->poa_key._buffer, poa->poa_key._length)!=0 )
    	return NULL;
    return poa;
}

/**
    {oid} is looked up within the context of {poa}, and {*okey}
    is filled in with an object key. Note that {okey->_buffer}
    is dynamically allocated, and must be released by the caller.
    This routine is not designed to be quick: it is assumed that
    lookups in this direction are rare.
**/
void
ORBit_POA_oid_to_okey(	/*in*/PortableServer_POA poa,
			/*in*/PortableServer_ObjectId *oid,
			/*out*/CORBA_sequence_octet *okey)
{
    ORBit_POA_Serial	keynum;
    int			restlen;
    CORBA_octet*	restbuf;
    if ( poa->num_to_koid_map ) {
	ORBit_POAKOid *koid = 0;
	int randlen = poa->koid_rand_len;
	/* search for existing mapping */
	for (keynum=0; keynum < poa->num_to_koid_map->len; keynum++) {
	    if ( (koid=g_ptr_array_index(poa->num_to_koid_map, keynum))
	      && koid->oid_length == oid->_length
	      && memcmp( ORBIT_POAKOID_OidBufOf(koid), 
	        oid->_buffer, oid->_length)==0 )
		  break;
	    koid = 0;
	}
	if ( koid==0 ) {
	    /* create a new key-oid mapping */
	    koid = g_malloc( sizeof(*koid) + oid->_length + randlen);
            keynum = poa->num_to_koid_map->len;
            g_ptr_array_add( poa->num_to_koid_map, koid);
	    koid->oid_length = oid->_length;
	    memcpy( ORBIT_POAKOID_OidBufOf(koid), oid->_buffer, oid->_length);
	    if ( randlen )
	    	ORBit_genrand_buf( poa->orb->genrand, ORBIT_POAKOID_RandBufOf(koid), randlen);
	}
	restlen = randlen;
	restbuf = ORBIT_POAKOID_RandBufOf(koid);
    } else {
    	keynum = 0 - oid->_length;
    	restlen = oid->_length;
    	restbuf = oid->_buffer;
    }
    ORBit_sequence_octet_init( okey,
      poa->poa_key._length + sizeof(ORBit_POA_Serial) + restlen);
    memcpy( okey->_buffer, poa->poa_key._buffer, poa->poa_key._length);
    *(ORBit_POA_Serial*)(okey->_buffer+poa->poa_key._length) = keynum;
    memcpy( okey->_buffer + poa->poa_key._length+ sizeof(ORBit_POA_Serial),
      restbuf, restlen);
}


/**
   {okey} will be looked up within the context of {poa}, and {*oid}
   will be filled in.  {oid->_buffer} will either point into {okey}, or
   some internal data (but not a static buffer). Thus its life-time is
   short: dup it if you want to keep it! 
   Returns TRUE if an oid is found, FALSE otherwise. Note
   that simply finding an oid does *not* mean that it is a valid
   or active object.
   WATCHOUT: This doesn't follow normal CORBA calling conventions:
   {oid->_buffer} must not be freed!
**/
CORBA_boolean
ORBit_POA_okey_to_oid(	/*in*/PortableServer_POA poa,
			/*in*/CORBA_sequence_octet *okey,
			/*out*/PortableServer_ObjectId *oid)
{
    int		poa_keylen = poa->poa_key._length;
    long	keynum;	/* this must be signed! */

    oid->_length = 0;
    if ( okey->_length < poa_keylen+sizeof(CORBA_long)
      || memcmp(okey->_buffer, poa->poa_key._buffer, poa_keylen)!=0 )
    	return FALSE;
    keynum = *(ORBit_POA_Serial*)(okey->_buffer + poa_keylen);
    if ( keynum > 0 ) {
	ORBit_POAKOid	*koid;
        if ( poa->num_to_koid_map==NULL || keynum >= poa->num_to_koid_map->len )
	    return FALSE;
	if ( (koid = g_ptr_array_index(poa->num_to_koid_map, keynum))==0 )
    	    return FALSE;
    	oid->_length = ORBIT_POAKOID_OidLenOf(koid);
        oid->_buffer = ORBIT_POAKOID_OidBufOf(koid);
    } else {
    	oid->_buffer = okey->_buffer + sizeof(CORBA_long) + poa_keylen;
    	oid->_length = okey->_length - sizeof(CORBA_long) - poa_keylen; 
    }
    /* NOTE that the oid length may be zero -- that is ok */
    return TRUE;
}


ORBit_POAObject*
ORBit_POA_oid_to_obj(PortableServer_POA poa, 
		PortableServer_ObjectId *oid, CORBA_boolean only_active,
		CORBA_Environment *ev) 
{
    ORBit_POAObject *pobj;
    if ( poa->servant_retention != PortableServer_RETAIN ) {
	if ( ev ) {
    	    CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
	  	ex_PortableServer_POA_WrongPolicy, NULL);
        }
    	return NULL;
    }
    pobj = g_hash_table_lookup(poa->oid_to_obj_map, oid);
    if ( only_active && pobj && pobj->servant==0 ) {
	if ( ev ) {
    	    CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
	  	ex_PortableServer_POA_ObjectNotActive, NULL);
	}
    	return NULL;
    }
    return pobj;
}



ORBit_POAObject*
ORBit_POA_okey_to_obj(PortableServer_POA poa, CORBA_sequence_octet *okey)
{
    PortableServer_ObjectId	oid;

    if ( !ORBit_POA_okey_to_oid(poa, okey, &oid) )
    	return NULL;
    return ORBit_POA_oid_to_obj(poa, &oid, /*active*/0, /*ev*/0);
}

/***************************************************************************
 *
 *		POAObject invokation
 *		Code for invoking requests on POAObject and its servant.
 *
 ***************************************************************************/

static PortableServer_Servant
ORBit_POA_ServantManager_use_servant(PortableServer_POA poa,
		ORBit_POAInvokation *irec,
	     	CORBA_Identifier opname,
	     	PortableServer_ServantLocator_Cookie *the_cookie,
	     	PortableServer_ObjectId *oid,
		CORBA_Environment *ev)
{
	PortableServer_ServantBase *retval;
	if(poa->servant_retention == PortableServer_RETAIN) {
		POA_PortableServer_ServantActivator *sm;
		POA_PortableServer_ServantActivator__epv *epv;
		
		sm = (POA_PortableServer_ServantActivator *)poa->servant_manager;
		epv = sm->vepv->PortableServer_ServantActivator_epv;
		retval = epv->incarnate(sm, oid, poa, ev);
		if ( retval ) {
			g_assert( retval->_private == 0 );
			retval->_private = irec->pobj;
			irec->pobj->servant = retval;
			/* XXX: handle MULT_ID case */
		}
	} else {
		POA_PortableServer_ServantLocator *sm;
		POA_PortableServer_ServantLocator__epv *epv;

		sm = (POA_PortableServer_ServantLocator *)poa->servant_manager;
		epv = sm->vepv->PortableServer_ServantLocator_epv;
		retval = epv->preinvoke(sm, oid, poa, opname, the_cookie, ev);

		if ( retval ) {
			irec->doUnuse = 1;
			g_assert( retval->_private == 0 );
			retval->_private = irec->pobj;
			irec->pobj->servant = retval;
		}
	}
	return retval;
}

static void
ORBit_POA_ServantManager_unuse_servant(
		PortableServer_POA poa,
		ORBit_POAInvokation *irec,
		CORBA_Identifier opname,
		PortableServer_ServantLocator_Cookie cookie,
		PortableServer_ObjectId *oid,
		PortableServer_Servant servant,
		CORBA_Environment *ev)
{
	POA_PortableServer_ServantLocator *sm;
	POA_PortableServer_ServantLocator__epv *epv;
	PortableServer_ServantBase *serv = servant;

	g_assert(poa->servant_retention == PortableServer_NON_RETAIN);

	sm = (POA_PortableServer_ServantLocator *)poa->servant_manager;
	epv = sm->vepv->PortableServer_ServantLocator_epv;
	
	g_assert( serv->_private == irec->pobj);
	serv->_private = NULL;
	epv->postinvoke(sm, oid, poa, opname, cookie, servant, ev);
}


extern ORBit_request_validate ORBIT_request_validator;

/**
    {recv_buffer} is ours (our caller never frees it).
**/
void
ORBit_POA_handle_request(PortableServer_POA poa, GIOPRecvBuffer *recv_buffer)
{
    PortableServer_ServantBase *servant;
    PortableServer_ServantLocator_Cookie cookie;
    ORBit_POAObject *pobj = NULL, tmp_pobj;
    ORBitSkeleton skel;
    gpointer imp = NULL;
    CORBA_Environment env, *ev = &env;
    PortableServer_ObjectId oid;
    PortableServer_POAManager_State	mgr_state;
    ORBit_POAInvokation	invoke_rec;
    CORBA_Identifier opname = recv_buffer->message.u.request.operation;

    CORBA_exception_init(ev);

    mgr_state = (poa->life_flags & ORBit_LifeF_DestroyDo)
      ? PortableServer_POAManager_INACTIVE : poa->the_POAManager->state;
    
    switch(mgr_state) {
    case PortableServer_POAManager_HOLDING:
	    poa->held_requests = g_slist_prepend(poa->held_requests,
						 recv_buffer);
	    return;
    case PortableServer_POAManager_DISCARDING:
	    CORBA_exception_set_system(ev,
					ex_CORBA_TRANSIENT,
					CORBA_COMPLETED_NO);
	    goto errout;
    case PortableServer_POAManager_INACTIVE:
	    CORBA_exception_set_system(ev,
					ex_CORBA_OBJ_ADAPTER,
					CORBA_COMPLETED_NO);
	    goto errout;
    case PortableServer_POAManager_ACTIVE:
	    break;
    default:
	    g_assert_not_reached();
	    break;
    }

    if ( !ORBit_POA_okey_to_oid( poa, 
      &(recv_buffer->message.u.request.object_key), &oid) ) {
	    /* the okey itself is malformed -- no way to recover */
	    CORBA_exception_set_system(ev,
				       ex_CORBA_OBJECT_NOT_EXIST,
				       CORBA_COMPLETED_NO);
	    goto errout;
    }

    pobj = 0;
    if(poa->servant_retention == PortableServer_RETAIN) {
	    pobj = ORBit_POA_oid_to_obj(poa, &oid, /*active*/FALSE, ev);
    }
    /* NOTE that any combination of RETAIN and DEFAULT_SERVANT is legal */
    if ( pobj == 0 ) {
    	switch ( poa->request_processing ) {
	case PortableServer_USE_ACTIVE_OBJECT_MAP_ONLY:
	    CORBA_exception_set_system(ev,
			    ex_CORBA_OBJECT_NOT_EXIST, CORBA_COMPLETED_NO);
	    goto errout;
	case PortableServer_USE_DEFAULT_SERVANT:
    	    if ( (pobj = poa->default_pobj) == 0 || pobj->servant==0 ) {
		CORBA_exception_set_system(ev,
			       ex_CORBA_OBJ_ADAPTER, CORBA_COMPLETED_NO);
	        goto errout;
	    }
	    break;
	case PortableServer_USE_SERVANT_MANAGER:
	    pobj = &tmp_pobj;
	    /* XXX: pobj->parent init */
	    pobj->object_id = &oid;
	    pobj->servant = 0;
	    pobj->poa = poa;
	    pobj->use_cnt = 0;
	    pobj->life_flags = 0;
	    break;
        }
    }
    g_assert( pobj );

    /* From here down, we must not error out until 
     * postinvoke() is called (if appropriate) 
     * and we decrement the counters.
    */
    ++(poa->use_cnt);
    ++(pobj->use_cnt);
    invoke_rec.pobj = pobj;
    invoke_rec.doUnuse = 0;
    invoke_rec.oid = &oid;
    invoke_rec.prev = poa->orb->poa_current_invokations;
    poa->orb->poa_current_invokations = &invoke_rec;

    if ( (servant=pobj->servant)==0 
      && poa->request_processing==PortableServer_USE_SERVANT_MANAGER ) {
	    servant = ORBit_POA_ServantManager_use_servant(poa,
			  &invoke_rec, opname, &cookie, &oid, ev);
    }

    if(ev->_major==0 && servant==0) {
	    CORBA_exception_set_system(ev,
	       ex_CORBA_OBJECT_NOT_EXIST, CORBA_COMPLETED_NO);
    }
    if (ev->_major==0) {
	skel = ORBIT_SERVANT_TO_CLASSINFO(servant)->
		    relay_call(servant, recv_buffer, &imp);
	if(!skel) {
	    if (opname[0] == '_' && strcmp(opname + 1, "is_a") == 0) {
		    skel = (gpointer)&ORBit_impl_CORBA_Object_is_a;
	    } else {
		    CORBA_exception_set_system(ev, ex_CORBA_BAD_OPERATION,
					       CORBA_COMPLETED_NO);
	    }
	} else if (!imp) {
		CORBA_exception_set_system(ev, ex_CORBA_NO_IMPLEMENT,
					   CORBA_COMPLETED_NO);
	}
    }
    if (ev->_major==0) {
	    if(!ORBIT_request_validator) {
		    /* If it got through the random keys, 
		    * and nobody else had the opportunity to say otherwise, 
		    * it must be auth'd */
		    GIOP_MESSAGE_BUFFER(recv_buffer)->connection->is_auth = TRUE;
	    }

	    skel(servant, recv_buffer, ev, imp);
	    /* Currently, the skel handles sending any and all exceptions
	     * that occur within the skel/implementation.
	     */
	    CORBA_exception_free(ev);
    }

    if( invoke_rec.doUnuse ) {
	    ORBit_POA_ServantManager_unuse_servant(poa,
		    &invoke_rec, opname, cookie, &oid, servant, ev);
	    /* NOTE: if the ServantManager raises an exception,
	     * then weird stuff will occur! 
	     */
    }
    g_assert( poa->orb->poa_current_invokations == &invoke_rec );
    poa->orb->poa_current_invokations = invoke_rec.prev;
    --(pobj->use_cnt);
    --(poa->use_cnt);
    if ( pobj->life_flags & ORBit_LifeF_NeedPostInvoke )
	ORBit_POAObject_post_invoke(pobj);
    /* WATCHOUT: dont touch pobj beyond here -- it may be gone! */

errout:
    if(ev->_major == CORBA_SYSTEM_EXCEPTION) {
	    GIOPSendBuffer *reply_buf;
	    reply_buf = giop_send_reply_buffer_use(
		    GIOP_MESSAGE_BUFFER(recv_buffer)->connection,
		    NULL,
		    recv_buffer->message.u.request.request_id,
		    CORBA_SYSTEM_EXCEPTION);
	     ORBit_send_system_exception(reply_buf, ev);
	     giop_send_buffer_write(reply_buf);
	     giop_send_buffer_unuse(reply_buf);
    } else {
	    /* User exceptions are handled in the skels! */
	    g_assert(ev->_major == CORBA_NO_EXCEPTION);
    }

    giop_recv_buffer_unuse(recv_buffer);
    CORBA_exception_free(ev);
}

void
ORBit_POA_handle_held_requests(PortableServer_POA poa) {
    GSList	*reqlist = poa->held_requests, *req;
    poa->held_requests = 0;
    /* zero out the held_requests first, because a given request may
     * get re-queued below.
     */
    for ( req=reqlist; req; req=req->next) {
	GIOPRecvBuffer *buf = req->data;
	ORBit_POA_handle_request(poa, buf);
    }
    g_slist_free(reqlist);
}

/**
    This function is invoked from the generated stub code, after
    invoking the servant method.
**/
void
ORBit_POAObject_post_invoke(ORBit_POAObject *pobj) {
    if ( pobj->use_cnt > 0 )
    	return;
    if ( pobj->life_flags & ORBit_LifeF_DeactivateDo ) {
	/* NOTE that the "desired" values of etherealize and cleanup
	 * are stored in pobj->life_flags and they dont need
	 * to be passed in again!
	 */
	ORBit_POA_deactivate_object(pobj->poa, pobj, /*ether*/0, /*cleanup*/0);
    	/* WATCHOUT: pobj may not exist anymore! */
    }
}

#ifdef NOT_BACKWARDS_COMPAT_0_4
void ORBit_servant_set_deathwatch(PortableServer_ServantBase *servant,
				  int *use_count,
                                  GFunc death_func,
                                  gpointer user_data)
{
	ORBit_POAObject *pobj;

	pobj = ORBIT_OBJECT_KEY(servant->_private)->object;

	pobj->use_count = use_count;
	pobj->death_callback = death_func;
	pobj->user_data = user_data;
}
#endif



/*************************************************************************
 *
 *		POAObject	creation and destruction
 *
 *************************************************************************/

/**
	Most routines in here do not raise exceptions. Instead, they
	assert. It is up to the caller to verify conditions before
	invoking.  We do it this way because cleaning up half-way through
	is tedious. Also, the caller knows more about what needs to be
	checked, and what has already been checked.

	PAOObject. A PAOObject is one-to-one with object ids.  A given
	POAObject may be active or not-active. It may have zero or
	more references that refer to it as their bypass_obj. Under
	MULTIPLE_ID policy, several POAObject's may reference the same
	servant.  A POAObject exists (has memory allocated and is in
	the poa->oid_to_obj_map) if and only if:
		a) It is active (pobj->servant != 0); and/or,
		b) One or more CORBA_Object's refer to it as bypass_obj.
	Note that the application is never given a pointer to a POAObject.

	The spec is somewhat complicated regarding object deactivate.
	On one hand, it says that the object should be removed from the
	active object map before etherealization. On the other hand,
	requests to re-activate (explicit or implicit) must block (or,
	if generated locally, fail). I dont know if I honour this.

	It would be nice to get rid of its "poa" field. I beleive this is
	possible except when releasing it as a bypass_obj. In that case
	(and only that case), we dont know what poa it belongs to.
**/

static guint
ORBit_ObjectId_sysid_hash(const PortableServer_ObjectId *oid) {
    /* g_assert( oid->_length >= sizeof(ORBit_POA_Serial) ); */
    return *(ORBit_POA_Serial*)oid->_buffer;
}





static void
ORBit_POAObject_free_fn(gpointer obj_in) {
    ORBit_POAObject *pobj = obj_in;
    PortableServer_POA poa = pobj->poa;
    if ( pobj->object_id ) {
        g_hash_table_remove(poa->oid_to_obj_map, pobj->object_id);
        CORBA_free(pobj->object_id);
    }
    g_free(pobj);
    ORBit_RootObject_release(poa);
}


static const ORBit_RootObject_Interface ORBit_POAObject_epv = {
    ORBIT_ROT_POAOBJECT,
    ORBit_POAObject_free_fn
};


/**
    Forms a SYSTEM_ID ObjectId using either RETAIN or NON_RETAIN policy.
    Under RETAIN, the object's random key is generated here as part of the oid.
**/
void
ORBit_POA_make_sysoid(PortableServer_POA poa, PortableServer_ObjectId *oid)
{
    int	randlen;
    g_assert( poa->id_assignment == PortableServer_SYSTEM_ID );
    randlen = poa->servant_retention == PortableServer_RETAIN
      ? poa->obj_rand_len : 0;
    oid->_length = oid->_maximum = sizeof(ORBit_POA_Serial) + randlen;
    oid->_buffer = CORBA_octet_allocbuf(oid->_length);
    oid->_release = CORBA_TRUE;
    *(ORBit_POA_Serial*)(oid->_buffer) = ++(poa->next_sysid);
    if ( randlen > 0 ) {
	ORBit_genrand_buf( poa->orb->genrand, oid->_buffer+sizeof(ORBit_POA_Serial), randlen);
    }
}


/*
    If USER_ID policy, {oid} must be non-NULL.
    If SYSTEM_ID policy, {oid} must ether be NULL, or must have
    been previously created by the POA. If the user passes in
    a bogus oid under SYSTEM_ID, we will assert or segfault. This
    is allowed by the CORBA spec.
*/
ORBit_POAObject*
ORBit_POA_create_object(PortableServer_POA poa, 
		PortableServer_ObjectId *oid, gboolean asDefault,
		CORBA_Environment *ev) 
{
    ORBit_POAObject *newobj;

    newobj = g_new0(ORBit_POAObject, 1);
    ORBit_RootObject_init(&newobj->parent, &ORBit_POAObject_epv);
    newobj->poa = ORBit_RootObject_dup(poa);

    if ( asDefault ) {
	/* It would be nice if we could mark this in some way... */
    	g_assert(poa->request_processing == PortableServer_USE_DEFAULT_SERVANT);
    } else {
	g_assert( poa->servant_retention == PortableServer_RETAIN );

	newobj->object_id = CORBA_sequence_octet__alloc();
	if ( poa->id_assignment == PortableServer_SYSTEM_ID ) {
	    if ( oid ) {
		g_assert( oid->_length 
		  == sizeof(ORBit_POA_Serial) + poa->obj_rand_len );
		ORBit_sequence_octet_copy(newobj->object_id, oid);
	    } else {
		ORBit_POA_make_sysoid(poa, newobj->object_id);
	    }
	} else {
	    /* USER_ID */
	    ORBit_sequence_octet_copy(newobj->object_id, oid);
	}
	g_hash_table_insert(poa->oid_to_obj_map, newobj->object_id, newobj);
    }
    return newobj;
}


/*
    Normally this is called for normal servants in RETAIN mode. 
    However, it may also be invoked on the default servant when
    it is installed. In this later case, it may be either RETAIN
    or NON_RETAIN.
*/
void
ORBit_POA_activate_object(PortableServer_POA poa, 
		ORBit_POAObject *pobj,
		PortableServer_ServantBase *servant, 
		CORBA_Environment *ev) 
{
    g_assert( pobj->servant == 0 );	/* must not be already active */
    g_assert( (poa->life_flags & ORBit_LifeF_DeactivateDo) == 0 );
    g_assert( pobj->use_cnt == 0 );
    /* XXX: above should be an exception? */
    g_assert( servant->_private == 0 );	/* its POAObject */
    {
	PortableServer_ClassInfo *ci = ORBIT_SERVANT_TO_CLASSINFO(servant);
	if ( ci->vepvmap==0 ) {
	    ci->vepvmap 
	      = g_new0(ORBit_VepvIdx, ORBit_class_assignment_counter+1);
	    ci->vepvlen = ORBit_class_assignment_counter + 1;
	    ci->init_vepvmap(ci->vepvmap);
	}
#  ifdef ORBIT_BYPASS_MAPCACHE
	pobj->vepvmap_cache = ci->vepvmap;
#  endif
    }
    pobj->servant = servant;
    if ( pobj->object_id==0 /* this occurs for the default servant */
      || poa->id_uniqueness == PortableServer_UNIQUE_ID ) {
        servant->_private = pobj;
    }
    ORBit_RootObject_dup(pobj);
}


/**
    Note that this doesn't necessarily remove the object from
    the oid_to_obj_map; it just removes knowledge of the servant.
    If object is currentin use (servicing a request), etherialization
    and memory release will occur later.
**/
void
ORBit_POA_deactivate_object(PortableServer_POA poa, ORBit_POAObject *pobj,
			     CORBA_boolean do_etherealize,
			     CORBA_boolean is_cleanup)
{
    PortableServer_ServantBase	*serv = pobj->servant;

    /* this fnc is also called for the default servant in NON_RETAIN */
    /* g_assert( poa->servant_retention == PortableServer_RETAIN ); */

    if ( (serv=pobj->servant)==0 ) {
	    /* deactivation has already occured, or is in progress */
	    return;
    }
    if ( do_etherealize && (pobj->life_flags&ORBit_LifeF_DeactivateDo)==0 )
    	pobj->life_flags |= ORBit_LifeF_DoEtherealize;
    if ( is_cleanup )
    	pobj->life_flags |= ORBit_LifeF_IsCleanup;
    if ( pobj->use_cnt > 0 ) {
        pobj->life_flags |= ORBit_LifeF_DeactivateDo;
	pobj->life_flags |= ORBit_LifeF_NeedPostInvoke;
	return;
    }
    pobj->servant = 0;
    serv->_private = 0;

    if ( (pobj->life_flags & ORBit_LifeF_DoEtherealize)!=0 ) {
	CORBA_Environment env, *ev = &env;
	CORBA_exception_init(ev);
	++(pobj->use_cnt);	/* prevent re-activation */
        if ( poa->request_processing == PortableServer_USE_SERVANT_MANAGER) {
	    POA_PortableServer_ServantActivator__epv *epv;
	    POA_PortableServer_ServantActivator *sm;

	    sm = (POA_PortableServer_ServantActivator *)poa->servant_manager;
	    epv = sm->vepv->PortableServer_ServantActivator_epv;
	    (*(epv->etherealize))(sm, pobj->object_id, poa,
			    serv,
			    (pobj->life_flags & ORBit_LifeF_IsCleanup)
			      ? CORBA_TRUE : CORBA_FALSE,
			    /* remaining_activations */ CORBA_FALSE,
			    ev);
        }
        {
	    PortableServer_ServantBase__epv *epv = serv->vepv[0];
	    /* In theory, the finalize fnc should always be non-NULL;
	     * however, for backward compat. and general extended
	     * applications we dont insist on it.
	     */
	    if ( epv && epv->finalize ) {
	        (*(epv->finalize))(serv, ev);
	    }
	}
	--(pobj->use_cnt);	/* allow re-activation */
	g_assert( ev->_major == 0 );
    }

    /* this will enable the POAObj to be removed from the oidobjmap
     * and the memory reclaimed, once there is no objref to it.
     */
    pobj->life_flags &= ~(ORBit_LifeF_DeactivateDo
      |ORBit_LifeF_IsCleanup|ORBit_LifeF_DoEtherealize);
    ORBit_RootObject_release(pobj);
}

/***************************************************************************
 *
 *			POA creation, deletion, helpers
 *
 ***************************************************************************/

#if 0
static void
ORBit_POA_dump(PortableServer_POA poa) {
}
#endif

static void
ORBit_POA_set_policy(PortableServer_POA poa,
		     CORBA_Policy policy,
		     CORBA_Environment *ev)
{
	switch(policy->policy_type) {
	case PortableServer_THREAD_POLICY_ID:
		poa->thread = ((PortableServer_ThreadPolicy)policy)->value;
		break;
	case PortableServer_LIFESPAN_POLICY_ID:
		poa->lifespan = ((PortableServer_LifespanPolicy)policy)->value;
		break;
	case PortableServer_ID_UNIQUENESS_POLICY_ID:
		poa->id_uniqueness = ((PortableServer_IdUniquenessPolicy)policy)->value;
		break;
	case PortableServer_ID_ASSIGNMENT_POLICY_ID:
		poa->id_assignment = ((PortableServer_IdAssignmentPolicy)policy)->value;
		break;
	case PortableServer_IMPLICIT_ACTIVATION_POLICY_ID:
	  poa->implicit_activation = ((PortableServer_ImplicitActivationPolicy)policy)->value;
		break;
	case PortableServer_SERVANT_RETENTION_POLICY_ID:
	  poa->servant_retention = ((PortableServer_ServantRetentionPolicy)policy)->value;
		break;
	case PortableServer_REQUEST_PROCESSING_POLICY_ID:
		poa->request_processing = ((PortableServer_ServantRetentionPolicy)policy)->value;
		break;
	case ORBit_PortableServer_OKEYRAND_POLICY_ID:
		poa->poa_rand_len = 
		  ((ORBit_PortableServer_OkeyrandPolicy)policy)->poa_rand_len;
		poa->obj_rand_len = 
		  ((ORBit_PortableServer_OkeyrandPolicy)policy)->obj_rand_len;
		break;
	default:
		g_warning("Unknown policy type, cannot set it on this POA");
	}
}


static void
ORBit_POA_check_policy_conflicts(PortableServer_POA poa,
		     CORBA_Environment *ev)
{
    gboolean bad = 0;
    if ( poa->lifespan == PortableServer_TRANSIENT ) {
        if ( poa->poa_rand_len < 0 ) poa->poa_rand_len = ORBIT_RAND_KEY_LEN;
        if ( poa->obj_rand_len < 0 ) poa->obj_rand_len = ORBIT_RAND_KEY_LEN;
    }
    if ( poa->lifespan == PortableServer_PERSISTENT ) {
        if ( poa->poa_rand_len < 0 ) poa->poa_rand_len = 0;
        if ( poa->obj_rand_len < 0 ) poa->obj_rand_len = 0;
        if ( poa->poa_rand_len!=0 || poa->obj_rand_len!=0 )
	    bad = 1;
        if ( poa->orb->cnx.ipv4==0 || !poa->orb->cnx.ipv4_isPersistent ) {
	    g_error("PERSISTENT POAs require -ORBIPv4Port option.");
	    bad = 1;
	}
    }

  /* Check for those policy combinations that aren't allowed */
  if ( bad ||
      (poa->servant_retention == PortableServer_NON_RETAIN &&
       poa->request_processing == PortableServer_USE_ACTIVE_OBJECT_MAP_ONLY) ||
      
      (poa->request_processing == PortableServer_USE_DEFAULT_SERVANT &&
       poa->id_uniqueness == PortableServer_UNIQUE_ID) ||
      
      (poa->implicit_activation == PortableServer_IMPLICIT_ACTIVATION &&
       (poa->id_assignment == PortableServer_USER_ID ||
	poa->servant_retention == PortableServer_NON_RETAIN))
      )
    {
      CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			  ex_PortableServer_POA_InvalidPolicy,
			  NULL);
    }
}
      

static void
ORBit_POA_set_policylist(PortableServer_POA poa,
			 CORBA_PolicyList *policies,
			 CORBA_Environment *ev)
{
	CORBA_unsigned_long i;

	for(i = 0; i < policies->_length; i++) {
		if(ev->_major != CORBA_NO_EXCEPTION)
			break;
		ORBit_POA_set_policy(poa, policies->_buffer[i], ev);
	}
}

/**
	While a POA and its children hold cross-linked pointers,
	it is the child that affects the reference count of its
	parent.
**/
void
ORBit_POA_add_child(PortableServer_POA poa,
		    PortableServer_POA child_poa,
		    CORBA_Environment *ev)

{
	g_assert( child_poa->parent_poa == 0 );
	child_poa->parent_poa = ORBit_RootObject_dup(poa);
	poa->child_poas = g_slist_prepend(poa->child_poas, child_poa);
}

void
ORBit_POA_remove_child(PortableServer_POA poa,
		       PortableServer_POA child_poa,
		       CORBA_Environment *ev)
{
	g_assert( child_poa->parent_poa == poa );
	poa->child_poas = g_slist_remove(poa->child_poas, child_poa);
	child_poa->parent_poa = 0;
	ORBit_RootObject_release(poa);
}


typedef struct TraverseInfo {
    int			num_in_use;
    gboolean		do_deact;
    PortableServer_POA	poa;
    CORBA_boolean	do_etherealize;
    CORBA_boolean	is_cleanup;
} TraverseInfo;

static void
traverse_cb(PortableServer_ObjectId *oid, ORBit_POAObject *pobj, 
  TraverseInfo *info)
{
    if ( pobj->use_cnt > 0 ) {
	++(info->num_in_use);
    }
    if ( info->do_deact ) {
        ORBit_POA_deactivate_object(info->poa, pobj,
			     info->do_etherealize,
			     info->is_cleanup);
    }
}

CORBA_boolean
ORBit_POA_is_inuse(PortableServer_POA poa, CORBA_boolean consider_children,
  CORBA_Environment *ev) {
    GSList *child;
    if ( poa->use_cnt > 0 ) 
    	return CORBA_TRUE;
    if ( consider_children ) {
	for ( child=poa->child_poas; child; child=child->next) {
	    PortableServer_POA cpoa = child->data;
	    if ( ORBit_POA_is_inuse(cpoa, /*kids*/1, ev) )
		return CORBA_TRUE;
	}
    }
    if ( poa->oid_to_obj_map ) {
    	TraverseInfo	info;
	info.do_deact = 0;
	info.num_in_use = 0;
	g_hash_table_foreach(poa->oid_to_obj_map, (GHFunc)traverse_cb, &info);
    	if ( info.num_in_use )
    	    return CORBA_TRUE;
    }
    return CORBA_FALSE;
}

static void
ORBit_POA_set_life(PortableServer_POA poa, 
  CORBA_boolean etherealize_objects, int action_do) {
    if ( (poa->life_flags 
      & (ORBit_LifeF_DeactivateDo|ORBit_LifeF_DestroyDo))==0 ) {
    	if ( etherealize_objects ) {
    	    poa->life_flags |= ORBit_LifeF_DoEtherealize;
        }
    }
    poa->life_flags |= action_do;
}

/**
    Returns TRUE if POA (and all its objects) are sucessfully
    deactivated (and optionally etherealized). Returns FALSE
    if this cannot be performed because some object is currently
    in-use servicing some request. Note that deactivating
    has no affect of children POAs.
**/
CORBA_boolean
ORBit_POA_deactivate(PortableServer_POA poa, CORBA_boolean etherealize_objects,
  CORBA_Environment *ev) {
    CORBA_boolean	done = CORBA_TRUE;

    ORBit_POA_set_life(poa, etherealize_objects, ORBit_LifeF_DeactivateDo);
    if ( poa->life_flags & ORBit_LifeF_Deactivated )
	return TRUE;	/* already did it */
    if ( poa->life_flags & ORBit_LifeF_Deactivating )
	return FALSE;	/* recursion */
    poa->life_flags |= ORBit_LifeF_Deactivating;

    /* bounce all pending requested (OBJECT_NOT_EXIST
     * exceptions raised); none should get requeued. */
    ORBit_POA_handle_held_requests(poa);
    g_assert( poa->held_requests == 0 );

    if ( poa->servant_retention == PortableServer_RETAIN ) {
	TraverseInfo	info;
	info.num_in_use = 0;
	info.do_deact = 1;
	info.poa = poa;
	info.do_etherealize = (poa->life_flags&ORBit_LifeF_DoEtherealize)
		  		?CORBA_TRUE:CORBA_FALSE;
	info.is_cleanup = TRUE;
	g_hash_table_freeze(poa->oid_to_obj_map);
    	g_assert( poa->oid_to_obj_map );
	g_hash_table_foreach(poa->oid_to_obj_map, (GHFunc)traverse_cb, &info);
    	done = info.num_in_use == 0 ? CORBA_TRUE : CORBA_FALSE;
    }
    if ( done )
    	poa->life_flags |= ORBit_LifeF_Deactivated;
    poa->life_flags &= ~ORBit_LifeF_Deactivating;
    return done;
}

/**
    Destroy implies deactivate, but also makes the POA's name
    re-usable. The affect of children POAs is different between
    deactivate and destroy.
    Returns FALSE if cannot destroy because some
    object is in-use servicing some request; otherwise TRUE.
    The poa will be removed from its parent's list if and only if
    it returns TRUE.
**/
CORBA_boolean
ORBit_POA_destroy(PortableServer_POA poa, CORBA_boolean etherealize_objects,
  CORBA_Environment *ev) {
    int	cpidx;
    int numobjs, totrefs;

    ORBit_POA_set_life(poa, etherealize_objects, ORBit_LifeF_DestroyDo);
    if ( poa->life_flags & ORBit_LifeF_Destroyed )
        return TRUE;	/* already did it */
    if ( poa->life_flags & (ORBit_LifeF_Deactivating|ORBit_LifeF_Destroying) )
	return FALSE;	/* recursion */
    poa->life_flags |= ORBit_LifeF_Destroying;

    /* Destroying the children is tricky, b/c they may die
     * while we are traversing. We traverse over the
     * ORB's global list (rather than poa->child_poas) 
     * to avoid walking into dead children.
     */
    for (cpidx=0; cpidx < poa->orb->poas->len; cpidx++ ) {
    	PortableServer_POA cpoa = g_ptr_array_index(poa->orb->poas, cpidx);
	if ( cpoa && cpoa->parent_poa == poa ) {
            ORBit_POA_destroy(cpoa, etherealize_objects, ev);
        }
    }

    /* Get rid of our default servant, if we have one.
     * This usage is non-standard.
     * It does not make any attempt to etherialize the servant.
     */
    PortableServer_POA_set_servant(poa, NULL, ev);

    /*
     * Get rid of all our RETAINed children.
     */
    if ( poa->child_poas 
      || poa->use_cnt
      || !ORBit_POA_deactivate(poa, etherealize_objects, ev) ) {
	poa->life_flags &= ~ORBit_LifeF_Destroying;
    	return CORBA_FALSE;
    }

    /* We're commited at this point.
     * Remove links so POA's name can be re-used. Most memory
     * will be free'd up during POA_release. */

    if ( poa->the_POAManager ) {
        ORBit_POAManager_unregister_poa(poa->the_POAManager, poa, ev);
    }
    if ( poa->parent_poa ) {
    	ORBit_POA_remove_child(poa->parent_poa, poa, ev);
    }
    if ( poa->orb && poa->poaID >= 0 ) {
	g_ptr_array_index(poa->orb->poas, poa->poaID) = NULL;
	poa->poaID = -1;
    }

    /* each objref holds a POAObj, and each POAObj holds a ref 
     * to the POA. In addition, the app can hold open refs
     * to the POA itself.
     */
    numobjs = poa->oid_to_obj_map ? g_hash_table_size(poa->oid_to_obj_map) : 0;
    totrefs = poa->parent.refs;
    g_assert( totrefs > numobjs );
    if ( poa->orb && poa == poa->orb->root_poa ) {
	/* The ORB still has refs to the RootPOA, and must hold onto
	 * it for various reasons. We check for remaining refs to
	 * the RootPOA within the ORB code later.
	 */
    	totrefs = numobjs+1;	/* HACK */
    }
    if ( totrefs > 1 ) {
    	g_warning("POA_do_destroy: Application still has "
	  "%d refs to POA %s and %d refs to objects within the POA.",
	  totrefs - 1 - numobjs, poa->the_name, numobjs);
    }
    poa->life_flags |= ORBit_LifeF_Destroyed;
    poa->life_flags &= ~ORBit_LifeF_Destroying;
    ORBit_RootObject_release(poa);
    return CORBA_TRUE;
}

/**
    In theory, it is impossible for this to get called until after
    the POA_destroy() is complete; this is because the POA holds
    a reference to itself until then. Once the POA is destroyed,
    and before it is released, the only thing we support doing
    is throwing exceptions. Thus pretty much everything else has
    already been cleaned up.
**/
static void
ORBit_POA_free_fn(gpointer obj_in)
{
    PortableServer_POA poa = obj_in;
    g_assert( poa->life_flags & ORBit_LifeF_Destroyed );
    if ( poa->oid_to_obj_map ) {
    	g_assert( g_hash_table_size(poa->oid_to_obj_map) == 0 );
    	g_hash_table_destroy(poa->oid_to_obj_map);
    }

    g_assert( poa->child_poas == 0 );
    g_assert( poa->parent_poa == 0 );
    if ( poa->the_name )
    	CORBA_free(poa->the_name);

    ORBit_sequence_octet_fini(&poa->poa_key);
    ORBit_RootObject_release(poa->orb);
    poa->orb = 0;
    g_free(poa);
}


static const ORBit_RootObject_Interface ORBit_POA_epv = {
	ORBIT_ROT_POA,
	ORBit_POA_free_fn
};

/**
    This returns a POA psuedo-object. Its reference count is 2:
    one ref is for the caller (since we are returning an objref),
    and one count is used internally to keep track of the POA being
    alive or not.
**/
PortableServer_POA 
ORBit_POA_new(CORBA_ORB orb,
	      CORBA_char *adapter_name,
	      PortableServer_POAManager the_POAManager,
	      CORBA_PolicyList *policies,
	      CORBA_Environment *ev)
{
    PortableServer_POA poa;

    /* Create the object */
    poa = (PortableServer_POA) g_new0(struct PortableServer_POA_type, 1); 
    if(poa == NULL) {
	    CORBA_exception_set_system(ev, ex_CORBA_NO_MEMORY, CORBA_COMPLETED_NO);
	    goto error;
    }
    poa->poaID = -1;

    ORBit_RootObject_init(&poa->parent, &ORBit_POA_epv);
    ORBit_RootObject_dup(poa);		/* released by POA_destroy() */

    /* If no POAManager was specified, create one */
    if(the_POAManager == NULL) {
      the_POAManager = ORBit_POAManager_new(orb, ev);
    }	  
    if(ev->_major != CORBA_NO_EXCEPTION) goto error;

    /* Register this poa with the poa manager (it it will do cross-link) */
    ORBit_POAManager_register_poa(the_POAManager,poa,ev);

    /* Initialise the child poas table */
    poa->child_poas = NULL;      /* initialise the slist */

    poa->held_requests = NULL;

    poa->orb = ORBit_RootObject_dup((ORBit_RootObject)orb);
    poa->poaID = orb->poas->len;
    g_ptr_array_set_size(orb->poas, orb->poas->len + 1);
    g_ptr_array_index(orb->poas, poa->poaID) = poa;


    /* Need to initialise poa policies etc.. here */
    poa->thread = PortableServer_ORB_CTRL_MODEL;
    poa->lifespan = PortableServer_TRANSIENT;
    poa->id_uniqueness = PortableServer_UNIQUE_ID;
    poa->id_assignment = PortableServer_SYSTEM_ID;
    poa->servant_retention = PortableServer_RETAIN;
    poa->request_processing = PortableServer_USE_ACTIVE_OBJECT_MAP_ONLY;
    poa->implicit_activation = PortableServer_NO_IMPLICIT_ACTIVATION;
    poa->obj_rand_len = -1;
    poa->poa_rand_len = -1;
    if (policies) {
      ORBit_POA_set_policylist(poa, policies, ev);
      if(ev->_major != CORBA_NO_EXCEPTION) goto error;
    }
    /* check_policy also sets up some defaults, so need to always call */
    ORBit_POA_check_policy_conflicts(poa, ev);
    if(ev->_major != CORBA_NO_EXCEPTION) goto error;

    /* policy-dependent initialization */
    poa->the_name = CORBA_string_dup(adapter_name);
    ORBit_sequence_octet_init(&poa->poa_key, 
      sizeof(CORBA_long) + poa->poa_rand_len);
    *(CORBA_long*)(poa->poa_key._buffer) = poa->poaID;
    if ( poa->poa_rand_len ) {
	    ORBit_genrand_buf( poa->orb->genrand, poa->poa_key._buffer + sizeof(CORBA_long), 
	      poa->poa_rand_len);
    }

    if( poa->servant_retention == PortableServer_RETAIN ) {
	if ( poa->id_assignment == PortableServer_SYSTEM_ID ) {
	    poa->oid_to_obj_map = g_hash_table_new(
			(GHashFunc)ORBit_ObjectId_sysid_hash,
			(GCompareFunc)ORBit_sequence_octet_equal);
	} else { /* USER_ID */
	    poa->oid_to_obj_map = g_hash_table_new(
			(GHashFunc)ORBit_sequence_octet_hash,
			(GCompareFunc)ORBit_sequence_octet_equal);
	}
    }
    if ( poa->id_assignment == PortableServer_USER_ID 
      && poa->obj_rand_len > 0 ) {
	poa->num_to_koid_map = g_ptr_array_new();
	g_ptr_array_set_size(poa->num_to_koid_map, 1);
	g_ptr_array_index(poa->num_to_koid_map, 0) = NULL;
    }

    /* caller must release returned objref */
    return ORBit_RootObject_dup(poa);

error:
    if(poa != NULL){
	    CORBA_boolean done;
	    done = ORBit_POA_destroy(poa, /*etherealize*/0, /*ev*/NULL);
    	    g_assert(done);
    }
    return NULL;
}
