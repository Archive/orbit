/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  ORBit: A CORBA v2.2 ORB
 *
 *  Copyright (C) 1998 Richard H. Porter and Red Hat Software
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Phil Dawes <philipd@parallax.co.uk>
 *  Author: Kennard White <kennard@berkeley.innomedia.com>
 *
 */

/*
 *   ORBit specific POA funcitons.
 *
 */

#ifndef _ORBIT_ORBIT_POA_TYPE_H_
#define _ORBIT_ORBIT_POA_TYPE_H_

#define ORBit_LifeF_NeedPostInvoke	(1<<0)
#define ORBit_LifeF_DoEtherealize	(1<<1)
#define ORBit_LifeF_IsCleanup		(1<<2)
#define ORBit_LifeF_DeactivateDo	(1<<4)
#define ORBit_LifeF_Deactivating	(1<<5)
#define ORBit_LifeF_Deactivated		(1<<6)
#define ORBit_LifeF_DestroyDo		(1<<8)
#define ORBit_LifeF_Destroying		(1<<9)
#define ORBit_LifeF_Destroyed		(1<<10)



typedef void (*ORBitSkeleton)(PortableServer_ServantBase *_ORBIT_servant,
			      gpointer _ORBIT_recv_buffer,
			      CORBA_Environment *ev,
			      gpointer implementation);
typedef ORBitSkeleton (*ORBit_impl_finder)(PortableServer_ServantBase *servant,
				    gpointer _ORBIT_recv_buffer,
				    gpointer *implementation);
typedef void (*ORBit_local_objref_init)(CORBA_Object obj,
					PortableServer_ServantBase *servant);
typedef short ORBit_VepvIdx;
typedef void (*ORBit_vepvmap_init)(ORBit_VepvIdx *vepvmap);
typedef struct {
	ORBit_impl_finder	relay_call;
	const gchar		*class_name;
	CORBA_unsigned_long	*class_id;
	ORBit_vepvmap_init	init_vepvmap;
	ORBit_VepvIdx*		vepvmap;
	int			vepvlen;
} PortableServer_ClassInfo;

#define ORBIT_RAND_KEY_LEN 8	/* default length */

typedef unsigned long ORBit_POA_Serial;

typedef struct ORBit_POAKOid_struct {
	short			oid_length;
	/* oid octets comes here */
	/* rand_data octets come here */
} ORBit_POAKOid;
#define ORBIT_POAKOID_OidLenOf(koid) \
	((koid)->oid_length)
#define ORBIT_POAKOID_OidBufOf(koid) \
	( (CORBA_octet*) (((char*)(koid))+sizeof(*(koid))) )
#define ORBIT_POAKOID_RandBufOf(koid) \
	( (CORBA_octet*) (((char*)(koid))+sizeof(*(koid))+(koid)->oid_length) )

typedef struct ORBit_POAObject_struct {
	struct ORBit_RootObject_struct	parent;
	PortableServer_ObjectId*	object_id;
	PortableServer_Servant		servant;
	PortableServer_POA		poa;
	short				use_cnt; /* method invokations */
	short				life_flags;
#ifdef NOT_BACKWARDS_COMPAT_0_4
	/* Stuff for doing shared libraries nicely */
	int *use_count;
	GFunc death_callback;
	gpointer user_data;
#endif
#ifdef ORBIT_BYPASS_MAPCACHE
	ORBit_VepvIdx*	vepvmap_cache;
#endif
} ORBit_POAObject;

typedef struct ORBit_POAInvokation_struct ORBit_POAInvokation;
struct ORBit_POAInvokation_struct {
    ORBit_POAInvokation*		prev;
    ORBit_POAObject*			pobj;
    PortableServer_ObjectId*		oid;
    char				doUnuse;
};
/* The pointer to the deepest-nested invokation is kept in the ORB */

#define ORBIT_SERVANT_SET_CLASSINFO(servant,ci) { 			\
  ((PortableServer_ServantBase *)(servant))->vepv[0]->_private = (ci);	\
}
#define ORBIT_SERVANT_TO_CLASSINFO(servant) ( 				\
  (PortableServer_ClassInfo*) 						\
  ( ((PortableServer_ServantBase *)(servant))->vepv[0]->_private )	\
)
#define ORBIT_SERVANT_TO_POAOBJECT(servant) (				\
  (ORBit_POAObject*) 							\
  ( ((PortableServer_ServantBase *)(servant))->_private )		\
)
#define ORBIT_SERVANT_MAJOR_TO_EPVPTR(servant,major) 			\
  ( ((PortableServer_ServantBase *)(servant))->vepv[major] )

#ifdef ORBIT_BYPASS_MAPCACHE
#define ORBIT_POAOBJECT_TO_EPVIDX(pobj,clsid) \
  ( (pobj)->vepvmap_cache[(clsid)] )
#else
#define ORBIT_POAOBJECT_TO_EPVIDX(pobj,clsid) \
  ( ORBIT_SERVANT_TO_CLASSINFO((pobj)->servant)->vepvmap[(clsid)] )
#endif
#define ORBIT_POAOBJECT_TO_EPVPTR(pobj,clsid) \
  ORBIT_SERVANT_MAJOR_TO_EPVPTR((pobj)->servant, \
    ORBIT_POAOBJECT_TO_EPVIDX((pobj),(clsid)) )

#define ORBIT_SERVANT_TO_EPVIDX(servant,clsid) \
  ( ORBIT_SERVANT_TO_CLASSINFO(servant)->vepvmap[(clsid)] )

#define ORBIT_SERVANT_TO_EPVPTR(servant,clsid) \
  ORBIT_SERVANT_MAJOR_TO_EPVPTR((servant), \
    ORBIT_SERVANT_TO_EPVIDX((servant),(clsid)) )

/***************** begin-of stub/skel macros */

#define ORBIT_SERVANT_TO_ORB(servant) \
  ( ORBIT_SERVANT_TO_POAOBJECT(servant)->poa->orb )

#define ORBIT_STUB_IsBypass(obj,clsid) \
  ( (obj)->bypass_obj && (obj)->bypass_obj->servant && (clsid) )
#define ORBIT_STUB_GetPoaObj(obj) \
  ( (obj)->bypass_obj )
#define ORBIT_STUB_GetServant(obj) \
  ( (obj)->bypass_obj->servant )
	
#define ORBIT_STUB_PreCall(obj, iframe) {		\
  ++( (obj)->bypass_obj->use_cnt );			\
  iframe.pobj = (obj)->bypass_obj;			\
  iframe.oid = 0;					\
  iframe.prev = (obj)->orb->poa_current_invokations;	\
  (obj)->orb->poa_current_invokations = &iframe;	\
}

#define ORBIT_STUB_PostCall(obj, iframe) {				\
  (obj)->orb->poa_current_invokations = iframe.prev;			\
  --( (obj)->bypass_obj->use_cnt );					\
  if ( (obj)->bypass_obj->life_flags & ORBit_LifeF_NeedPostInvoke )	\
	ORBit_POAObject_post_invoke( (obj)->bypass_obj);		\
}


#define ORBIT_STUB_GetEpv(obj,clsid) \
  ORBIT_POAOBJECT_TO_EPVPTR( (obj)->bypass_obj, (clsid))

/***************** end-of stub/skel macros */


#define ORBit_PortableServer_OKEYRAND_POLICY_ID (CORBA_VPVID_ORBit0|1)
struct ORBit_PortableServer_OkeyrandPolicy_type {
	struct CORBA_Policy_type corba_policy;
	CORBA_short		poa_rand_len;
	CORBA_short		obj_rand_len;
};
typedef struct ORBit_PortableServer_OkeyrandPolicy_type *ORBit_PortableServer_OkeyrandPolicy;

struct PortableServer_POA_type {
	struct ORBit_RootObject_struct parent;
	int life_flags;	/* see LifeF_ above */
	int use_cnt;	/* method invokations */

	PortableServer_POA parent_poa;
	CORBA_ORB orb;
	CORBA_unsigned_long poaID;
	PortableServer_ObjectId	poa_key; /* poaID + rand_data */

	ORBit_POA_Serial next_sysid;
	GHashTable *oid_to_obj_map;	/* active or not: oid to POAObject */
	GPtrArray *num_to_koid_map;	/* okeys to koids */

	/* Requests received while in a HOLDING state */
	GSList *held_requests;

	/* this'll be a hash table when I can be arsed to look up 
	   how to implement efficient hash tables  - Phil.*/
	GSList *child_poas;     
	
	CORBA_char *the_name;
	PortableServer_POAManager the_POAManager;

	PortableServer_AdapterActivator the_activator;

	PortableServer_ServantManager servant_manager;
	ORBit_POAObject *default_pobj;

	PortableServer_ThreadPolicyValue thread;
	PortableServer_LifespanPolicyValue lifespan;
	PortableServer_IdUniquenessPolicyValue id_uniqueness;
	PortableServer_IdAssignmentPolicyValue id_assignment;
	PortableServer_ImplicitActivationPolicyValue implicit_activation;
	PortableServer_ServantRetentionPolicyValue servant_retention;
	PortableServer_RequestProcessingPolicyValue request_processing;
	int obj_rand_len;
	int poa_rand_len;
	int koid_rand_len;
};

/*
    These are implemented in server.c; I'm not sure where they 
    should really be implemented or prototype!.
*/

typedef struct ORBit_SrvForwBind {
    CORBA_sequence_octet	objkey;
    CORBA_Object		objref;
    CORBA_sequence_octet	to_okey;	// local POA
} ORBit_SrvForwBind;
extern void ORBit_ORB_forw_bind( CORBA_ORB orb, 
			CORBA_sequence_octet *okey,
			CORBA_Object oref, CORBA_Environment *ev);
extern void ORBit_ORB_forw_unbind_all(CORBA_ORB orb);


/* should really be moved somewhere else... */
struct CORBA_ServerRequest_type {
	struct ORBit_RootObject_struct parent;
	GIOPRecvBuffer *rbuf;
	GIOPSendBuffer *sbuf;
	CORBA_NVList *params;
	CORBA_ORB orb;
	guchar did_ctx, did_exc;
};

#endif /* !_ORBIT_ORBIT_POA_TYPE_H_ */
