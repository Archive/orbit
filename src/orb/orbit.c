/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  ORBit: A CORBA v2.2 ORB
 *
 *  Copyright (C) 1998 Richard H. Porter and Red Hat Software
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Dick Porter <dick@cymru.net>
 *
 */

/*
 * This file is a repository for random functions that don't fit anywhere
 * else, and for ORBit-specific stuff.
 */

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <math.h>

#include "orbit.h"

const guint orbit_major_version = ORBIT_MAJOR_VERSION,
	orbit_minor_version = ORBIT_MINOR_VERSION,
	orbit_micro_version = ORBIT_MICRO_VERSION;
const char orbit_version[] = ORBIT_VERSION;


CORBA_char* CORBA_string_dup(const CORBA_char *string)
{
	if(!string) return NULL;
	return strcpy( ORBit_alloc_simple(strlen(string)+1), string);
}

CORBA_char* CORBA_string_alloc(CORBA_unsigned_long len)
{
	return ORBit_alloc_simple(len + 1);
}

CORBA_wchar* CORBA_wstring_alloc(CORBA_unsigned_long len)
{
	return ORBit_alloc_simple(len + 1);
}

gpointer
CORBA_string__freekids(gpointer mem, gpointer dat)
{
	CORBA_char **pstr = mem;
	CORBA_free(*pstr);
	return pstr + 1; 
}

gpointer CORBA_Object__freekids(gpointer mem, gpointer dat)
{
	CORBA_Object *pobj = mem;
	ORBit_RootObject_release(*pobj);
	return pobj + 1;
}

/* 19.14 */

/* The big picture for fixeds.
   We have to represent a number in memory.

   1 2 3 . 4 5 6 7

   There are three pieces of information in a fixed:

   - Number of significant digits. (_digits)

   - The scale. The number of places the decimal point is to the right
   of the first significant digit. (_scale)

   - The digits themselves (_value)

 */
CORBA_long CORBA_fixed_integer_part(const void *fp)
{
	CORBA_long retval = 0;
	int i, power_of_ten, digit;
	const CORBA_fixed_d_s *val = fp;

	g_return_val_if_fail(fp != NULL, INT_MIN);

	for(i = 0; i < (val->_digits - val->_scale); i++) {
		power_of_ten = val->_digits - i - val->_scale - 1;
		digit = val->_value[i];
		retval += digit * ((int)pow(10, power_of_ten));
	}

	return retval;
}

CORBA_long CORBA_fixed_fraction_part(const void *fp)
{
	CORBA_long retval = 0;
	int i, power_of_ten, digit;
	const CORBA_fixed_d_s *val = fp;

	g_return_val_if_fail(fp != NULL, INT_MIN);

	for(i = val->_digits - val->_scale; i < val->_digits; i++){
		power_of_ten = val->_digits - i - 1;
		digit = val->_value[i];
		retval += digit * ((int)pow(10, power_of_ten));
	}

	return retval;
}

static inline
CORBA_long do_div (CORBA_long *n)
{
  int __res;

  __res = (*n) % (unsigned) 10;
  *n = (*n) / (unsigned) 10;

  return __res;
}

void CORBA_fixed_set(void *rp, CORBA_long i, CORBA_long f)
{
	CORBA_fixed_d_s *val = rp;
	CORBA_long left_to_eat, cur;
	signed char sign = 1;

	g_return_if_fail(rp != NULL);

	memset(val->_value, 0, val->_digits);

	if(i) sign = i/abs(i);
	val->_sign = sign;
	i = abs(i);
	f = abs(f);

	for(cur = 0, left_to_eat = i;
	    left_to_eat != 0 && cur < val->_digits; cur++) {
		val->_value[cur] = do_div(&left_to_eat) * sign;
		sign = 1;
	}

	val->_scale = cur - 1;

	for(left_to_eat = f;
	    left_to_eat != 0 && cur < val->_digits; cur++) {
		val->_value[cur] = do_div(&left_to_eat);
	}
}

void CORBA_fixed_add(void *rp, const void *f1p, const void *f2p)
{
	g_assert(!"Not yet implemented");
}

void CORBA_fixed_sub(void *rp, const void *f1p, const void *f2p)
{
	g_assert(!"Not yet implemented");
}

void CORBA_fixed_mul(void *rp, const void *f1p, const void *f2p)
{
	g_assert(!"Not yet implemented");
}

void CORBA_fixed_div(void *rp, const void *f1p, const void *f2p)
{
	g_assert(!"Not yet implemented");
}

CORBA_fixed_d_s *CORBA_fixed_alloc(CORBA_unsigned_short d)
{
	return (CORBA_fixed_d_s *)
		g_malloc(sizeof(CORBA_fixed_d_s) + d + 1);
}

int ORBit_parse_unixsock(CORBA_Object obj,
			 char *sockpath,
			 gboolean existing_only)
{
	if(!sockpath || !*sockpath)
		return -1;
	
	obj->connection =
		GIOP_CONNECTION(iiop_connection_unix_get(sockpath,
							 existing_only));

	if(!obj->connection)
		return -1;

	giop_connection_ref(obj->connection);
	return 0;
}

int ORBit_parse_inet(CORBA_Object obj, char *hostname, unsigned short port,
			 gboolean existing_only)
{
	obj->connection = GIOP_CONNECTION(iiop_connection_get(hostname, port, existing_only));

	if(!obj->connection)
		return -1;
	giop_connection_ref(obj->connection);
	return 0;
}

/*public*/ const CORBA_unsigned_long ORBit_zero_int = 0;

struct iovec ORBit_default_principal_iovec = 
  {(gpointer)&ORBit_zero_int, sizeof(ORBit_zero_int)};

void ORBit_set_default_principal(CORBA_Principal *principal)
{
	gpointer t;

	if((gpointer)ORBit_default_principal_iovec.iov_base != (gpointer)&ORBit_zero_int)
		g_free(ORBit_default_principal_iovec.iov_base);

	ORBit_default_principal_iovec.iov_len = principal->_length
		+ sizeof(CORBA_unsigned_long);

	t = ORBit_default_principal_iovec.iov_base =
		g_malloc(ORBit_default_principal_iovec.iov_len);

	memcpy(t, &principal->_length, sizeof(principal->_length));

	t = ((guchar *)t) + sizeof(principal->_length);
	memcpy(t, principal->_buffer, principal->_length);
}
