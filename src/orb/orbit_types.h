/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  ORBit: A CORBA v2.2 ORB
 *
 *  Copyright (C) 1998 Richard H. Porter and Red Hat Software
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Dick Porter <dick@cymru.net>
 *
 */

#ifndef _ORBIT_TYPES_H_
#define _ORBIT_TYPES_H_

#include <stddef.h>	/* for wchar_t */
#include <sys/types.h>  /* for sysdep types */
#include <netinet/in.h>	/* for sockaddr_in */
#include <sys/uio.h> /* for struct iovec */

#define CORBA_TRUE 1
#define CORBA_FALSE 0

#include <orb/corba_simple_types.h>


/**
    Define various PIDL native types and language-specific types.
**/
typedef CORBA_char *CORBA_ORBid;
#define _CORBA_Object_defined 1
typedef struct CORBA_Object_struct *CORBA_Object;

typedef struct CORBA_Environment_type CORBA_Environment;
typedef struct CORBA_any_type CORBA_any;
#include "corba_any_type.h"

#define _CORBA_OpaqueValue_defined 1
typedef void *CORBA_OpaqueValue;

#define _CORBA_NVList_defined 1
struct CORBA_NVList_type {
	CORBA_unsigned_long flags;	/* should be CORBA_Flags */
	GArray *list;
};
typedef struct CORBA_NVList_type CORBA_NVList;

/* CORBA_Policy should be a real object, be we treat it as psuedo-object! */
#define _CORBA_Policy_defined 1
#define CORBA_Policy__freekids CORBA_Object__freekids
typedef struct CORBA_Policy_type *CORBA_Policy;

#define CORBA_OBJECT_NIL NULL	/* no way to express in PIDL */
#define ORBIT_IMPLEMENTS_IS_A	/* backward compat flag? */


/* CORBA C mapping specifies non-standard mapping for these: */
#define CORBA_ServerRequest__get_operation CORBA_ServerRequest_operation

#include "poa_base.h"

#include "corba_typecode.h"

/**
    This the "real" and "pseudo" definitions from the CORBA IDL module.
    The #defines and #undefs are to support specialized C mapping.
**/
#define CORBA_Object_create_request CORBA_Object_create_request_bogus
#define CORBA_ServerRequest_set_exception CORBA_ServerRequest_set_exception_bogus
#include <orb/corba_defs.h>
#undef CORBA_Object_create_request
#undef CORBA_ServerRequest_set_exception

#include "corba_any.h"

typedef CORBA_unsigned_long CORBA_enum;	/* what is this? */

typedef struct CORBA_DynFixed_type *CORBA_DynFixed;

#include "corba_env.h"
#include "corba_any_type.h"


#include "orbit_rootobject.h"

struct CORBA_Policy_type {
        struct ORBit_RootObject_struct parent;
	CORBA_PolicyType policy_type;
};


#include "corba_context.h"

#include "corba_portableserver.h"

#include "corba_sequences_type.h"

#include "orbit_object_type.h"

#include "corba_object_type.h"

#include "corba_orb_type.h"

#include "corba_typecode.h"
#include "corba_typecode_type.h"
#include "corba_any_proto.h"

/* 19.14 */

/* XXX */
typedef struct CORBA_fixed_d_s {
	CORBA_unsigned_short _digits;
	CORBA_short _scale;
	signed char _sign;
	signed char _value[1];
} CORBA_fixed_d_s;

#include "corba_env_type.h"


typedef struct CORBA_WrongTransaction {
	int dummy;
} CORBA_WrongTransaction;

#define CORBA_ARG_IN (1<<0)
#define CORBA_ARG_OUT (1<<1)
#define CORBA_ARG_INOUT (1<<2)
#define CORBA_CTX_RESTRICT_SCOPE (1<<3)
#define CORBA_CTX_DELETE_DESCENDENTS (1<<4)
#define CORBA_OUT_LIST_MEMORY (1<<5)
#define CORBA_IN_COPY_VALUE (1<<6)
#define CORBA_DEPENDENT_LIST (1<<7)
#define CORBA_INV_NO_RESPONSE (1<<8)
#define CORBA_INV_TERM_ON_ERROR (1<<9)
#define CORBA_RESP_NO_WAIT (1<<10)

typedef CORBA_char *CORBA_FieldName;

typedef struct CORBA_NameValuePair {
	CORBA_FieldName id;
	CORBA_any value;
} CORBA_NameValuePair;

struct CORBA_Current_type {
	int fill_me_in;
};

#include "corba_portableserver_type.h"


/*
 * ServerRequest and Dynamic Servant Invokation (DSI)
 * See dsi.c
 */
/* C Mapping deviants from standard IDL mapping */
extern void CORBA_ServerRequest_set_exception(
	CORBA_ServerRequest req,
	CORBA_exception_type major,
	const CORBA_any *value,
	CORBA_Environment *env);

extern void PortableServer_DynamicImpl__init(PortableServer_Servant,
					     CORBA_Environment *ev);

extern void PortableServer_DynamicImpl__fini(PortableServer_Servant,
					     CORBA_Environment *ev);


/* The ALIGNOF_ values of basic types are computed by configure, and
 * appear in ORBit/config.h. We setup aligns for more complex types here.
 * TCVAL means CORBA_any._value, while ANY means the CORBA_any struct itself.
 * SEQ means the sequence struct, not its buffer.
 */
#define ALIGNOF_CORBA_ANY  MAX(		ALIGNOF_CORBA_STRUCT, 	\
					ALIGNOF_CORBA_POINTER)
#define ALIGNOF_CORBA_TCVAL MAX(MAX(	ALIGNOF_CORBA_LONG, 	\
					ALIGNOF_CORBA_STRUCT), 	\
					ALIGNOF_CORBA_POINTER)
#define ALIGNOF_CORBA_SEQ  MAX(MAX(	ALIGNOF_CORBA_STRUCT, 	\
					ALIGNOF_CORBA_LONG), 	\
					ALIGNOF_CORBA_POINTER)


#endif /* !_ORBIT_TYPES_H_ */
