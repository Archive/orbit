/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/*
 *  ORBit: A CORBA v2.2 ORB
 *
 *  Copyright (C) 1998 Red Hat Software, Richard H. Porter
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *          Dick Porter <dick@cymru.net>
 *
 */

#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "orbit.h"
/* #include "interface_repository.h" */

struct CORBA_Request_type {
	struct ORBit_RootObject_struct parent;

	CORBA_Object obj;
	CORBA_Context ctx;

	CORBA_Flags req_flags;
	CORBA_Identifier operation;

	CORBA_NamedValue *result;
	CORBA_NVList *arg_list;

	CORBA_unsigned_long request_id;
	GIOPSendBuffer *request_buffer;
	GIOPRecvBuffer *reply_buffer;

	CORBA_TypeCode *exception_list;
};


/**
    Frees the Request psuedo-object. First delete the contents
    of the Request (if not already), then free up the object itself.
**/
static void CORBA_Request_free_fn(gpointer obj_in)
{
	CORBA_Request req = obj_in;
	CORBA_Request_delete(req, /*ev*/0);
	g_free(req);
}

static const ORBit_RootObject_Interface CORBA_Request_epv = {
	ORBIT_ROT_REQUEST,
	CORBA_Request_free_fn
};

CORBA_Status
CORBA_Object_create_request2(CORBA_Object obj,
					    CORBA_Context ctx,
					    CORBA_Identifier operation,
					    CORBA_NVList *arg_list,
					    CORBA_NamedValue *result,
							CORBA_TypeCode *exception_list,
					    CORBA_Request *request,
					    CORBA_Flags req_flags,
					    CORBA_Environment *ev)
{
	CORBA_Request new;

	new=g_new0(struct CORBA_Request_type, 1);
	if(new==NULL) {
		CORBA_exception_set_system(ev,
				   ex_CORBA_NO_MEMORY, CORBA_COMPLETED_NO);
		return;
	}
	ORBit_RootObject_init(&new->parent, &CORBA_Request_epv);

	new->obj=CORBA_Object_duplicate(obj, ev);
	new->ctx=ORBit_RootObject_dup(ctx);
	new->operation=CORBA_string_dup(operation);

	new->result=result;

	new->req_flags=req_flags;
	new->request_id = giop_get_request_id();
	new->arg_list = arg_list;

	new->exception_list = exception_list;

	*request=ORBit_RootObject_dup(new);
}

/* Section 5.2.1 */
CORBA_Status
CORBA_Object_create_request(CORBA_Object obj,
					    CORBA_Context ctx,
					    CORBA_Identifier operation,
					    CORBA_NVList *arg_list,
					    CORBA_NamedValue *result,
					    CORBA_Request *request,
					    CORBA_Flags req_flags,
					    CORBA_Environment *ev)
{
	return CORBA_Object_create_request2(obj, ctx, operation, arg_list, result,
										NULL, request, req_flags, ev);
}

/* Section 5.2, 5.2.2 */
CORBA_Status
CORBA_Request_add_arg(CORBA_Request req,
					  CORBA_Identifier name,
					  CORBA_TypeCode arg_type,
					  void *value,
					  CORBA_long len,
					  CORBA_Flags arg_flags,
					  CORBA_Environment *ev)
{
	gpointer new_value;

	g_assert(req!=NULL);

	if ( req->arg_list == 0 ) {
	    CORBA_ORB_create_list(req->obj->orb, /*count:XXX*/5, 
	      &req->arg_list, ev);
	}

	if((arg_flags & CORBA_IN_COPY_VALUE) && (arg_flags & CORBA_ARG_IN)) {
		new_value = ORBit_copy_value(value, arg_type);
		if(new_value==NULL) {
			CORBA_exception_set_system(ev, 
				ex_CORBA_NO_MEMORY, CORBA_COMPLETED_NO);
			return;
		}
	} else {
		new_value=value;
	}

	CORBA_NVList_add_item(req->arg_list, name, arg_type,
			  new_value, len, arg_flags | req->req_flags, ev);
}

/* Section 5.2, 5.2.3 */
CORBA_Status
CORBA_Request_invoke(CORBA_Request req,
					 CORBA_Flags invoke_flags,
					 CORBA_Environment *ev)
{
	CORBA_Request_send(req, invoke_flags, ev);
	if(ev->_major == CORBA_NO_EXCEPTION)
		CORBA_Request_get_response(req, invoke_flags, ev);
}


/* Section 5.2, 5.2.4
    This deletes the contents of the Request (any internal memory),
    but not the psuedo-object itself. The psuedo-object is only
    released by calling CORBA_Object_release(). This function must
    be written so that it may be called any number of times.
*/
CORBA_Status CORBA_Request_delete(CORBA_Request req, CORBA_Environment *ev)
{
	ORBit_RootObject_release(req->obj);	req->obj = 0;
	ORBit_RootObject_release(req->ctx);	req->ctx = 0;

	CORBA_free(req->operation);		req->operation = 0;

	if(req->arg_list != NULL) {
		if(req->req_flags & CORBA_OUT_LIST_MEMORY)
			CORBA_NVList_free(req->arg_list, /*ev*/NULL);
		else {
			int i;
			for(i = 0; i < req->arg_list->list->len; i++)
				ORBit_NamedValue_free(&g_array_index(req->arg_list->list,
													 CORBA_NamedValue, i));
			g_array_free(req->arg_list->list, TRUE);
			
			g_free(req->arg_list);
		}
	}

	if(req->result!=NULL) {
		ORBit_NamedValue_free(req->result);
		req->result = 0;
	}

	if(req->request_buffer) {
		giop_send_buffer_unuse(req->request_buffer);
		req->request_buffer = 0;
	}
	if(req->reply_buffer) {
		giop_recv_buffer_unuse(req->reply_buffer);
		req->reply_buffer = 0;
	}
}

/* Section 5.2, 5.3.1 */
CORBA_Status
CORBA_Request_send(CORBA_Request req,
				   CORBA_Flags invoke_flags,
				   CORBA_Environment *ev)
{
	int i;
	GIOPConnection *cnx;

	struct { CORBA_unsigned_long opstrlen; char opname[1]; } *opnameinfo;
	struct iovec opvec = { NULL, 0 };

	opvec.iov_len = strlen(req->operation)+1+sizeof(CORBA_unsigned_long);

	opnameinfo = g_malloc(strlen(req->operation)+1+sizeof(CORBA_unsigned_long));
	opvec.iov_base = (gpointer)opnameinfo;
	opnameinfo->opstrlen = strlen(req->operation) + 1;
	strcpy(opnameinfo->opname, req->operation);

	if ( (cnx = ORBit_object_get_connection(req->obj)) == 0 ) {
		CORBA_exception_set_system(ev,
			   ex_CORBA_COMM_FAILURE, CORBA_COMPLETED_NO);
		return;
	}

	g_assert(req->obj->active_profile);
	req->request_buffer =
		giop_send_request_buffer_use(req->obj->connection,
			 NULL,
			 req->request_id,
			 (invoke_flags & CORBA_INV_NO_RESPONSE) ? FALSE:TRUE,
			 &(req->obj->active_profile->object_key_vec),
			 &opvec,
			 &ORBit_default_principal_iovec);

	if(!req->request_buffer) {
		CORBA_exception_set_system(ev,
			   ex_CORBA_COMM_FAILURE, CORBA_COMPLETED_NO);
		return;
	}

	if ( req->arg_list && req->arg_list->list ) {
	    for(i = 0; i < req->arg_list->list->len; i++) {
		CORBA_NamedValue *nv;

		nv = &g_array_index(req->arg_list->list, CORBA_NamedValue, i); 

		if((nv->arg_modes & CORBA_ARG_IN)
		   || (nv->arg_modes & CORBA_ARG_INOUT))
			ORBit_marshal_arg(req->request_buffer, 
							  nv->argument._value,
							  nv->argument._type);
	    }
	}
				      
	giop_send_buffer_write(req->request_buffer);

	giop_send_buffer_unuse(req->request_buffer);
	req->request_buffer = 0;

	g_free(opnameinfo);
}

/* Section 5.3.2 */
CORBA_Status
CORBA_send_multiple_requests(CORBA_Request *reqs,
							 CORBA_Environment *env,
							 CORBA_long count,
							 CORBA_Flags invoke_flags)
{
	int i;

	for(i = 0; i < count; i++)
		CORBA_Request_send(reqs[i], invoke_flags, env);	
}

/**
    See V2.3 sec 7.1.1 for discussion of memory management. For out
    arguments (which includes the result), the _value may be NULL
    or non-NULL. If NULL, the DII must alloc memory for it. If non-NULL,
    the DII should attempt to use that memory. Quoting:
	If the storage pointed to is not sufficient to hold
	the value of the out parameter, the behavior is undefined.
    ORBit's behavior is to trash memory and subsequently core-dump.
    So only pass in non-NULL args for fixed length types.
    In an ideal world, ORBit could detect that the type about to
    be demarshalled wont fit, and would realloc new memory.
**/

static void
ORBit_demarshal_nv(CORBA_Request req, CORBA_NamedValue *nv) {
    if (nv->arg_modes & CORBA_ARG_INOUT) {
    	ORBit_TypeCode_freekids(nv->argument._type, nv->argument._value);
    }
    if ( nv->arg_modes & (CORBA_ARG_INOUT|CORBA_ARG_OUT) ) {
	gpointer	val2;
    	if ( nv->argument._value == 0 ) {
	    nv->argument._value = ORBit_alloc_tcval(nv->argument._type, 1);
	    nv->argument._release = 1;
    	}
	val2 = nv->argument._value;
	ORBit_TypeCode_demarshal_value( nv->argument._type, &val2,
	  req->reply_buffer, /*dup_strings*/TRUE, req->obj->orb);
    }
}

static void
ORBit_handle_dii_reply(CORBA_Request req, CORBA_Environment *ev)
{

	/* XXX TODO - handle exceptions, location forwards(?), all that */

	switch ( req->reply_buffer->message.u.reply.reply_status )
	{
	case GIOP_NO_EXCEPTION:
		if ( req->result ) {
			req->result->arg_modes |= CORBA_ARG_OUT;
			ORBit_demarshal_nv(req, req->result);
		}
		if ( req->arg_list ) {
			int i;
			for(i = 0; i < req->arg_list->list->len; i++) {
				CORBA_NamedValue *nv = &g_array_index(req->arg_list->list, CORBA_NamedValue, i); 
				ORBit_demarshal_nv(req, nv);
			}
		}
		break;

	case GIOP_USER_EXCEPTION:
		ORBit_handle_dii_exception(req->reply_buffer, ev, req->exception_list, req->obj->orb);
		break;

	case GIOP_SYSTEM_EXCEPTION:
		ORBit_handle_exception(req->reply_buffer, ev, NULL, req->obj->orb);
		break;
	  
	case GIOP_LOCATION_FORWARD:
		/* TODO: handle this */

	default:
		/* really bad, unknown reply status! */
		break;
	}

	giop_recv_buffer_unuse(req->reply_buffer);
	req->reply_buffer = 0;
}

/* Section 5.2, 5.3.3
 *
 * Raises: WrongTransaction
 */
CORBA_Status
CORBA_Request_get_response(CORBA_Request req,
						   CORBA_Flags response_flags,
						   CORBA_Environment *ev)
{
	req->reply_buffer = giop_recv_reply_buffer_use(req->request_id,
												   !(response_flags & CORBA_RESP_NO_WAIT));

	if(!req->reply_buffer) {
		CORBA_exception_set_system(ev,
								   ex_CORBA_COMM_FAILURE,
								   CORBA_COMPLETED_NO);
		return;
	}

	ORBit_handle_dii_reply(req, ev);
}

/* Section 5.3.4
 *
 * Raises: WrongTransaction
 */
CORBA_Status
CORBA_get_next_response(CORBA_Environment *env,
						CORBA_Flags response_flags,
						CORBA_Request *req)
{
	int i;
	GIOPRecvBuffer *rb;
	GArray *reqids = g_array_new(FALSE, FALSE,
								 sizeof(CORBA_unsigned_long));

	for(i = 0; req[i]; i++) {
		g_array_append_val(reqids, req[i]->request_id);
	}

	rb = giop_recv_reply_buffer_use_multiple(reqids,
											 !(response_flags & CORBA_RESP_NO_WAIT));

	if(rb) {
		for(i = 0; i < reqids->len; i++) {
			if(g_array_index(reqids, CORBA_unsigned_long, i)
			   == rb->message.u.reply.request_id) {
				req[i]->reply_buffer = rb;
				break;
			}
		}

		if(i < reqids->len)
			ORBit_handle_dii_reply(req[i], env);
	}

	g_array_free(reqids, TRUE);
}


/* Section 5.4.1 */
CORBA_Status
CORBA_ORB_create_list(CORBA_ORB orb,
					  CORBA_long count,
					  CORBA_NVList **new_list,
					  CORBA_Environment *ev)
{
	CORBA_NVList *new;

	new = g_new0(CORBA_NVList, 1);
	if(new==NULL) goto new_alloc_failed;

	new->list = g_array_new(FALSE, TRUE, sizeof(CORBA_NamedValue));

	*new_list = new;

	return;

 new_alloc_failed:
	CORBA_exception_set_system(ev, ex_CORBA_NO_MEMORY, CORBA_COMPLETED_NO);
}

/* Section 5.4.6 */
CORBA_Status
CORBA_ORB_create_operation_list(CORBA_ORB orb,
					CORBA_OperationDef oper,
					CORBA_NVList **new_list,
					CORBA_Environment *ev)
{
	if(!new_list) {
		CORBA_exception_set_system(ev,
			   ex_CORBA_BAD_PARAM, CORBA_COMPLETED_NO);
		return;
	}

	g_warning("CORBA_ORB_create_operation_list NYI");

	CORBA_exception_set_system(ev,
			   ex_CORBA_IMP_LIMIT, CORBA_COMPLETED_NO);
}

/* Section 5.4.2 */
CORBA_Status
CORBA_NVList_add_item(CORBA_NVList *list,
					  CORBA_Identifier item_name,
					  CORBA_TypeCode item_type,
					  void *value,
					  CORBA_long value_len,
					  CORBA_Flags item_flags,
					  CORBA_Environment *ev)
{
	CORBA_NamedValue newval;

	g_assert(list!=NULL);

	newval.name = CORBA_string_dup(item_name);
	newval.argument._type = ORBit_RootObject_dup(item_type);
	if(item_flags & CORBA_IN_COPY_VALUE) {
		newval.argument._value = ORBit_copy_value(value, item_type);
		newval.argument._release = CORBA_TRUE;
	} else {
		newval.argument._value = value;
		newval.argument._release = CORBA_FALSE;
	}

	newval.len = value_len; /* Is this even useful? *sigh* */
	newval.arg_modes = item_flags;

	g_array_append_val(list->list, newval);
}

void ORBit_NamedValue_free(CORBA_NamedValue *nv)
{
	CORBA_free(nv->name);	nv->name = 0;
}

/* Section 5.4.3 */
CORBA_Status
CORBA_NVList_free(CORBA_NVList *list, CORBA_Environment *ev)
{
	int i;

	CORBA_NVList_free_memory(list, ev);

	if ( list->list ) {
	    for(i = 0; i < list->list->len; i++) {
		CORBA_NamedValue *nv = &g_array_index(list->list, CORBA_NamedValue, i);
		ORBit_NamedValue_free(nv);
	    }
	    g_array_free(list->list, TRUE);
	    list->list = 0;
	}
	g_free(list);
}

/* Section 5.4.4 */
CORBA_Status
CORBA_NVList_free_memory(CORBA_NVList *list, CORBA_Environment *ev)
{
	int i;

	if ( list->list ) {
	    for(i = 0; i < list->list->len; i++) {
		CORBA_NamedValue *nv = &g_array_index(list->list, CORBA_NamedValue, i);
		if ( nv->argument._release ) {
		    CORBA_free(nv->argument._value);
		    nv->argument._value = 0;
		}
		ORBit_RootObject_release(nv->argument._type);
		nv->argument._type = 0;
	    }
	}
}


/* Section 5.4.5 */
CORBA_Status
CORBA_NVList_get_count(CORBA_NVList *list,
					   CORBA_long *count,
					   CORBA_Environment *ev)
{
	*count = list->list->len;
}

