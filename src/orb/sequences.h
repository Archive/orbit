/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  ORBit: A CORBA v2.2 ORB
 *
 *  Copyright (C) 1998 Richard H. Porter
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Dick Porter <dick@cymru.net>
 *
 */

#ifndef _ORBIT_SEQUENCES_H_
#define _ORBIT_SEQUENCES_H_

extern gpointer CORBA_sequence__freekids(gpointer mem, gpointer dat);

#define CORBA_octet_allocbuf CORBA_sequence_CORBA_octet_allocbuf
#define CORBA_sequence_octet__alloc CORBA_sequence_CORBA_octet__alloc
#define TC_CORBA_sequence_octet TC_CORBA_sequence_CORBA_octet

extern CORBA_sequence_octet*	
ORBit_sequence_octet_init(	CORBA_sequence_octet* dst, int buflen);
extern void
ORBit_sequence_octet_fini(	CORBA_sequence_octet* dst);
extern CORBA_sequence_octet*
ORBit_sequence_octet_create(	int buflen);
extern CORBA_sequence_octet*	
ORBit_sequence_octet_copy(	CORBA_sequence_octet* dst, 
				CORBA_sequence_octet* src);
extern CORBA_sequence_octet*	
ORBit_sequence_octet_dup(	CORBA_sequence_octet* src);
extern guint		
ORBit_sequence_octet_hash(	const CORBA_sequence_octet *so);
extern gint
ORBit_sequence_octet_cmp(	const CORBA_sequence_octet *s1, 
				const CORBA_sequence_octet *s2);
extern gint
ORBit_sequence_octet_equal(	const CORBA_sequence_octet *s1, 
				const CORBA_sequence_octet *s2);

extern CORBA_boolean
ORBit_hexbuf_to_buf(const CORBA_char *hexbuf, int hexlen, char *buf);
extern CORBA_char*
ORBit_sequence_octet_to_hexstrdup(const CORBA_sequence_octet *src);
extern CORBA_sequence_octet*
ORBit_sequence_octet_create_from_hexstr(const CORBA_char *src);

extern CORBA_char**
CORBA_string_allocbuf(		CORBA_unsigned_long len);


#endif /* !_ORBIT_SEQUENCES_H_ */
