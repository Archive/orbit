/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  ORBit: A CORBA v2.2 ORB
 *
 *  Copyright (C) 1998 Richard H. Porter, and Red Hat Software
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Dick Porter <dick@cymru.net>
 *          Elliot Lee <sopwith@redhat.com>
 *          Kennard White <kennard@berkeley.innomedia.com>
 *
 */

#include <string.h>
#include <stdio.h>
#include <assert.h>

#include "orbit.h"
#include "orbit_poa.h"
#include "genrand.h"

PortableServer_ThreadPolicyValue
PortableServer_ThreadPolicy__get_value(PortableServer_ThreadPolicy  obj, CORBA_Environment *ev)
{
	if(!obj) {
		ev->_major = 2 ;
		goto error_exit;
	}
	ev->_major = CORBA_NO_EXCEPTION;

	return obj->value;

error_exit:
	CORBA_exception_set_system(ev, 0, CORBA_COMPLETED_NO);
	return 0;
}

PortableServer_LifespanPolicyValue
PortableServer_LifespanPolicy__get_value(PortableServer_LifespanPolicy  obj, CORBA_Environment *ev)
{
	if(!obj) {
		ev->_major = 2 ;
		goto error_exit;
	}

	ev->_major = CORBA_NO_EXCEPTION;
	return obj->value;

error_exit:
	CORBA_exception_set_system(ev, 0, CORBA_COMPLETED_NO);
	return 0;
}

PortableServer_IdUniquenessPolicyValue
PortableServer_IdUniquenessPolicy__get_value(PortableServer_IdUniquenessPolicy  obj, CORBA_Environment *ev)
{
	if(!obj) {
		ev->_major = 2 ;
		goto error_exit;
	}

	ev->_major = CORBA_NO_EXCEPTION;
	return obj->value;

error_exit:
	CORBA_exception_set_system(ev, 0, CORBA_COMPLETED_NO);
	return 0;
}

PortableServer_IdAssignmentPolicyValue
PortableServer_IdAssignmentPolicy__get_value(PortableServer_IdAssignmentPolicy  obj, CORBA_Environment *ev)
{
	if(!obj) {
		ev->_major = 2 ;
		goto error_exit;
	}

	ev->_major = CORBA_NO_EXCEPTION;
	return obj->value;

error_exit:
	CORBA_exception_set_system(ev, 0, CORBA_COMPLETED_NO);
	return 0;
}

PortableServer_ImplicitActivationPolicyValue
PortableServer_ImplicitActivationPolicy__get_value(PortableServer_ImplicitActivationPolicy  obj, CORBA_Environment *ev)
{
	if(!obj) {
		ev->_major = 2 ;
		goto error_exit;
	}

	ev->_major = CORBA_NO_EXCEPTION;
	return obj->value;

error_exit:
	CORBA_exception_set_system(ev, 0, CORBA_COMPLETED_NO);
	return 0;
}

PortableServer_ServantRetentionPolicyValue
PortableServer_ServantRetentionPolicy__get_value(PortableServer_ServantRetentionPolicy  obj, CORBA_Environment *ev)
{
	if(!obj) {
		ev->_major = 2 ;
		goto error_exit;
	}

	ev->_major = CORBA_NO_EXCEPTION;
	return obj->value;

error_exit:
	CORBA_exception_set_system(ev, 0, CORBA_COMPLETED_NO);
	return 0;
}

PortableServer_RequestProcessingPolicyValue
PortableServer_RequestProcessingPolicy__get_value(PortableServer_RequestProcessingPolicy  obj, CORBA_Environment *ev)
{
	if(!obj) {
		ev->_major = 2 ;
		goto error_exit;
	}

	ev->_major = CORBA_NO_EXCEPTION;
	return obj->value;

error_exit:
	CORBA_exception_set_system(ev, 0, CORBA_COMPLETED_NO);
	return 0;
}

/* make emacs happy; */

PortableServer_POAManager_State
PortableServer_POAManager_get_state(PortableServer_POAManager obj,
				    CORBA_Environment *ev)
{
	if(!obj) {
		CORBA_exception_set_system(ev,
					   ex_CORBA_BAD_PARAM,
					   CORBA_COMPLETED_NO);
		return -1;
	}

	ev->_major = CORBA_NO_EXCEPTION;
	return obj->state;
}

/**** PortableServer_POAManager_activate
      Inputs: 'obj' - a POAManager to activate
      Outputs: '*ev' - result of the activate operation

      Side effect: Clears the 'held_requests' lists for all POA's
                   associated with the 'obj' POAManager.

      Description: Sets the POAManager state to 'ACTIVE', then
                   goes through all the POA's associated with this
		   POAManager, and makes them re-process their
		   'held_requests'
 */
void
PortableServer_POAManager_activate(PortableServer_POAManager obj,
				   CORBA_Environment *ev)
{
	GSList *curitem;
	PortableServer_POA curpoa;

	if(!obj) {
		CORBA_exception_set_system(ev,
					   ex_CORBA_BAD_PARAM,
					   CORBA_COMPLETED_NO);
		return;
	}

	if(obj->state == PortableServer_POAManager_INACTIVE) {
		CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
				    ex_PortableServer_POAManager_AdapterInactive,
				    NULL);
		return;
	}

	obj->state = PortableServer_POAManager_ACTIVE;

	for(curitem = obj->poa_collection; curitem;
	    curitem = g_slist_next(curitem)) {
		curpoa = (PortableServer_POA)curitem->data;
		ORBit_POA_handle_held_requests(curpoa);
	}
	ev->_major = CORBA_NO_EXCEPTION;
}

void
PortableServer_POAManager_hold_requests(PortableServer_POAManager obj,
					CORBA_boolean wait_for_completion,
					CORBA_Environment *ev)
{
	if(!obj) {
		CORBA_exception_set_system(ev,
					   ex_CORBA_BAD_PARAM,
					   CORBA_COMPLETED_NO);
		return;
	}

	if(obj->state == PortableServer_POAManager_INACTIVE) {
		CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
				    ex_PortableServer_POAManager_AdapterInactive,
				    NULL);
		return;
	}

	obj->state = PortableServer_POAManager_HOLDING;
	if(!wait_for_completion)
		g_warning("hold_requests not finished - don't know how to kill outstanding request fulfillments");

	ev->_major = CORBA_NO_EXCEPTION;
}

void
PortableServer_POAManager_discard_requests(PortableServer_POAManager obj,
					   CORBA_boolean wait_for_completion,
					   CORBA_Environment *ev)
{
	if(!obj) {
		CORBA_exception_set_system(ev,
					   ex_CORBA_BAD_PARAM,
					   CORBA_COMPLETED_NO);
		return;
	}

	if(obj->state == PortableServer_POAManager_INACTIVE) {
		CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
				    ex_PortableServer_POAManager_AdapterInactive,
				    NULL);
		return;
	}

	obj->state = PortableServer_POAManager_DISCARDING;
	if(!wait_for_completion)
		g_warning("discard_requests not finished - don't know how to kill outstanding request fulfillments");
	ev->_major = CORBA_NO_EXCEPTION;
}

void
PortableServer_POAManager_deactivate(PortableServer_POAManager obj,
				     CORBA_boolean etherealize_objects,
				     CORBA_boolean wait_for_completion,
				     CORBA_Environment *ev)
{
	GSList	*poai;
	ev->_major = CORBA_NO_EXCEPTION;
	if(!obj) {
		CORBA_exception_set_system(ev,
					   ex_CORBA_BAD_PARAM,
					   CORBA_COMPLETED_NO);
		return;
	}

	if(obj->state == PortableServer_POAManager_INACTIVE) {
		CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
				    ex_PortableServer_POAManager_AdapterInactive,
				    NULL);
		return;
	}
	if ( wait_for_completion ) {
		for (poai=obj->poa_collection; poai; poai=poai->next) {
			if ( !ORBit_POA_is_inuse(poai->data, /*kids*/0, ev) ) {
				CORBA_exception_set_system(ev, 
					ex_CORBA_BAD_INV_ORDER,
				        CORBA_COMPLETED_NO);
				return;
			}
		}
	}

	obj->state = PortableServer_POAManager_INACTIVE;


	for (poai=obj->poa_collection; poai; poai=poai->next) {
		ORBit_POA_deactivate(poai->data, etherealize_objects, ev);
	}
}


CORBA_boolean
PortableServer_AdapterActivator_unknown_adapter(PortableServer_AdapterActivator obj,
						PortableServer_POA parent,
						CORBA_char *name,
						CORBA_Environment *ev)
{
	g_assert(!"Not yet implemented");
	return(CORBA_FALSE);
}


/**** PortableServer_ServantActivator_incarnate
 */
PortableServer_Servant

PortableServer_ServantActivator_incarnate
(PortableServer_ServantActivator obj,
 PortableServer_ObjectId *oid,
 PortableServer_POA adapter,
 CORBA_Environment *ev)
{
	g_assert(!"Not yet implemented");
	return(NULL);
}

void
PortableServer_ServantActivator_etherealize
(PortableServer_ServantActivator obj,
 PortableServer_ObjectId *oid, PortableServer_POA adapter,
 PortableServer_Servant serv,
 CORBA_boolean cleanup_in_progress,
 CORBA_boolean remaining_activations,
 CORBA_Environment *ev)
{
	g_assert(!"Not yet implemented");
	return;
}

/**
    Caller must release returned objref. In addition, caller
    must either call POA_destroy() or ORB_destroy() to release
    the memory assoicated with the POA.
**/
PortableServer_POA
PortableServer_POA_create_POA
               (PortableServer_POA poa,
		CORBA_char *adapter_name,
		PortableServer_POAManager a_POAManager,
		CORBA_PolicyList* policies,
		CORBA_Environment *ev)
{
	PortableServer_POA new_poa = NULL;
	PortableServer_POA check_poa = NULL;
	
	/* Check for a child POA by the same name in parent */
	check_poa = PortableServer_POA_find_POA(poa,adapter_name,
						FALSE, ev);
	CORBA_exception_free (ev);

	if (!check_poa) {
	  new_poa = ORBit_POA_new(poa->orb,
				  adapter_name, a_POAManager, policies, ev);
	} else {
	  CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			  ex_PortableServer_POA_AdapterAlreadyExists,
			  NULL);
	  new_poa = NULL;
	}

	if(ev->_major == CORBA_NO_EXCEPTION) {
	  /* register parent-child (it will do both cross-link pointers) */
	  ORBit_POA_add_child(poa, new_poa, ev);
	}

	return new_poa;
}

/**** PortableServer_POA_find_POA
      Inputs: 'obj' - a POA
              'activate_it' - whether to activate unknown POA's

      Outputs: 'child_poa'

      Description: Finds (and optionally activates) a child POA of 'obj'
                   with the specified names.

      TODO: Activate non-existent adapters if asked.

 */
PortableServer_POA
PortableServer_POA_find_POA(PortableServer_POA obj,
			    CORBA_char *adapter_name,
			    CORBA_boolean activate_it,
			    CORBA_Environment *ev)
{
	GSList *curitem;
	PortableServer_POA child_poa;

	for(curitem = obj->child_poas; curitem; curitem = curitem->next) {
		child_poa = (PortableServer_POA)curitem->data;
		if(!strcmp(child_poa->the_name, adapter_name)) {
			ev->_major = CORBA_NO_EXCEPTION;
			return child_poa;
		}
	}

	if(activate_it)
		g_warning("Don't yet know how to activate POA named \"%s\"",
			  adapter_name);

	CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			    ex_PortableServer_POA_AdapterNonExistent,
			    NULL);	

	return NULL;
}

/**** PortableServer_POA_destroy
      Inputs: 'obj' - the POA to be destroyed
              'etherealize_objects' - flag indicating whether any servant
	                              manager should be asked to etherealize
				      objects in the active object map
	      'wait_for_completion' - flag indicating whether to wait for
	                              requests currently being handled
	Recursive invokations are allowable; all subsequent
	calls do nothing.
 */
void
PortableServer_POA_destroy(PortableServer_POA obj,
			   CORBA_boolean etherealize_objects,
			   CORBA_boolean wait_for_completion,
			   CORBA_Environment *ev)
{
	CORBA_boolean	done;
	if ( obj->life_flags & ORBit_LifeF_Destroyed )
		return;
	if ( wait_for_completion ) {
		if ( ORBit_POA_is_inuse(obj, /*kids*/1, ev) ) {
			CORBA_exception_set_system(ev, ex_CORBA_BAD_INV_ORDER,
					   CORBA_COMPLETED_NO);
			return;
		}
	}
	done = ORBit_POA_destroy(obj, etherealize_objects, ev);
	g_assert( done || !wait_for_completion );
}

PortableServer_ThreadPolicy PortableServer_POA_create_thread_policy(PortableServer_POA obj, PortableServer_ThreadPolicyValue value, CORBA_Environment *ev)
{
	PortableServer_ThreadPolicy retval;
	retval = ORBit_Policy_create_type(
	 		struct PortableServer_ThreadPolicy_type,
			PortableServer_THREAD_POLICY_ID, ev);
	retval->value = value;
	return retval;
}

PortableServer_LifespanPolicy PortableServer_POA_create_lifespan_policy(PortableServer_POA obj, PortableServer_LifespanPolicyValue value, CORBA_Environment *ev)
{
	PortableServer_LifespanPolicy retval;
	retval = ORBit_Policy_create_type( 
			struct PortableServer_LifespanPolicy_type,
			PortableServer_LIFESPAN_POLICY_ID, ev);
	retval->value = value;
	return retval;
}

PortableServer_IdUniquenessPolicy PortableServer_POA_create_id_uniqueness_policy(PortableServer_POA obj, PortableServer_IdUniquenessPolicyValue value, CORBA_Environment *ev)
{
	PortableServer_IdUniquenessPolicy retval;
	retval = ORBit_Policy_create_type(
			struct PortableServer_IdUniquenessPolicy_type,
			PortableServer_ID_UNIQUENESS_POLICY_ID, ev);
	retval->value = value;
	return retval;
}

PortableServer_IdAssignmentPolicy PortableServer_POA_create_id_assignment_policy(PortableServer_POA obj, PortableServer_IdAssignmentPolicyValue value, CORBA_Environment *ev)
{
	PortableServer_IdAssignmentPolicy retval;
	retval = ORBit_Policy_create_type(
			struct PortableServer_IdAssignmentPolicy_type,
			PortableServer_ID_ASSIGNMENT_POLICY_ID, ev);
	retval->value = value;
	return retval;
}

PortableServer_ImplicitActivationPolicy PortableServer_POA_create_implicit_activation_policy(PortableServer_POA obj, PortableServer_ImplicitActivationPolicyValue value, CORBA_Environment *ev)
{
	PortableServer_ImplicitActivationPolicy retval;
	retval = ORBit_Policy_create_type(
			struct PortableServer_ImplicitActivationPolicy_type,
			PortableServer_IMPLICIT_ACTIVATION_POLICY_ID, ev);
	retval->value = value;
	return retval;
}

PortableServer_ServantRetentionPolicy PortableServer_POA_create_servant_retention_policy(PortableServer_POA obj, PortableServer_ServantRetentionPolicyValue value, CORBA_Environment *ev)
{
	PortableServer_ServantRetentionPolicy retval;
	retval = ORBit_Policy_create_type(
			struct PortableServer_ServantRetentionPolicy_type,
			PortableServer_SERVANT_RETENTION_POLICY_ID, ev);
	retval->value = value;
	return retval;
}

PortableServer_RequestProcessingPolicy PortableServer_POA_create_request_processing_policy(PortableServer_POA obj, PortableServer_RequestProcessingPolicyValue value, CORBA_Environment *ev)
{
	PortableServer_RequestProcessingPolicy retval;
	retval = ORBit_Policy_create_type(
			struct PortableServer_RequestProcessingPolicy_type,
			PortableServer_REQUEST_PROCESSING_POLICY_ID, ev);
	retval->value = value;
	return retval;
}

ORBit_PortableServer_OkeyrandPolicy 
ORBit_PortableServer_POA_create_okeyrand_policy(PortableServer_POA obj,
  CORBA_short poa_rand_len, CORBA_short obj_rand_len,
  CORBA_Environment *ev)
{
	ORBit_PortableServer_OkeyrandPolicy retval;
	retval = ORBit_Policy_create_type(
			struct ORBit_PortableServer_OkeyrandPolicy_type,
			ORBit_PortableServer_OKEYRAND_POLICY_ID, ev);
	retval->poa_rand_len = poa_rand_len;
	retval->obj_rand_len = obj_rand_len;
	return retval;
}

CORBA_char *PortableServer_POA__get_the_name(PortableServer_POA obj, CORBA_Environment *ev)
{
	g_assert(obj);
	g_assert(obj->the_name);
	return obj->the_name;	/* XXX: should this be dup'd? */
}

PortableServer_POA
PortableServer_POA__get_the_parent(PortableServer_POA obj,
				   CORBA_Environment *ev)
{
	if(!obj) {
		CORBA_exception_set_system(ev,
					   ex_CORBA_BAD_PARAM,
					   CORBA_COMPLETED_NO);
		return NULL;
	}

	return ORBit_RootObject_dup(obj->parent_poa);
}

PortableServer_POAManager
PortableServer_POA__get_the_POAManager(PortableServer_POA obj,
				       CORBA_Environment *ev)
{
	if(!obj) {
		CORBA_exception_set_system(ev,
					   ex_CORBA_BAD_PARAM,
					   CORBA_COMPLETED_NO);
		return NULL;
	}

	return ORBit_RootObject_dup(obj->the_POAManager);
}

PortableServer_AdapterActivator PortableServer_POA__get_the_activator(PortableServer_POA obj, CORBA_Environment *ev)
{
	if(!obj) {
		CORBA_exception_set_system(ev,
					   ex_CORBA_BAD_PARAM,
					   CORBA_COMPLETED_NO);
		return NULL;
	}

	return obj->the_activator;
}

void PortableServer_POA__set_the_activator(PortableServer_POA obj, PortableServer_AdapterActivator the_activator, CORBA_Environment *ev)
{
	if(!obj) {
		CORBA_exception_set_system(ev,
					   ex_CORBA_BAD_PARAM,
					   CORBA_COMPLETED_NO);
		return;
	}

	obj->the_activator = the_activator;
}

PortableServer_ServantManager PortableServer_POA_get_servant_manager(PortableServer_POA obj, CORBA_Environment *ev)
{
	if(!obj) {
		CORBA_exception_set_system(ev, ex_CORBA_BAD_PARAM,
					   CORBA_COMPLETED_NO);
		return NULL;
	}

	if(obj->request_processing != PortableServer_USE_SERVANT_MANAGER) {
		CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
				    ex_PortableServer_POA_WrongPolicy,
				    NULL);
		return NULL;
	}

	return obj->servant_manager;
}

void PortableServer_POA_set_servant_manager(PortableServer_POA obj, PortableServer_ServantManager imgr, CORBA_Environment *ev)
{
	if(!obj) {
		CORBA_exception_set_system(ev, ex_CORBA_BAD_PARAM,
					   CORBA_COMPLETED_NO);
		return;
	}

	if(obj->request_processing != PortableServer_USE_SERVANT_MANAGER) {
		CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
				    ex_PortableServer_POA_WrongPolicy,
				    NULL);
		return;
	}

	obj->servant_manager = imgr;
}

PortableServer_Servant PortableServer_POA_get_servant(PortableServer_POA obj, CORBA_Environment *ev)
{
	if(!obj) {
		CORBA_exception_set_system(ev, ex_CORBA_BAD_PARAM,
					   CORBA_COMPLETED_NO);
		return NULL;
	}

	if(obj->request_processing != PortableServer_USE_DEFAULT_SERVANT) {
		CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
				    ex_PortableServer_POA_WrongPolicy,
				    NULL);
		return NULL;
	}

	return obj->default_pobj ? obj->default_pobj->servant : 0;
}

/**
    We treat the default servant somewhat like a normal object reference
    that is being invoked via a bypass interface.
    As an extention to the standard, if p_servant is NULL, then
    we just unset the prior servant and don't add another.
**/
void PortableServer_POA_set_servant(PortableServer_POA obj, PortableServer_Servant p_servant, CORBA_Environment *ev)
{
	PortableServer_ServantBase *servant = p_servant;
	ORBit_POAObject 	*pobj;

	if(!obj) {
		CORBA_exception_set_system(ev, ex_CORBA_BAD_PARAM,
					   CORBA_COMPLETED_NO);
		return;
	}
	if ( (pobj=obj->default_pobj) !=0 ) {
	    obj->default_pobj = 0;
	    ORBit_POA_deactivate_object(obj, pobj, /*ether*/0, /*clean*/0);
    	    ORBit_RootObject_release(pobj);
	    pobj = 0;
	}
	if ( servant == 0 )
	    return;	/* not standard! */

	if(obj->request_processing != PortableServer_USE_DEFAULT_SERVANT) {
		CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
				    ex_PortableServer_POA_WrongPolicy,
				    NULL);
		return;
	}
	/* next line may be be required, but I'm not sure what to do... */
	g_assert( (pobj=ORBIT_SERVANT_TO_POAOBJECT(servant)) == 0 );
	pobj = ORBit_POA_create_object(obj, /*oid*/NULL, /*isDef*/TRUE, ev);
	obj->default_pobj = ORBit_RootObject_dup(pobj);
	/* this doesn't activate it in the active-object-map sense,
	 * but it happens to do the "right" thing. */
	ORBit_POA_activate_object(obj, pobj, servant, ev);
}


static CORBA_ORB
get_orb_for_poa(PortableServer_POA poa)
{
	if(poa->orb)
		return poa->orb;
	if(poa->parent_poa)
		return get_orb_for_poa(poa->parent_poa);

	return CORBA_OBJECT_NIL;
}

PortableServer_ObjectId *
PortableServer_POA_activate_object(PortableServer_POA obj,
				   PortableServer_Servant p_servant,
				   CORBA_Environment *ev)
{
	PortableServer_ServantBase *servant = p_servant;
	ORBit_POAObject 	*newobj;

	if(obj->servant_retention != PortableServer_RETAIN
	   || obj->id_assignment != PortableServer_SYSTEM_ID) {
		CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
				    ex_PortableServer_POA_WrongPolicy,
				    NULL);
		return NULL;
	}
	
	if((obj->id_uniqueness==PortableServer_UNIQUE_ID) &&
	   (ORBIT_SERVANT_TO_POAOBJECT(servant) != 0)) {
		CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
				    ex_PortableServer_POA_ServantAlreadyActive,
				    NULL);
		return NULL;
	}

	newobj = ORBit_POA_create_object(obj, /*oid*/NULL, /*isDef*/FALSE, ev);
	ORBit_POA_activate_object(obj, newobj, servant, ev);
	return ORBit_sequence_octet_dup(newobj->object_id);
}

/*
    V2.3 sec 11.3.8.16
    Raises WrongPolicy if not RETAIN.
    Raises ObjectAlreadyActive if {id} is already bound.
    Raises ServantAlreadyActive if {servant} is bound and UNIQUE.
    If SYSTEM_ID policy, the {id} *must* have been previously created
    by this POA.
*/
void
PortableServer_POA_activate_object_with_id(PortableServer_POA obj,
					   PortableServer_ObjectId *id,
					   PortableServer_Servant p_servant,
					   CORBA_Environment *ev)
{
	PortableServer_ServantBase *servant = p_servant;
	ORBit_POAObject *newobj;

	ev->_major = CORBA_NO_EXCEPTION;
	if(!obj || !id || !p_servant) {
		CORBA_exception_set_system(ev, ex_CORBA_BAD_PARAM,
					   CORBA_COMPLETED_NO);
		return;
	}

	newobj = ORBit_POA_oid_to_obj(obj, id, /*active*/0, ev);
	if ( ev->_major )
		return;
	if ( newobj && newobj->servant!=0 ) {
		CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
				    ex_PortableServer_POA_ObjectAlreadyActive, 
				    NULL);
		return;
	}
	if((obj->id_uniqueness==PortableServer_UNIQUE_ID) &&
	   (ORBIT_SERVANT_TO_POAOBJECT(servant) != 0)) {
		CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
				    ex_PortableServer_POA_ServantAlreadyActive,
				    NULL);
		return;
	}
	if ( newobj==0 )
	    newobj = ORBit_POA_create_object(obj, id, /*isDef*/FALSE, ev);
	ORBit_POA_activate_object(obj, newobj, servant, ev);
}

/*
    V2.3 sec 11.3.8.17
    Raises WrongPolicy is not RETAIN.
    This never waits for etherealization to complete.
*/
void
PortableServer_POA_deactivate_object(PortableServer_POA obj,
				     PortableServer_ObjectId *oid,
				     CORBA_Environment *ev)
{
	ORBit_POAObject *oldobj;

	ev->_major = CORBA_NO_EXCEPTION;
	if(!obj || !oid) {
		CORBA_exception_set_system(ev, ex_CORBA_BAD_PARAM,
					   CORBA_COMPLETED_NO);
		return;
	}
	oldobj = ORBit_POA_oid_to_obj(obj, oid, /*active*/1, ev);
	if ( ev->_major )
		return;
	if(!oldobj) {
		CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
				    ex_PortableServer_POA_ObjectNotActive,
				    NULL);
		return;
	}
	ORBit_POA_deactivate_object(obj, oldobj, 
	  /*etherealize_objects*/CORBA_TRUE,
	  /*cleanup_in_progress*/CORBA_FALSE);
}


/**
    V2.3 sec 11.3.8.20
    Raises ServantNotActive, WrongPolicy.
    The coding and structure of this is very similar to servant_to_reference,
    see sec 21.
**/
PortableServer_ObjectId *PortableServer_POA_servant_to_id(PortableServer_POA obj, PortableServer_Servant p_servant, CORBA_Environment *ev)
{
	PortableServer_ServantBase *servant = p_servant;
	int defserv = obj->request_processing == PortableServer_USE_DEFAULT_SERVANT;
	int retain = obj->servant_retention == PortableServer_RETAIN;
	int implicit = obj->implicit_activation == PortableServer_IMPLICIT_ACTIVATION;
	int unique = obj->id_uniqueness==PortableServer_UNIQUE_ID;
	int policy_ok = defserv || (retain && (unique || implicit));
	ORBit_POAObject *pobj = ORBIT_SERVANT_TO_POAOBJECT(servant);

	g_return_val_if_fail(p_servant != NULL, NULL);

	if ( retain && unique && pobj && pobj->servant==p_servant ) {
	    return ORBit_sequence_octet_dup(pobj->object_id);
	} else if ( retain && implicit && (!unique || pobj==0) ) {
	    pobj = ORBit_POA_create_object(obj, /*oid*/NULL, /*isDef*/0, ev);
	    ORBit_POA_activate_object(obj, pobj, servant, ev);
	    /* XXX: seems like above activate could fail in many ways! */
	    return ORBit_sequence_octet_dup(pobj->object_id);
	} else {
	    /*
	     * This handles case 3 of the spec; but is broader:
	     * it matches invokations on any type of servant, not
	     * just the default servant.
	     * The stricter form could be implemented, 
	     * but it would only add more code...
	     */
	    ORBit_POAInvokation	*ik = obj->orb->poa_current_invokations;
	    for ( ; ik; ik = ik->prev) {
	    	if ( ik->pobj->servant == p_servant ) {
	    	    return ORBit_sequence_octet_dup(ik->pobj->object_id);
		}
	    }
	}
	CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			policy_ok ? ex_PortableServer_POA_ServantNotActive
			: ex_PortableServer_POA_WrongPolicy,
			NULL);
	return NULL;
}


/*
	Public methods where references are is created (0=NULL, V=valid):
					oid	pobj	type
	servant_to_reference 		0	V	0
	id_to_reference			0	V	0
	create_reference[RETAIN]	0	V	V
	create_reference[NON_RETAIN] 	V	0	V
	create_ref..._with_id[RETAIN]	0	V	V
	create_ref.._with_id[NONRETAIN]	V	0	V
*/
static CORBA_Object
ORBit_POA_oid_to_ref(PortableServer_POA obj,
				      PortableServer_ObjectId *oid,
				      const char *type_id,
				      CORBA_Environment *ev)
{
	GSList *profiles=NULL;
	ORBit_Object_info *object_info;
	CORBA_Object retval;
	CORBA_ORB orb;

	g_assert( oid && type_id );

	orb = obj->the_POAManager->orb;

	/* Do the local connection first, so it will be attempted first by
	   the client parsing the IOR string
	 */
	if(orb->cnx.ipv6 || orb->cnx.usock) {
		object_info = g_new0(ORBit_Object_info, 1);

		object_info->profile_type=IOP_TAG_ORBIT_SPECIFIC;
		object_info->iiop_major = 1;
		object_info->iiop_minor = 0;
		ORBit_POA_oid_to_okey(obj, oid, &object_info->object_key);

#ifdef HAVE_IPV6
		if(orb->cnx.ipv6) {
			object_info->tag.orbitinfo.ipv6_port =
				ntohs(IIOP_CONNECTION(orb->cnx.ipv6)->u.ipv6.location.sin6_port);
		}
#endif
		if(orb->cnx.usock) {
			object_info->tag.orbitinfo.unix_sock_path =
				g_strdup(IIOP_CONNECTION(orb->cnx.usock)->u.usock.sun_path);
		}
		ORBit_set_object_key(object_info);
		profiles=g_slist_append(profiles, object_info);
	}

	if(orb->cnx.ipv4) {
		object_info=g_new0(ORBit_Object_info, 1);

		object_info->profile_type = IOP_TAG_INTERNET_IOP;
		object_info->iiop_major = 1;
		object_info->iiop_minor = 0;
		ORBit_POA_oid_to_okey(obj, oid, &object_info->object_key);

		object_info->tag.iopinfo.host = g_strdup(IIOP_CONNECTION(orb->cnx.ipv4)->u.ipv4.hostname);
		object_info->tag.iopinfo.port = ntohs(IIOP_CONNECTION(orb->cnx.ipv4)->u.ipv4.location.sin_port);

		ORBit_set_object_key(object_info);
		profiles=g_slist_append(profiles, object_info);
	}

	retval = ORBit_create_object_with_info(profiles, type_id, orb, ev);
	/* We depend upon ORBit_CORBA_Object_new() to zero out retval */
	return retval;
}

static CORBA_Object
ORBit_POA_obj_to_ref(PortableServer_POA poa,
				      ORBit_POAObject *pobj,
				      const char *type_id,
				      CORBA_Environment *ev)
{
	CORBA_Object	objref;
	g_assert( pobj );
	if ( type_id==0 ) {
		g_assert( pobj->servant );
		type_id = ORBIT_SERVANT_TO_CLASSINFO(pobj->servant)->class_name;
	}
	objref = ORBit_POA_oid_to_ref(poa, pobj->object_id, type_id, ev);
	if ( ev->_major )
		return NULL;
	objref->bypass_obj = ORBit_RootObject_dup(pobj);
	return objref;
}

/*
    V2.3 sec 11.3.8.21
    RAISES ServantNotActive and WrongPolicy

    In the (!unique && implict) case, spec says it is optional to actually 
    activate it; this can occur later.
*/
CORBA_Object
PortableServer_POA_servant_to_reference(PortableServer_POA obj, PortableServer_Servant p_servant, CORBA_Environment *ev)
{
	PortableServer_ServantBase *servant = p_servant;
	ORBit_POAObject *pobj = ORBIT_SERVANT_TO_POAOBJECT(servant);
	int retain = obj->servant_retention == PortableServer_RETAIN;
	int implicit = obj->implicit_activation == PortableServer_IMPLICIT_ACTIVATION;
	int unique = obj->id_uniqueness==PortableServer_UNIQUE_ID;
	int policy_ok = retain && (unique || implicit);

	if ( retain && unique && pobj ) {
		/* pobj!=NULL --> servant active */
		return ORBit_POA_obj_to_ref(obj, pobj, /*type*/NULL, ev);
	} else if ( retain && implicit && (!unique || pobj==0) ) {
		pobj = ORBit_POA_create_object(obj, /*oid*/NULL, /*isDef*/FALSE, ev);
		ORBit_POA_activate_object(obj, pobj, servant, ev);
	        /* XXX: seems like above activate could fail in many ways! */
		return ORBit_POA_obj_to_ref(obj, pobj, /*type*/NULL, ev);
	} else {
	    /* This case deals with "invoked in the context of
	     * executing a request." Note that there are no policy
	     * restrictions for this case. We must do a forward search
	     * looking for matching {servant}. If unique, we could 
	     * go backward from servant to pobj to use_cnt, but we
	     * dont do this since forward search is more general 
	     */
	    ORBit_POAInvokation	*ik = obj->orb->poa_current_invokations;
	    for ( ; ik; ik = ik->prev) {
	    	if ( ik->pobj->servant == p_servant ) {
		    return ORBit_POA_obj_to_ref(obj, pobj, /*type*/NULL, ev);
		}
	    }
	}
	CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			    policy_ok ? ex_PortableServer_POA_ServantNotActive
			    : ex_PortableServer_POA_WrongPolicy,
			    NULL);
	return NULL;
}

/**
    Actually, all profiles have the same okey, so the loop is kind of silly.
**/
static CORBA_sequence_octet*
POA_find_ref_okey(CORBA_Object ref)
{
	GSList *cur;
	for(cur = ref->profile_list; cur; cur = cur->next) {
		ORBit_Object_info *curprof = cur->data;
		if ( curprof->object_key._length > 0 )
			return &curprof->object_key;
	}
	return NULL;
}

PortableServer_Servant
PortableServer_POA_reference_to_servant(PortableServer_POA obj, CORBA_Object reference, CORBA_Environment *ev)
{
	gboolean policy_ok = 0;
	g_assert(reference);

	if(obj->request_processing != PortableServer_USE_DEFAULT_SERVANT
	   && obj->servant_retention != PortableServer_RETAIN) {
		CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
				    ex_PortableServer_POA_WrongPolicy,
				    NULL);
		return NULL;
	}

	if ( obj->servant_retention == PortableServer_RETAIN ) {
		CORBA_sequence_octet *okey;
		ORBit_POAObject *pobj;
		policy_ok = 1;
		if ( (pobj=reference->bypass_obj) && pobj->servant )
			return pobj->servant;

		if ( (okey=POA_find_ref_okey(reference))
		  && (pobj=ORBit_POA_okey_to_obj(obj, okey))
		  && pobj->servant ) {
			return pobj->servant;
		}
	}

	if ( obj->request_processing == PortableServer_USE_DEFAULT_SERVANT ) {
		policy_ok = 1;
	  	if ( obj->default_pobj )
			return obj->default_pobj->servant;
	}

	CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			policy_ok ? ex_PortableServer_POA_ObjectNotActive
			  	  : ex_PortableServer_POA_WrongPolicy,
			    NULL);
	return NULL;
}

/**
    V2.3 sec 11.3.8.23
    This "should" work in all policies. 
    Raises WrongAdaptor if {reference} doesnt belong to this POA.
    The referred-to object need not be active. (That is, oids exist
    for in-active objects).

    With RETAIN, we could use the bypass pointer; however, I'm not sure
    that would work in all cases (e.g., when we send a ref remotely, and then
    it gets send back to us). If you need this func to run faster give bypass
    a try.
**/
PortableServer_ObjectId *PortableServer_POA_reference_to_id(PortableServer_POA obj, CORBA_Object reference, CORBA_Environment *ev)
{
	PortableServer_ObjectId oid;
	CORBA_sequence_octet *okey;

	if ( (okey=POA_find_ref_okey(reference))
	  && ORBit_POA_okey_to_oid(obj, okey, &oid) )
		return ORBit_sequence_octet_dup(&oid);

	CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
				    ex_PortableServer_POA_WrongAdapter,
				    NULL);
	return NULL;
}

/**
	V2.3 sec 11.3.8.24
	Raises WrongPolicy if not RETAIN nor USE_DEFAULT_SERVANT.
	Raises ObjectNotActive if corresponding servant not active.
	Note that RETAIN and USE_DEFAULT_SERVANT might both apply.
**/
PortableServer_Servant PortableServer_POA_id_to_servant(PortableServer_POA obj, PortableServer_ObjectId *oid, CORBA_Environment *ev)
{
	PortableServer_Servant	servant = 0;
	gboolean		policy_ok = 0;

	if ( obj->servant_retention == PortableServer_RETAIN ) {
		ORBit_POAObject *pobj;
		policy_ok = 1;
		if ( (pobj = ORBit_POA_oid_to_obj(obj, oid, 
		  /*active*/1, /*ev*/0))!=0 )
			servant = pobj->servant;
	}
	if ( servant==0 
	  && obj->request_processing == PortableServer_USE_DEFAULT_SERVANT ) {
		policy_ok = 1;
	  	servant = obj->default_pobj ? obj->default_pobj->servant : 0;
	}
	if(servant==0) {
		CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			policy_ok ? ex_PortableServer_POA_ObjectNotActive
			  	  : ex_PortableServer_POA_WrongPolicy,
		    	NULL);
	}
	return servant;
}


/**
	V2.3 sec 11.3.8.25
	Raises WrongPolicy if not RETAIN
	Raises ObjectNotActive if servant for {oid} is not active.
	(NOTE: We could easily construct references for un-active oids,
	but CORBA artifically constrains this method for some reason.)
**/
CORBA_Object PortableServer_POA_id_to_reference(PortableServer_POA obj,
						PortableServer_ObjectId *oid,
						CORBA_Environment *ev)
{
	ORBit_POAObject *pobj;
	pobj = ORBit_POA_oid_to_obj(obj, oid, /*active*/1, ev);
	if ( ev->_major )
		return NULL;	/* may raise WrongPolicy or ObjectNotActive */
	return ORBit_POA_obj_to_ref(obj, pobj, /*type*/NULL, ev);
}


/**
	V2.3 sec 11.3.8.18
	Raises WrongPolicy if not SYSTEM_ID
	This always creates a new ObjectId, and thus always creates
	a new objref.
	While this never triggers an activation, the RETAIN policy will
	cause the creation of an in-active POAObject.
**/
CORBA_Object
PortableServer_POA_create_reference(PortableServer_POA obj,
				    CORBA_RepositoryId intf,
				    CORBA_Environment *ev)
{
	if ( obj->id_assignment != PortableServer_SYSTEM_ID ) {
		CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			ex_PortableServer_POA_WrongPolicy, NULL);
		return NULL;
	}
	if( obj->servant_retention == PortableServer_RETAIN ) {
		ORBit_POAObject	*pobj;
		pobj = ORBit_POA_create_object(obj, /*oid*/NULL, /*isDef*/0, ev);
		return ORBit_POA_obj_to_ref(obj, pobj, intf, ev);
	} else {
		/* NON_RETAIN */
		PortableServer_ObjectId	oid;
		ORBit_POA_make_sysoid(obj, &oid);
		return ORBit_POA_oid_to_ref(obj, &oid, intf, ev);
	}
}

/**
	V2.3 sec 11.3.8.19
	No POA exceptions. May return BAD_PARAM or seg fault if
	called with malformed oid under SYSTEM_ID.
	While this never triggers an activation, the RETAIN policy may
	cause the creation of a POAObject if {oid} is new.

	This is trickier than most of these methods, because
	{oid} may or may not already be known, and may or may not
	already be active. If not already known and RETAIN, we have a 
	design choice to immediately create the POAObject, or
	defer it. For now, we do it immediately so that the bypass_obj
	can be correctly setup. However, under a DEFAULT_SERVANT
	policy this could be very expensive.
**/
CORBA_Object
PortableServer_POA_create_reference_with_id(PortableServer_POA obj,
					    PortableServer_ObjectId *oid,
					    CORBA_RepositoryId intf,
					    CORBA_Environment *ev)
{
	if ( obj->servant_retention == PortableServer_RETAIN ) {
		ORBit_POAObject	*pobj;
		pobj = ORBit_POA_oid_to_obj(obj, oid, /*active*/0, /*ev*/0);
		if ( pobj == NULL ) {
			pobj = ORBit_POA_create_object(obj, oid, /*isDef*/0, ev);
		}
		return ORBit_POA_obj_to_ref(obj, pobj, intf, ev);
	}
	return ORBit_POA_oid_to_ref(obj, oid, intf, ev);
}

/*****************************************************************************
 *
 *			POACurrent
 *
 *****************************************************************************/

static void
ORBit_POACurrent_free_fn(gpointer obj_in) {
    PortableServer_Current poacur = obj_in;
    ORBit_RootObject_release(poacur->orb);
    g_free(poacur);
}

static const ORBit_RootObject_Interface ORBit_POACurrent_epv = {
    ORBIT_ROT_POACURRENT,
    ORBit_POACurrent_free_fn
};

/**
    The returned object has already been dup'd; caller must free
    if it doesnt want it!
**/
PortableServer_Current
ORBit_POACurrent_new(CORBA_ORB orb) {
    PortableServer_Current poacur;
    poacur = (PortableServer_Current) 
      g_new0(struct PortableServer_Current_type, 1);
    ORBit_RootObject_init(&poacur->parent, &ORBit_POACurrent_epv);
    poacur->orb = ORBit_RootObject_dup(orb);
    return ORBit_RootObject_dup(poacur);
}

static ORBit_POAInvokation*
ORBit_POACurrent_get_invokation(PortableServer_Current obj, 
CORBA_Environment *ev) {
    g_assert( obj && obj->parent.interface->type == ORBIT_ROT_POACURRENT );
    if ( obj->orb->poa_current_invokations == 0 ) {
    	CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
				ex_PortableServer_Current_NoContext,
				NULL);
	return 0;
    }
    return obj->orb->poa_current_invokations;
}

PortableServer_POA PortableServer_Current_get_POA(PortableServer_Current obj, CORBA_Environment *ev)
{
    ORBit_POAInvokation	*invoke;
    if ( (invoke=ORBit_POACurrent_get_invokation(obj, ev))==0 )
    	return 0;
    return ORBit_RootObject_dup(invoke->pobj->poa);
}

PortableServer_ObjectId *PortableServer_Current_get_object_id(PortableServer_Current obj, CORBA_Environment *ev)
{
    ORBit_POAInvokation	*invoke;
    if ( (invoke=ORBit_POACurrent_get_invokation(obj, ev))==0 )
	return 0;
    return ORBit_sequence_octet_dup(invoke->oid 
      ? invoke->oid : invoke->pobj->object_id);
}


/***********************************************************************
 *
 *		Object to/from strings
 *
 * These are specified in the C language mapping spec, not the base
 * spec. See sec 1.26.2. These are meanful only under USER_ID policy,
 * since SYSTEM_ID oids have NULs in them. Probably they would only
 * be used with MULTIPLE_ID/DEFAULT_SERVANT policies.
 * Using strings as oids may be convenient, but it seems horribly slow.
 *
 ***********************************************************************/

CORBA_char *PortableServer_ObjectId_to_string(PortableServer_ObjectId *id, 
  CORBA_Environment *ev)
{
#if 1
	CORBA_char	*str;
	if ( memchr( id->_buffer, 0, id->_length) ) {
		/* we could try to escape it, but the spec
		 * specifically alllows us to throw an exception. */
		CORBA_exception_set_system(ev, ex_CORBA_BAD_PARAM,
					   CORBA_COMPLETED_NO);
		return NULL;
	}
	str = CORBA_string_alloc(id->_length);
	memcpy( str, id->_buffer, id->_length);
	str[id->_length] = 0;
	return str;
#else
	return ORBit_sequence_octet_to_hexstrdup(id);
#endif
}

CORBA_wchar *PortableServer_ObjectId_to_wstring(PortableServer_ObjectId *id, CORBA_Environment *env)
{
	g_assert(!"Not yet implemented");
	return(NULL);
}

PortableServer_ObjectId *PortableServer_string_to_ObjectId(CORBA_char *str, CORBA_Environment *env)
{
#if 1
	CORBA_sequence_octet	tmp;
	tmp._length = strlen(str);
	tmp._buffer = str;
	return ORBit_sequence_octet_dup(&tmp);
#else
	return ORBit_sequence_octet_create_from_hexstr(str);
#endif
}

PortableServer_ObjectId *PortableServer_wstring_to_ObjectId(CORBA_wchar *str, CORBA_Environment *env)
{
	g_assert(!"Not yet implemented");
	return(NULL);
}

PortableServer_POA PortableServer_ServantBase__default_POA(PortableServer_Servant servant, CORBA_Environment *ev)
{
	g_return_val_if_fail(servant, NULL);
	return ORBIT_SERVANT_TO_POAOBJECT(servant)->poa;
}

void PortableServer_ServantLocator_preinvoke(PortableServer_ObjectId *oid, PortableServer_POA adapter, CORBA_Identifier op_name, PortableServer_ServantLocator_Cookie *cookie)
{
	g_assert(!"Not yet implemented");
	return;
}

void PortableServer_ServantLocator_postinvoke(PortableServer_ObjectId *oid, PortableServer_POA adapter, CORBA_Identifier op_name, PortableServer_ServantLocator_Cookie cookie, PortableServer_Servant servant)
{
	g_assert(!"Not yet implemented");
	return;
}

void PortableServer_ServantBase__init(PortableServer_Servant p_servant,
				      CORBA_Environment *ev)
{
	PortableServer_ServantBase *servant = p_servant;
	/* NOTE: servant must be init'd before POAObj activated */
	g_assert( servant->_private == 0 );
}

void PortableServer_ServantBase__fini(PortableServer_Servant p_servant,
				      CORBA_Environment *ev)
{
	/* NOTE: servant must be fini'd after POAObj de-activated */
	PortableServer_ServantBase *servant = p_servant;
	g_assert( servant->_private == 0 );
}


void
POA_PortableServer_ServantActivator__init(PortableServer_Servant servant,
					CORBA_Environment * ev)
{
	static PortableServer_ClassInfo class_info =
	{NULL,
	"IDL:omg.org/PortableServer/ServantActivator:1.0",
	NULL};

	PortableServer_ServantBase__init(((PortableServer_ServantBase *) servant), ev);
	ORBIT_SERVANT_SET_CLASSINFO(servant, &class_info);
}

void
POA_PortableServer_ServantActivator__fini(PortableServer_Servant servant,
					CORBA_Environment * ev)
{
	PortableServer_ServantBase__fini(servant, ev);
}

void
POA_PortableServer_ServantLocator__init(PortableServer_Servant servant,
					CORBA_Environment * ev)
{
	static PortableServer_ClassInfo class_info =
	{NULL,
	"IDL:omg.org/PortableServer/ServantLocator:1.0",
	NULL};

	PortableServer_ServantBase__init(((PortableServer_ServantBase *)servant), ev);
	ORBIT_SERVANT_SET_CLASSINFO(servant, &class_info);
}

void
POA_PortableServer_ServantLocator__fini(PortableServer_Servant servant,
					CORBA_Environment * ev)
{
	PortableServer_ServantBase__fini(servant, ev);
}
