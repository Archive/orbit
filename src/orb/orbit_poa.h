/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  ORBit: A CORBA v2.2 ORB
 *
 *  Copyright (C) 1998 Richard H. Porter and Red Hat Software
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Phil Dawes <philipd@parallax.co.uk>
 *
 */

/*
 *   ORBit specific POA funcitons.
 *
 */

#ifndef _ORBIT_ORBIT_POA_H_
#define _ORBIT_ORBIT_POA_H_

#include "orbit_types.h"
#include "orbit_poa_type.h"

/*
 * ClassInfo operations
 */
extern void
ORBit_classinfo_register(	PortableServer_ClassInfo *ci);

extern void
ORBit_classinfo_fini(		void);

extern CORBA_unsigned_long
ORBit_classinfo_lookup_id(	const char *repo_id);

/*
 * POA Manager operations
 */
extern PortableServer_POAManager 
ORBit_POAManager_new(		CORBA_ORB orb, 
				CORBA_Environment *ev);

/*
 * POA operations
 */

extern ORBit_PortableServer_OkeyrandPolicy
ORBit_PortableServer_POA_create_okeyrand_policy(PortableServer_POA poa,
				CORBA_short poa_rand_len,
				CORBA_short obj_rand_len,
				CORBA_Environment *ev);


extern PortableServer_POA 
ORBit_POA_new(			CORBA_ORB orb,
				CORBA_char *adapter_name,
				PortableServer_POAManager the_POAManager,
				CORBA_PolicyList *policies,
				CORBA_Environment *ev);
extern CORBA_boolean
ORBit_POA_deactivate(		PortableServer_POA poa,
				CORBA_boolean etherealize_objects,
			  	CORBA_Environment *ev);

extern CORBA_boolean
ORBit_POA_destroy(		PortableServer_POA poa,
				CORBA_boolean etherealize_objects,
			  	CORBA_Environment *ev);

extern CORBA_boolean
ORBit_POA_is_inuse(		PortableServer_POA poa,
				CORBA_boolean consider_children,
				CORBA_Environment *ev);

extern void 
ORBit_POA_add_child(		PortableServer_POA poa,
				PortableServer_POA child_poa,
				CORBA_Environment *ev);
extern void 
ORBit_POA_remove_child(		PortableServer_POA poa,
			    	PortableServer_POA child_poa,
			    	CORBA_Environment *ev);

extern void 
ORBit_POA_handle_request(	PortableServer_POA poa,
				GIOPRecvBuffer *recv_buffer);
extern void 
ORBit_POA_handle_held_requests(	PortableServer_POA poa);

/*
 * POA Object operations
 */

void
ORBit_POA_oid_to_okey(		PortableServer_POA poa,
				PortableServer_ObjectId *oid,
				CORBA_sequence_octet *okey);

CORBA_boolean
ORBit_POA_okey_to_oid(  	PortableServer_POA poa,
                        	CORBA_sequence_octet *okey,
			        PortableServer_ObjectId *oid);

PortableServer_POA
ORBit_POA_okey_to_poa(  	PortableServer_POA root_poa,
                        	CORBA_sequence_octet *okey);

extern ORBit_POAObject*
ORBit_POA_okey_to_obj(		PortableServer_POA poa, 
				CORBA_sequence_octet *okey);

extern ORBit_POAObject*
ORBit_POA_oid_to_obj(		PortableServer_POA poa,   
                		PortableServer_ObjectId *oid,
				CORBA_boolean only_active,
		                CORBA_Environment *ev);

extern void
ORBit_POA_deactivate_object(	PortableServer_POA poa, 
				ORBit_POAObject *obj,
                             	CORBA_boolean do_etherialize,
                             	CORBA_boolean is_cleanup);

extern void
ORBit_POA_make_sysoid(		PortableServer_POA poa,
				PortableServer_ObjectId *new_oid);

extern ORBit_POAObject*
ORBit_POA_create_object(	PortableServer_POA poa, 
				PortableServer_ObjectId *oid,
				gboolean isDefault,
				CORBA_Environment *ev);
extern void 
ORBit_POA_activate_object(	PortableServer_POA poa, 
				ORBit_POAObject *obj,
				PortableServer_ServantBase *servant,
				CORBA_Environment *ev);

extern void
ORBit_POAObject_post_invoke(	ORBit_POAObject *obj);

#ifdef NOT_BACKWARDS_COMPAT_0_4
/* Bad hack for shared libraries */
void ORBit_servant_set_deathwatch(PortableServer_ServantBase *servant,
				  int *use_count,
				  GFunc death_func,
				  gpointer user_data);
#endif

/*
 * POACurrent operations
 */
extern PortableServer_Current ORBit_POACurrent_new(CORBA_ORB orb);


#endif /* !_ORBIT_ORBIT_POA_H_ */
