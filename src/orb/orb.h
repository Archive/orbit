/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  ORBit: A CORBA v2.2 ORB
 *
 *  Copyright (C) 1998 Richard H. Porter
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Dick Porter <dick@cymru.net>
 *
 */

#ifndef _ORBIT_ORB_H_
#define _ORBIT_ORB_H_

#include "orb/orbit_types.h"
/* #include "orb/interface_repository.h" */

extern CORBA_ORB CORBA_ORB_init(
	int *argc,
	char **argv,
	CORBA_ORBid orb_identifier,
	CORBA_Environment *ev);


/* ORBit extention */
extern void CORBA_ORB_set_initial_reference(
	CORBA_ORB orb,
	CORBA_ORB_ObjectId identifier,
	CORBA_Object obj,
	CORBA_Environment *ev);

extern CORBA_Current *CORBA_ORB_get_current(
	CORBA_ORB orb,
	CORBA_Environment *ev);

#endif /* !_ORBIT_ORB_H_ */
