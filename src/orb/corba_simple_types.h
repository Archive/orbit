/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  ORBit: A CORBA v2.2 ORB
 *
 *  Copyright (C) 1998 Richard H. Porter
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Dick Porter <dick@cymru.net>
 *
 */

#ifndef _ORBIT_CORBA_SIMPLE_TYPES_H_
#define _ORBIT_CORBA_SIMPLE_TYPES_H_

/**
    This module defines simple types shared by both IIOP and core ORB.
    It should stand on its own without pulling in the entire CORBA IDL.
**/

#include <ORBitutil/basic_types.h>


/**
    CORBA_string is not defined by the standard, and apps should
    avoid using it. It is defined here to make the orbit idl compiler
    happy. It derives the name "CORBA_string" as the element type for
    arrays and sequences. Right solution is to teach compiler to substitute
    `CORBA_char*', but I dont know how to do that.
**/
typedef CORBA_char *CORBA_string;

/**
    As far as I can tell, CORBA_Status is not defined by OMG. I don't
    know why we use it.
**/
typedef void CORBA_Status;

/* Generic sequence */
struct CORBA_Sequence_type {
	CORBA_unsigned_long _maximum;
	CORBA_unsigned_long _length;
	void *_buffer;
	CORBA_boolean _release;
};

typedef struct CORBA_sequence_octet_struct {
	CORBA_unsigned_long _maximum;
	CORBA_unsigned_long _length;
	CORBA_octet *_buffer;
	CORBA_boolean _release;
} CORBA_sequence_octet;

#define _CORBA_sequence_CORBA_octet_defined 1
typedef CORBA_sequence_octet CORBA_sequence_CORBA_octet;
/* NOTE: the TC_CORBA_sequence_CORBA_octet is not defined here! */


#include <orb/corba_sequences.h>
#endif /* !_ORBIT_CORBA_SIMPLE_TYPES_H_ */
