/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* Elliot's stuff */
/* This is somewhat a mess, because I tried to make it easy to add
   select() support, and as a result #ifdef's litter the land. */

#include "orbit.h"
#include "orbit_poa.h"
#include "orbit_poa_type.h"
#include "sequences.h"
#ifdef HAVE_SYS_POLL_H
#include <sys/poll.h>
#endif
#include <sys/types.h>
#include <sys/socket.h>
#include <IIOP/IIOP-private.h>

/* We need:
   a way to find out what FD's need to be selected on
   a dummy main loop to implement the CORBA_ORB_run() routine;
*/

gboolean orb_server_keep_running = FALSE;

ORBit_request_validate ORBIT_request_validator = NULL;

/* function protos */
static void ORBit_handle_incoming_message(GIOPRecvBuffer *recv_buffer);

void
ORBit_custom_run_setup(CORBA_ORB orb, CORBA_Environment *ev)
{
	IIOPIncomingMessageHandler = ORBit_handle_incoming_message;
}

void
CORBA_ORB_run(CORBA_ORB orb, CORBA_Environment *ev)
{
	ORBit_custom_run_setup(orb, ev);

	orb_server_keep_running = TRUE;

	giop_main();
}


static PortableServer_POA
ORBit_ORB_find_POA_for_okey(CORBA_ORB orb, CORBA_sequence_octet *okey)
{
    PortableServer_POA	 poa;
    ORBit_SrvForwBind	*bd;

    /* try it as a directly encoded okey (POAnum is first 4 bytes) */
    if ( (poa=ORBit_POA_okey_to_poa(orb->root_poa, okey)) != 0 ) {
	return poa;
    }
    /* try it as a server-side forwarded okey */
    if ( orb->srv_forw_bindings != 0
      && (bd=g_hash_table_lookup(orb->srv_forw_bindings, okey)) != 0 ) {
	if ( bd->to_okey._length > 0 ) {
		/* This is a bit of a hack! */
	    g_assert( okey->_release == 0 );
	    okey->_length = bd->to_okey._length;
	    okey->_buffer = bd->to_okey._buffer;
	    return ORBit_ORB_find_POA_for_okey(orb, okey);
	}
    }
    return 0;
}

static void
ORBit_handle_incoming_request(GIOPRecvBuffer *recv_buffer)
{
	CORBA_ORB orb;
	PortableServer_POA poa;
	GIOPConnection *connection;
	ORBit_MessageValidationResult mvr;
	gboolean do_unuse = TRUE;

	g_assert(recv_buffer);

	connection = GIOP_MESSAGE_BUFFER(recv_buffer)->connection;
	g_return_if_fail(connection != NULL);

	orb = connection->orb_data;

	g_return_if_fail(orb != NULL);

	ORBit_info(ORBIT_LOG_DOMAIN_ORB,
		   "Received request %s, id %ul, on %s",
		   recv_buffer->message.u.request.operation,
		   recv_buffer->message.u.request.request_id,
		   recv_buffer->message.u.request.object_key._buffer);
 	/* XXX: printing the object_key above is likely to core dump! -- kennard */

	if(ORBIT_request_validator)
		mvr = ORBIT_request_validator(recv_buffer->message.u.request.request_id,
					      &recv_buffer->message.u.request.requesting_principal,
					      recv_buffer->message.u.request.operation);
	else
		mvr = ORBIT_MESSAGE_ALLOW;

	if(mvr == ORBIT_MESSAGE_ALLOW_ALL)
		connection->is_auth = TRUE;

	if(mvr != ORBIT_MESSAGE_BAD) {
		/* Find the POA for this incoming request */
		poa = ORBit_ORB_find_POA_for_okey(orb,
		   &recv_buffer->message.u.request.object_key);
		if(poa) {
			ORBit_POA_handle_request(poa, recv_buffer);
			do_unuse = 0;	/* always consumed */
		} else
			g_warning("No POA found for operation %s [%d]",
				  recv_buffer->message.u.request.operation,
				  recv_buffer->message.u.request.request_id);
	} else {
		g_warning("Request %s, ID %d was rejected by the authentication mechanism!",
			  recv_buffer->message.u.request.operation,
			  recv_buffer->message.u.request.request_id);
	}

	if(do_unuse)
		giop_recv_buffer_unuse(recv_buffer);
}

static void
ORBit_handle_incoming_locate_request(GIOPRecvBuffer *recv_buffer)
{
	CORBA_ORB orb;
	PortableServer_POA poa;
	GIOPConnection *connection;
	GIOPSendBuffer *send_buffer;

	g_assert(recv_buffer!=NULL);

	connection = GIOP_MESSAGE_BUFFER(recv_buffer)->connection;
	g_return_if_fail(connection != NULL);

	orb = connection->orb_data;

	g_return_if_fail(orb != NULL);

	ORBit_info(ORBIT_LOG_DOMAIN_ORB,
		   "Received locate request id %d, on %s",
		   recv_buffer->message.u.locate_request.request_id,
		   recv_buffer->message.u.locate_request.object_key._buffer);
	/* Find the POA for this incoming request */
	poa = ORBit_ORB_find_POA_for_okey(orb, 
	  &recv_buffer->message.u.locate_request.object_key);
		
	if(poa) {
		/* Object found, reply with "Object Here" */
		send_buffer = giop_send_locate_reply_buffer_use(connection,
			 recv_buffer->message.u.locate_request.request_id,
			 GIOP_OBJECT_HERE);
		giop_send_buffer_write(send_buffer);
		giop_send_buffer_unuse(send_buffer);
	} else {
		/* Object not found, reply with "Unknown Object" */
		send_buffer = giop_send_locate_reply_buffer_use(connection,
			 recv_buffer->message.u.locate_request.request_id,
			 GIOP_UNKNOWN_OBJECT);
		giop_send_buffer_write(send_buffer);
		giop_send_buffer_unuse(send_buffer);
	}

	giop_recv_buffer_unuse(recv_buffer);
}

static void
ORBit_handle_incoming_message(GIOPRecvBuffer *recv_buffer)
{
	GIOPConnection *connection;

	g_assert(recv_buffer);

	connection = GIOP_MESSAGE_BUFFER(recv_buffer)->connection;
	g_return_if_fail(connection != NULL);

	switch(GIOP_MESSAGE_BUFFER(recv_buffer)->message_header.message_type) {
	case GIOP_REQUEST:
		ORBit_handle_incoming_request(recv_buffer);
		break;
	case GIOP_LOCATEREQUEST:
		ORBit_handle_incoming_locate_request(recv_buffer);
		break;
	case GIOP_CLOSECONNECTION:
		/* Lame hack - need to do this in a manner that isn't
                   IIOP-specific */
		giop_recv_buffer_unuse(recv_buffer);
		giop_main_handle_connection_exception(connection);
		break;
	case GIOP_REPLY:
		/* the above comment probably applies here also... */
		giop_received_list_push(recv_buffer);
		break;
	default:
		g_warning("discarding message type %d (id possibly %d)",
			  GIOP_MESSAGE_BUFFER(recv_buffer)->message_header.message_type,
			  GIOP_MESSAGE_BUFFER(recv_buffer)->message_header.message_type?recv_buffer->message.u.reply.request_id:recv_buffer->message.u.request.request_id);
		giop_recv_buffer_unuse(recv_buffer);
		break;
	}
}

void
ORBit_set_request_validation_handler(ORBit_request_validate validator)
{
	ORBIT_request_validator = validator;
}


/****************************************************************************
 *
 *			Server Forwarding
 *
 ****************************************************************************/

/**
    This sub-module deals with "server side forward" via some
    ORBit-specific interfaces. This is a little weird and complicated,
    just like everything else in CORBA.

    Forwarding mechanisms:
    LOCATE
	Client sends out LOCATE_REQUEST, and the server responds with
	LOCATE_RESPONSE. Client generally caches the response, and will
	send future requests to the cached forwarding location.
    EXCEPTION
        Client sends a normal REQUEST, and the server spontaneous
    	responds with an exception that indicates a forwarded location.
	Client then resends the REQUEST to this new location. Client
	may (or not) cache the new location for future requests.
    SERVER-ALIAS
	[ORBit-specific]. Client sends a normal REQUEST, and if
	the object_key is an alias, re-routes the message to
	its "true" object key, and dispatches message to local POA.
	Client is unaware of alias/re-route.
**/

void
ORBit_ORB_forw_bind(CORBA_ORB orb, CORBA_sequence_octet *okey,
  CORBA_Object oref, CORBA_Environment *ev) {
    ORBit_SrvForwBind	*bd;
    ORBit_POAObject	*pobj;
    if ( orb->srv_forw_bindings ==0 ) {
    	orb->srv_forw_bindings = g_hash_table_new( 
	  (GHashFunc)ORBit_sequence_octet_hash, 
	  (GCompareFunc)ORBit_sequence_octet_equal);
    }
    bd = g_new0(ORBit_SrvForwBind,1);
    ORBit_sequence_octet_copy(&bd->objkey, okey);
    bd->objref = ORBit_RootObject_dup(oref);
    g_assert( oref->bypass_obj );
    if ( (pobj=oref->bypass_obj) != 0 ) {
    	ORBit_POA_oid_to_okey(pobj->poa, pobj->object_id, &bd->to_okey);
    }
    g_hash_table_insert(orb->srv_forw_bindings, &bd->objkey, bd);
}

static gboolean
ORBit_ORB_forw_unbind_cb(gpointer key, gpointer value, gpointer userdata) {
    ORBit_SrvForwBind	*bd = value;
    ORBit_sequence_octet_fini(&bd->objkey);
    ORBit_RootObject_release(bd->objref);
    ORBit_sequence_octet_fini(&bd->to_okey);
    g_free(bd);
    return TRUE;	/* release hash entry */
}


void
ORBit_ORB_forw_unbind_all(CORBA_ORB orb) {
    if ( orb->srv_forw_bindings ) {
	g_hash_table_foreach_remove(orb->srv_forw_bindings, 
	  ORBit_ORB_forw_unbind_cb, /*user_data*/0);
	g_hash_table_destroy(orb->srv_forw_bindings); 
	orb->srv_forw_bindings = 0;
    }
}
