/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* By Elliot Lee. Copyright (c) 1998 Red Hat Software */

#ifndef ALLOCATORS_H
#define ALLOCATORS_H 1

#include <orb/orbit.h>

#include <orb/allocator-defs.h>

#define ORBIT_CHUNK_ALLOC(typename) \
ORBit_chunk_alloc(typename##_allocator, &typename##_allocator_lock)

#define ORBIT_CHUNK_FREE(typename, mem) \
ORBit_chunk_free(typename##_allocator, &typename##_allocator_lock, (mem))

void ORBit_chunks_init(void);
void ORBit_chunks_fini(void);

gpointer ORBit_chunk_alloc(GMemChunk *chunk,
			   GStaticMutex* mutex);

void ORBit_chunk_free(GMemChunk *chunk,
		      GStaticMutex* mutex,
		      gpointer mem);

/* General memory allocation routines */

#define PTR_TO_MEMINFO(x) (((ORBit_mem_info *)(x)) - 1)
#define MEMINFO_TO_PTR(x) ((gpointer)((x) + 1))

/**
    This function-type is used internally by the memory allocator
    as a callback.  When called, it must "free" anything contained
    in {mem}.  Normally, it does not free {mem} itself, only what is
    inside {mem}.  For example, if {mem} contains only integers, then
    nothing happens. Alternatively, if {mem} contains object references,
    then CORBA_Object_free (or ORBit_RootObject_release()) must be
    invoked on it.

    The callback func must return the "end" of {mem}. This is used
    when iterating through sequences, arrays and structs.

    Previously, older code supported the idea that the callback could
    return FALSE (or NULL?), in which case that meant that the callback
    itself had already free'd the memory. This convention is no longer
    used.

    Below, some magic values of the fnc ptr are defined.
**/
typedef gpointer (ORBit_free_kidvals)(gpointer mem, gpointer func_data);
typedef void	 (ORBit_free_blk)(gpointer mem, gpointer prefix);

#ifdef ORBIT_DEBUG
#define ORBIT_MAGIC_MEMPREFIX		0x1234fedc
#define	ORBIT_MEM_MAGICDEF(name)	gulong name 
#define ORBIT_MEM_MAGICSET(name)	(name) = ORBIT_MAGIC_MEMPREFIX
#else
#define ORBIT_MEM_MAGICDEF(name)
#define ORBIT_MEM_MAGICSET(name)
#endif

typedef gulong ORBit_MemHow;

#define ORBIT_MEMHOW_PRELEN_MASK	(0xF<<28)
#define ORBIT_MEMHOW_CODE_MASK		(0xF<<24)
#define ORBIT_MEMHOW_NUMELS_MASK	((1<<24)-1)

#define ORBIT_MEMHOW_NONE	(1<<24)
#define ORBIT_MEMHOW_SIMPLE	(2<<24)

#define ORBIT_MEMHOW_TYPECODE	(3<<24)
typedef struct ORBit_MemPrefix_TypeCode_type {
    ORBIT_MEM_MAGICDEF(magic);
    CORBA_TypeCode	tc;
} ORBit_MemPrefix_TypeCode;

#define ORBIT_MEMHOW_KIDFNC1	(4<<24)
typedef struct ORBit_MemPrefix_KidFnc1_type {
    ORBIT_MEM_MAGICDEF(magic);
    ORBit_free_kidvals*	freekids;
} ORBit_MemPrefix_KidFnc1;

#define ORBIT_MEMHOW_FREEFNC1	(5<<24)
typedef struct ORBit_MemPrefix_FreeFnc1_type {
    ORBIT_MEM_MAGICDEF(magic);
    ORBit_free_blk*	freeblk;
} ORBit_MemPrefix_FreeFnc1;


extern gpointer ORBit_alloc_core(	
			size_t block_size,
			ORBit_MemHow how,
			size_t prefix_size,
			gpointer *prefixref,
			guint8 align);

extern void ORBit_free(gpointer mem);


extern gpointer ORBit_alloc_simple(size_t block_size);
extern gpointer ORBit_alloc_kidfnc(size_t element_size, guint num_elements,
			ORBit_free_kidvals *free_fnc);

#define ORBit_alloc(sz, len, fnc) ORBit_alloc_kidfnc( (sz), (len), (fnc))

#endif /* ALLOCATORS_H */
