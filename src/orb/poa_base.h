
typedef struct PortableServer_POA_type *PortableServer_POA;
typedef void *PortableServer_Servant;

typedef struct PortableServer_ServantBase__epv {
	void *_private;
	void (*finalize)(PortableServer_Servant, CORBA_Environment *);
	PortableServer_POA (*default_POA)(PortableServer_Servant, CORBA_Environment *);
} PortableServer_ServantBase__epv;
