/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  ORBit: A CORBA v2.2 ORB
 *
 *  Copyright (C) 1998 Richard H. Porter and Red Hat Software
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Dick Porter <dick@acm.org>
 *          Elliot Lee <sopwith@redhat.com>
 *
 */

#define o_return_val_if_fail(expr, val) if(!(expr)) { CORBA_exception_set_system(ev, ex_CORBA_BAD_PARAM, CORBA_COMPLETED_NO); return (val); }
#define o_return_if_fail(expr) if(!(expr)) { CORBA_exception_set_system(ev, ex_CORBA_BAD_PARAM, CORBA_COMPLETED_NO); return; }

#include "config.h"

#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <pwd.h>
#include <time.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <utime.h>
#include <popt.h>

#include "../IIOP/iiop-endianP.h"
#include "orbit.h"

#include "orbit_poa.h"
#include "orbit_object.h"
#include "orbit_object_type.h"
#include "genrand.h"

#ifndef SUN_LEN
/* This system is not POSIX.1g.  */
#define SUN_LEN(ptr) ((size_t) (((struct sockaddr_un *) 0)->sun_path)  \
                      + strlen ((ptr)->sun_path))
#endif

static GList *opt_initrefs=NULL;
static char *opt_definitref=NULL;

/*************************************************************************
 *
 *		ORBit_OrbGroup
 *
 *************************************************************************/

struct ORBit_OrbGroup_type {
    struct ORBit_RootObject_struct	parent;
    ORBit_genrand	genrand;
    gboolean		initialized;
};

static void
ORBit_OrbGroup_free_fn(gpointer obj_in)
{
    ORBit_OrbGroup *grp = obj_in;
    ORBit_chunks_fini();
    ORBit_genrand_fini(&grp->genrand);
    ORBit_classinfo_fini();
    grp->initialized = 0;
}

static const ORBit_RootObject_Interface ORBit_OrbGroup_epv =
{
	ORBIT_ROT_ORBGROUP,
	ORBit_OrbGroup_free_fn
};

static ORBit_OrbGroup*
ORBit_OrbGroup_dup(void)
{
    static ORBit_OrbGroup 	the_group;
    ORBit_OrbGroup		*grp = &the_group;
    if ( !grp->initialized ) {
	ORBit_RootObject_init(&grp->parent, &ORBit_OrbGroup_epv);
	ORBit_genrand_init(&grp->genrand);
	ORBit_chunks_init();
    	grp->initialized = 1;
    }
    return ORBit_RootObject_dup(grp);
}


/*************************************************************************
 *
 *		CORBA_ORB Initial References (Iref)
 *
 *************************************************************************/

/**
    This first set of funcs. deal with intial references.
    CORBA specifies that irefs are taged by ORB_ObjectId, which
    is a string. It is useful to note that this is completely
    different thing than a POA_ObjectId, which is a sequence<octet>.

    The factory function follows CORBA calling conventions in that
    the return value must be dup'd by the factory, and its arguments
    are roughly treated as "in".
**/

typedef struct ORBit_Iref ORBit_Iref;
typedef CORBA_Object (ORBit_Iref_FactoryFnc)(
					CORBA_ORB orb, 
					const ORBit_Iref *iref,
  					CORBA_Environment *ev);
#define ORBit_IrefF_NoUrl	(1<<0)
#define ORBit_IrefF_NoDef	(1<<1)
struct ORBit_Iref {
    int				flags;
    gchar*			name;	/* ORB_ObjectId, balloc'd */
    gchar*			url;	/* alloc'd */
    CORBA_Object		objref;
    ORBit_Iref_FactoryFnc*	factoryFnc;
};

static ORBit_Iref*
ORBit_ORB_iref_set(CORBA_ORB orb, CORBA_ORB_ObjectId name, CORBA_Object obj,
  CORBA_Environment *ev) {
    ORBit_Iref		*iref;
    if ( orb->irefs==0 || (iref=g_hash_table_lookup(orb->irefs, name))==0 )
    	return 0;
    if ( iref->objref ) {
    	CORBA_Object_release(iref->objref, ev); iref->objref = 0;
    }
    iref->objref = CORBA_Object_duplicate(obj, ev);
    return iref;
}

static void
ORBit_ORB_iref_define(CORBA_ORB orb, CORBA_ORB_ObjectId name, int flags,
  const char *opt_ior, /* const char *repoId, */
  ORBit_Iref_FactoryFnc* factoryFnc, 
  CORBA_Environment *ev) {
    ORBit_Iref		*iref;
    GList		*ir_item;
    int			namelen = strlen(name);

    if ( orb->irefs == 0 ) {
    	orb->irefs = g_hash_table_new( g_str_hash, g_str_equal);
    }
    if ( name==0 || name[0]==0 || g_hash_table_lookup(orb->irefs, name)!=0 ) {
	CORBA_exception_set(ev,CORBA_USER_EXCEPTION,
			    ex_CORBA_ORB_InvalidName,
			    NULL);
    	return;
    }

    iref = g_new0(ORBit_Iref,1);
    iref->name = g_strdup(name);
    iref->factoryFnc = factoryFnc;
    g_hash_table_insert(orb->irefs, iref->name, iref);
    if ( opt_ior && opt_ior[0] ) {
	/* The (old backward compat.) command line option 
	 * takes precidence over -ORBInitRef */
    	iref->url = g_strdup(opt_ior);
    	return;
    }
    if ( (flags & ORBit_IrefF_NoUrl)==0 ) {
	for ( ir_item=opt_initrefs; ir_item; ir_item=ir_item->next) {
	    gchar *def = (gchar*)(ir_item->data);
	    if ( strncmp(def,name,namelen)==0 && def[namelen]=='=' ) {
		iref->url = strcpy( def, def+namelen+1); /* shift it down */
		opt_initrefs = g_list_remove_link(opt_initrefs, ir_item);
		return;
	    }
	}
	if ( (flags & ORBit_IrefF_NoDef)==0
	  && opt_definitref && opt_definitref[0] ) {
	    iref->url = g_strdup_printf("%s/%s", opt_definitref, iref->name);
	    return;
	}
    }
}

static gboolean
ORBit_ORB_iref_undefine_cb(gpointer key, gpointer value, gpointer userdata) {
    ORBit_Iref		*iref = value;
    ORBit_RootObject_release(iref->objref);
    g_free(iref->name);
    g_free(iref->url);
    g_free(iref);
    return TRUE;	/* remove this item */
}

static void
ORBit_ORB_iref_undefine_all(CORBA_ORB orb) {
    g_hash_table_foreach_remove(orb->irefs, ORBit_ORB_iref_undefine_cb, 
      /*user_data*/0);
    g_hash_table_destroy(orb->irefs); orb->irefs = 0;
}

static ORBit_Iref*
ORBit_ORB_iref_lookup(CORBA_ORB orb, const char* name,
  CORBA_Environment *ev) {
    ORBit_Iref		*iref;
    if ( orb->irefs==0 
      || name==0 || name[0]==0
      || (iref = g_hash_table_lookup(orb->irefs, name)) == 0 )
    	return 0;
    if ( iref->objref == 0 && iref->url ) {
	iref->objref = CORBA_ORB_string_to_object(orb, iref->url, ev);
        return iref;
    }
    if ( iref->objref == 0 && iref->factoryFnc ) {
	iref->objref = (*(iref->factoryFnc))(orb, iref, ev);
    }
    return iref;
}

static CORBA_Object
ORBit_ORB_iref_RootPOA_factory(CORBA_ORB orb, const ORBit_Iref *iref, 
  CORBA_Environment *ev) {
    if ( orb->root_poa==0 ) {
	CORBA_PolicyList policies = {1L,0L,NULL,CORBA_FALSE};
	CORBA_Policy implicit = (CORBA_Policy)
	  PortableServer_POA_create_implicit_activation_policy(
	    (PortableServer_POA)0,PortableServer_IMPLICIT_ACTIVATION,ev);
	policies._buffer=&implicit;
	policies._length= 1L;
	/* Create the root poa (will auto-create poamgr) */
	orb->root_poa =
		ORBit_POA_new(orb, "RootPOA", /*poamgr*/NULL, &policies, ev);
	ORBit_RootObject_release(implicit);
    }
    return ORBit_RootObject_dup(orb->root_poa);
}

static CORBA_Object
ORBit_ORB_iref_POACurrent_factory(CORBA_ORB orb, const ORBit_Iref *iref,
  CORBA_Environment *ev) {
    return (CORBA_Object)ORBit_POACurrent_new(orb);
}


static void
ORBit_ORB_iref_list_cb(gpointer key, gpointer value, gpointer user_data) {
	ORBit_Iref		*iref = value;
	CORBA_ORB_ObjectIdList	*list = user_data;
	list->_buffer[(list->_length)++] = CORBA_string_dup(iref->name);
}

/* Section 4.5 */
CORBA_ORB_ObjectIdList *CORBA_ORB_list_initial_services(CORBA_ORB orb, CORBA_Environment *ev)
{
	CORBA_ORB_ObjectIdList *list;

	g_return_val_if_fail(ev, NULL);
	o_return_val_if_fail(orb, NULL);

	list = (CORBA_ORB_ObjectIdList *)CORBA_sequence_octet__alloc();
	if ( orb->irefs ) {
	    list->_maximum = g_hash_table_size(orb->irefs);
	    list->_buffer = CORBA_string_allocbuf(list->_maximum);
	    list->_length = 0;
	    list->_release = 1;
	    g_hash_table_foreach( orb->irefs, ORBit_ORB_iref_list_cb, list);
	}
	return list;
}

/* Section 4.5
 *
 * raises InvalidName
 */
CORBA_Object CORBA_ORB_resolve_initial_references(CORBA_ORB orb, CORBA_ORB_ObjectId identifier, CORBA_Environment *ev)
{
	ORBit_Iref	*iref;
	g_return_val_if_fail(ev, CORBA_OBJECT_NIL);
	o_return_val_if_fail(orb, CORBA_OBJECT_NIL);

	if ( (iref = ORBit_ORB_iref_lookup(orb, identifier, ev)) != 0 ) {
	    return CORBA_Object_duplicate(iref->objref, ev);
	}
	CORBA_exception_set(ev,CORBA_USER_EXCEPTION,
			    ex_CORBA_ORB_InvalidName,
			    NULL);
	return NULL;
}

/* This is a MICO extension
 *
 * raises InvalidName
 */
void CORBA_ORB_set_initial_reference(CORBA_ORB orb, 
  CORBA_ORB_ObjectId identifier, CORBA_Object obj, CORBA_Environment *ev)
{
	g_return_if_fail(ev);
	o_return_if_fail(orb && identifier && obj);
	
	if ( !ORBit_ORB_iref_set(orb, identifier, obj, ev) ) {
		CORBA_exception_set(ev,CORBA_USER_EXCEPTION,
			ex_CORBA_ORB_InvalidName,NULL);	
	}
}

#if 0
/**
    Defined in the interceptors spec under "Dynamic Initial Reference",
    chap. 11 of 99-12-02. Presumably this will eventually appear
    in the core ORB spec.
    Raises InvalidName if identifier is empty or it has already been
    registered.
**/
void CORBA_ORB_register_initial_reference(CORBA_ORB orb, 
  CORBA_ORB_ObjectId identifier, CORBA_Object obj, CORBA_Environment *ev)
{
	ORBit_ORB_iref_define(orb, identifier, /*flags*/0,
	  /*ior*/0, /*fnc*/0, ev);
	if ( ev->_major )
		return;
	ORBit_ORB_iref_set(orb, identifier, obj, ev);
}
#endif

/*************************************************************************
 *
 *		CORBA_ORB misc
 *
 *************************************************************************/

static int ORBit_ORBid_setup(CORBA_ORB orb, CORBA_ORBid id)
{
	g_assert(orb!=NULL);
	g_assert(id!=NULL);

	if(strcmp(id, "orbit-local-orb")) {
		fprintf(stderr, "ORBit_ORBid_setup: Unknown ORB id: %s\n", id);
		return(0);
	}

	orb->orb_identifier=g_strdup(id);

	return(1);
}

#define ORBIT_RELCONN(conn) { 					\
	if ( conn ) {						\
		if ( GIOP_CONNECTION(conn)->refcount != 1 ) {	\
			g_warning("ORB: connection " #conn 	\
			  " has %d remaining references.",	\
			  GIOP_CONNECTION(conn)->refcount-1);	\
		}						\
		giop_connection_unref(conn);			\
		conn = NULL;					\
	}							\
}

static void ORBit_ORB_release_conns(CORBA_ORB orb)
{
	giop_connection_remove_by_orb(orb);
	ORBIT_RELCONN(orb->cnx.ipv4);
	ORBIT_RELCONN(orb->cnx.ipv6);
	ORBIT_RELCONN(orb->cnx.usock);
	giop_connection_remove_by_orb(orb);
}

static void ORBit_ORB_free_fn(gpointer obj_in) 
{
	CORBA_ORB orb = obj_in;
	ORBit_ORB_release_conns(orb);

	g_assert( g_hash_table_size(orb->objrefs) == 0 );
	g_hash_table_destroy(orb->objrefs);
	g_ptr_array_free(orb->poas,1);
	if(orb->orb_identifier!=NULL) {
		g_free(orb->orb_identifier);
	}
	ORBit_RootObject_release(orb->group);
	g_free(orb);
}


static const ORBit_RootObject_Interface CORBA_ORB_epv =
{
	ORBIT_ROT_ORB,
	ORBit_ORB_free_fn
};

static GIOPConnection*
ORBit_ORB_make_usock_connection(void)
{
	GIOPConnection *retval = NULL;
	GString *tmpstr;
	struct stat statbuf;

	tmpstr = g_string_new(NULL);

	g_string_sprintf(tmpstr, "/tmp/orbit-%s", g_get_user_name());
		
	if(mkdir(tmpstr->str, 0700) != 0) {
		int e = errno;
			
		switch (e) {
		case 0:
		case EEXIST:
			if (stat(tmpstr->str, &statbuf) != 0)
				g_error ("Can not stat %s\n", tmpstr->str);

			if (statbuf.st_uid != getuid ())
				g_error ("Owner of %s is not the current user\n",
					 tmpstr->str);

			if((statbuf.st_mode & (S_IRWXG|S_IRWXO))
			   || !S_ISDIR(statbuf.st_mode))
				g_error ("Wrong permissions for %s\n",
					 tmpstr->str);
			break;
				
		default:
			g_error("Unknown error on directory creation of %s (%s)\n",
				tmpstr->str, g_strerror (e));
		}
	}

	{
		struct utimbuf utb;
		memset(&utb, 0, sizeof(utb));
		utime(tmpstr->str, &utb);
	}


#ifdef WE_DONT_CARE_ABOUT_STUPID_2DOT0DOTX_KERNELS
	g_string_sprintf(tmpstr, "/tmp/orbit-%s",
			 g_get_user_name());
	dirh = opendir(tmpstr->str);
	while(!retval && (dent = readdir(dirh))) {
		int usfd, ret;
		struct sockaddr_un saddr;

		saddr.sun_family = AF_UNIX;

		if(strncmp(dent->d_name, "orb-", 4))
			continue;

		g_snprintf(saddr.sun_path, sizeof(saddr.sun_path),
			   "/tmp/orbit-%s/%s",
			   g_get_user_name(), dent->d_name);

		usfd = socket(AF_UNIX, SOCK_STREAM, 0);
		g_assert(usfd >= 0);

		ret = connect(usfd, &saddr, SUN_LEN(&saddr));
		close(usfd);

		if(ret >= 0)
			continue;

		unlink(saddr.sun_path);
	}
	closedir(dirh);
#endif /* WE_DONT_CARE_ABOUT_STUPID_2DOT0DOTX_KERNELS */

	srand(time(NULL));
	while(!retval) {
		g_string_sprintf(tmpstr, "/tmp/orbit-%s/orb-%d%d",
				 g_get_user_name(), rand(), rand());
		retval =
			GIOP_CONNECTION(iiop_connection_server_unix(tmpstr->str));
	}

	g_string_free(tmpstr, TRUE);

	return retval;
}

static int use_ipv4=1;
static int use_ipv6=0;
static int use_usock=1;
static char *debug_level=NULL;
static char *debug_modules=NULL;
static char *imr_ior=NULL;
static char *ir_ior=NULL;
static char *naming_ior=NULL;
static char *orb_id_opt=NULL;
static int opt_ipv4_port=0;
static gboolean args_parsed=FALSE;
static int args_num_parsed = 0;

/* This is handled with a popt callback so it still happens when an app has
   included the popt arg table */

static void ORBit_rc_load_internal(const char *rcfile)
{
	FILE*fp;
	poptContext popt_con;
	int rc;
	
	fp=fopen(rcfile, "r");
	if(fp==NULL) {
		return;
	}
	
	while(!feof(fp)) {
		char buf[1024];
		const char **argv;
		int argc;
		
		if(fgets(buf, sizeof(buf), fp)!=NULL) {
			char *c;
			
			if((c=strchr(buf, '#'))!=NULL) {
				*c='\0';
			}
			
			if(buf[strlen(buf)-1]=='\n') {
				buf[strlen(buf)-1]='\0';
			}
			
			if((rc=poptParseArgvString(buf, &argc, &argv)) < -1) {
				g_print("libORBit: error loading %s: %s: %s\n",
					rcfile, poptStrerror(rc), buf);
				exit(0);
			}
			
			popt_con=poptGetContext("libORBit", argc, argv,
						ORBit_options,
						POPT_CONTEXT_KEEP_FIRST);
			if((rc=poptGetNextOpt(popt_con)) < -1) {
				g_print("libORBit: bad option %s in %s: %s\n",
					poptBadOption(popt_con,
						      POPT_BADOPTION_NOALIAS),
					rcfile,
					poptStrerror(rc));
				exit(0);
			}
			poptFreeContext(popt_con);
			free(argv);
		}
	}
	fclose(fp);
}

/* This is a popt "pre" callback, to set up options from the rc files before
   command line args are checked */
static void ORBit_rc_load(poptContext unused, enum poptCallbackReason reason,
			  const struct poptOption *opt, char *arg, void *data)
{
	static int already=0;
	char *ctmp, *buf;
	int old_args=args_num_parsed;
	
	if(already) {
		/* The RC parser uses popt on the same option table,
		   thereby leading to infinite recursion :-( */
		return;
	}
	already++;
	
	ORBit_rc_load_internal(ORBit_SYSRC);
	
	ctmp=g_get_home_dir();
	buf=alloca(strlen(ctmp)+sizeof("/.orbitrc"));
	sprintf(buf, "%s/.orbitrc", ctmp);
	
	ORBit_rc_load_internal(buf);

	/* Nasty kludge around the nasty kludge around the popt bug */
	args_num_parsed=old_args;
}

static void ORBit_args_parsed(poptContext con, enum poptCallbackReason reason,
			      const struct poptOption *opt, char *arg,
			      void *data)
{
	
	if ( reason==POPT_CALLBACK_REASON_POST ) {
	    args_parsed=TRUE;
	    return;
	}
	++args_num_parsed;
	if ( strcmp(opt->longName,"ORBInitRef")==0 ) {
	    opt_initrefs = g_list_append(opt_initrefs, g_strdup(arg));
	}
}


const struct poptOption ORBit_options[]={
	{NULL, '\0', POPT_ARG_CALLBACK|POPT_CBFLAG_PRE, (void *)ORBit_rc_load, 0, NULL, NULL},
	{NULL, '\0', POPT_ARG_CALLBACK|POPT_CBFLAG_POST, (void *)ORBit_args_parsed, 0, NULL, NULL},

	/* below here are CORBA-specified options */
	{"ORBid", '\0', POPT_ARG_STRING|POPT_ARGFLAG_ONEDASH|POPT_ARGFLAG_STRIP, &orb_id_opt, 0, "The ORB identifier", "ID"},
	{"ORBInitRef", '\0', POPT_ARG_STRING|POPT_ARGFLAG_ONEDASH|POPT_ARGFLAG_STRIP, 0, 0, "An ORB initial reference.", "ObjId=ObjURL"},
	{"ORBDefaultInitRef", '\0', POPT_ARG_STRING|POPT_ARGFLAG_ONEDASH|POPT_ARGFLAG_STRIP, &opt_definitref, 0, "The ORB default initial reference prefix.", "ObjURL prefix"},

	/* below here are ORBit-specific options */
	{"ORBImplRepoIOR", '\0', POPT_ARG_STRING|POPT_ARGFLAG_ONEDASH|POPT_ARGFLAG_STRIP, &imr_ior, 0, "The IOR of the Implementation Repository", "IOR string"},
	{"ORBIfaceRepoIOR", '\0', POPT_ARG_STRING|POPT_ARGFLAG_ONEDASH|POPT_ARGFLAG_STRIP, &ir_ior, 0, "The IOR of the Interface Repository", "IOR string"},
	{"ORBNamingIOR", '\0', POPT_ARG_STRING|POPT_ARGFLAG_ONEDASH|POPT_ARGFLAG_STRIP, &naming_ior, 0, "The IOR of the Name Server", "IOR string"},
	{"ORBDebugLevel", '\0', POPT_ARG_STRING|POPT_ARGFLAG_ONEDASH|POPT_ARGFLAG_STRIP, &debug_level, 0, "The amount of debugging information to display", "int"},
	{"ORBDebugModules", '\0', POPT_ARG_STRING|POPT_ARGFLAG_ONEDASH|POPT_ARGFLAG_STRIP, &debug_modules, 0, "The modules that display debugging information", "int"},
	{"ORBIIOPUsock", '\0', POPT_ARG_NONE|POPT_ARGFLAG_ONEDASH|POPT_ARGFLAG_STRIP, &use_usock, 0, "Use Unix socket", NULL},
	{"ORBIIOPIPv4", '\0', POPT_ARG_NONE|POPT_ARGFLAG_ONEDASH|POPT_ARGFLAG_STRIP, &use_ipv4, 0, "Use IPv4 server socket", NULL},
	{"ORBIPv4Port", '\0', POPT_ARG_INT|POPT_ARGFLAG_ONEDASH|POPT_ARGFLAG_STRIP, &opt_ipv4_port, 0, "IIOP IPv4 server port address", "int"},
	{"ORBIIOPIPv6", '\0', POPT_ARG_NONE|POPT_ARGFLAG_ONEDASH|POPT_ARGFLAG_STRIP, &use_ipv6, 0, "Use IPv6 socket", NULL},
	{NULL, '\0', 0, NULL, 0, NULL, NULL},
};

/* Section 4.4
 *
 * Adjusts argc and argv appropriately
 */
CORBA_ORB CORBA_ORB_init(int *argc, char **argv, CORBA_ORBid orb_identifier, CORBA_Environment *ev)
{
	static CORBA_ORB orb=NULL;

	g_return_val_if_fail(ev != NULL, NULL);
	o_return_val_if_fail(argc && argv && orb_identifier, NULL);

	if(orb) {
		return(orb);
	}
	
	if(args_parsed==FALSE) {
		poptContext popt_con;
		int rc;
		
		popt_con=poptGetContext("libORBit", *argc, (const char **)argv,
					ORBit_options, 0);
		if((rc=poptGetNextOpt(popt_con))<-1 && rc!=POPT_ERROR_BADOPT) {
			g_print("libORBit: bad argument %s: %s\n",
				poptBadOption(popt_con,POPT_BADOPTION_NOALIAS),
				poptStrerror(rc));
			exit(0);
		}

		/* reset argv with the stripped args
		 * checking args_num_parsed is a really really lame
		 * hack to work around a bug in popt! */
		if ( args_num_parsed ) {
		    *argc=poptStrippedArgv(popt_con, *argc, argv);
		}

		poptFreeContext(popt_con);
	} else {
		/* Technically we should strip ORBit args from argv
                   here too, but seeing as the app has already parsed
                   args with our popt struct we aren't going to
                   confuse it with funny options */
	}
	
	ORBit_log_domain_mask = 
		ORBit_parse_debug_domain_string (debug_modules);
	ORBit_log_level_mask = 
		ORBit_parse_debug_level_string (debug_level, TRUE);
	CORBA_exception_init(ev);

	giop_init(argv[0]);

	orb=g_new0(struct CORBA_ORB_type, 1);

	if(orb==NULL) {
		CORBA_exception_set_system(ev, ex_CORBA_NO_MEMORY, CORBA_COMPLETED_NO);
		goto error;
	}

	ORBit_RootObject_init(&orb->parent, &CORBA_ORB_epv);
	ORBit_RootObject_dup(orb);	/* released by ORB_destroy() */

	orb->group = ORBit_OrbGroup_dup();
	orb->genrand = &orb->group->genrand;

	if(orb_id_opt!=NULL) {
		if(!ORBit_ORBid_setup(orb, orb_id_opt))
			goto error;
		g_free(orb_id_opt);
	} else if(orb_identifier!=NULL) {
		if(!ORBit_ORBid_setup(orb, orb_identifier))
			goto error;
	} else {
		orb->orb_identifier=g_strdup("orbit-local-orb");
	}

	if(orb->orb_identifier==NULL) {
		CORBA_exception_set_system(ev, ex_CORBA_NO_MEMORY, CORBA_COMPLETED_NO);
		goto error;
	}

	if(use_ipv4 && opt_ipv4_port >= 0 ) {
		orb->cnx.ipv4_isPersistent = opt_ipv4_port > 0 ? 1 : 0;
		orb->cnx.ipv4 = GIOP_CONNECTION(
		  iiop_connection_server(opt_ipv4_port));
		giop_connection_ref(orb->cnx.ipv4);
		GIOP_CONNECTION(orb->cnx.ipv4)->orb_data = orb;
	}

	if(use_ipv6) {
		orb->cnx.ipv6 = GIOP_CONNECTION(iiop_connection_server_ipv6());
		giop_connection_ref(orb->cnx.ipv6);
		GIOP_CONNECTION(orb->cnx.ipv6)->orb_data = orb;
	}

	if(use_usock) {
		orb->cnx.usock = ORBit_ORB_make_usock_connection();
		giop_connection_ref(orb->cnx.usock);
		GIOP_CONNECTION(orb->cnx.usock)->orb_data = orb;
	}

	orb->objrefs = g_hash_table_new((GHashFunc)g_CORBA_Object_hash,
					(GCompareFunc)g_CORBA_Object_equal);
	orb->poas = g_ptr_array_new();

#if 1
	/* this is a common, but un-standardized interface.
	 * It is un-implemented by ORBit, but is here for the future.
	 */
	ORBit_ORB_iref_define(orb, "ImplementationRepository", /*flags*/0,
	  imr_ior, /* "IDL:omg.org/CORBA/ImplRepository:1.0", */
	  /*fnc*/0, ev);
#endif
	ORBit_ORB_iref_define(orb, "InterfaceRepository", /*flags*/0,
	  ir_ior, /* "IDL:omg.org/CORBA/Repository:1.0", */
	  /*fnc*/0, ev);
	ORBit_ORB_iref_define(orb, "NameService", /*flags*/0,
	  naming_ior, /* "IDL:omg.org/CosNaming/NamingContext:1.0", */
	  /*fnc*/0, ev);
	ORBit_ORB_iref_define(orb, "TradingService", /*flags*/0,
	  /*ior*/0, /*fnc*/0, ev);

	ORBit_ORB_iref_define(orb, "RootPOA", ORBit_IrefF_NoUrl,
	  /*ior*/0,
	  ORBit_ORB_iref_RootPOA_factory, ev);
	ORBit_ORB_iref_define(orb, "POACurrent", ORBit_IrefF_NoUrl,
	  /*ior*/0,
	  ORBit_ORB_iref_POACurrent_factory, ev);
	/* Someday implement: 
	 * SecurityCurrent, TransactionCurrent, DynAnyFactory
	 */

	if ( opt_initrefs ) {
	    /* if the list is non-empty at this point, it means
	     * that user passed in object we know nothing about.
	     */
	    g_print("libORBit: unknown or redundant InitRef: %s\n",
		    (char*)(opt_initrefs->data));
	    goto error;
	}

	ORBit_custom_run_setup(orb, ev);

	return ORBit_RootObject_dup(orb);

error:
	if(orb!=NULL) {
		CORBA_ORB_destroy(orb, /*ev*/NULL);
		orb=NULL;
	}
	g_free(imr_ior);
	g_free(ir_ior);
	g_free(naming_ior);
	g_free(orb_id_opt);

	return(NULL);
}

typedef struct {
	CORBA_Object obj;
	CDR_Codec *codec;
	gboolean emit_active;
} profile_user_data;

static void ORBit_emit_profile(gpointer item, gpointer userdata)
{
	ORBit_Object_info *profile=(ORBit_Object_info *)item;
	profile_user_data *data=(profile_user_data *)userdata;
	CORBA_Object obj=data->obj;
	CDR_Codec encaps_codec_d;
	CDR_Codec *codec=data->codec, *encaps = &encaps_codec_d;
	gboolean emit_active=data->emit_active;
	static const CORBA_octet iiopversion[] = {1,0};
	CORBA_octet codecbuf[2048];

	g_assert(obj!=NULL);
	g_assert(codec!=NULL);
	g_assert(profile!=NULL);

	if((profile == obj->active_profile) && (emit_active == FALSE))
		return;			/* we already did this one */

	switch(profile->profile_type) {
	case IOP_TAG_INTERNET_IOP:
		CDR_codec_init_static(encaps);
		encaps->buffer = codecbuf;
		encaps->release_buffer = CORBA_FALSE;
		encaps->buf_len = 2048;
		encaps->readonly = CORBA_FALSE;
		encaps->host_endian = encaps->data_endian = FLAG_ENDIANNESS;

		CDR_put_ulong(codec, IOP_TAG_INTERNET_IOP);
		CDR_put_octet(encaps, FLAG_ENDIANNESS);
		CDR_put_octets(encaps, (gpointer)iiopversion, sizeof(iiopversion));
		CDR_put_string(encaps, profile->tag.iopinfo.host);
		CDR_put_ushort(encaps, profile->tag.iopinfo.port);
		CDR_put_ulong(encaps, profile->object_key._length);
		CDR_put_octets(encaps, profile->object_key._buffer,
			       profile->object_key._length);
		CDR_put_ulong(codec, encaps->wptr);
		CDR_put_octets(codec, encaps->buffer, encaps->wptr);
		break;

	case IOP_TAG_ORBIT_SPECIFIC:
		CDR_codec_init_static(encaps);
		encaps->buffer = codecbuf;
		encaps->release_buffer = CORBA_FALSE;
		encaps->buf_len = 2048;
		encaps->readonly = CORBA_FALSE;
		encaps->host_endian = encaps->data_endian = FLAG_ENDIANNESS;

		CDR_put_ulong(codec, IOP_TAG_ORBIT_SPECIFIC);
		CDR_put_octet(encaps, FLAG_ENDIANNESS);
		CDR_put_octets(encaps, (gpointer)iiopversion, sizeof(iiopversion));
		CDR_put_string(encaps, profile->tag.orbitinfo.unix_sock_path);
		CDR_put_ushort(encaps, profile->tag.orbitinfo.ipv6_port);
		CDR_put_ulong(encaps, profile->object_key._length);
		CDR_put_octets(encaps, profile->object_key._buffer,
			profile->object_key._length);
		CDR_put_ulong(codec, encaps->wptr);
		CDR_put_octets(codec, encaps->buffer, encaps->wptr);
		break;

	default:
		g_warning("Skipping tag %d", profile->profile_type);
		break;
	}
}

CORBA_char *
CORBA_ORB_object_to_string(CORBA_ORB orb,
			   CORBA_Object obj,
			   CORBA_Environment *ev)
{
  int i;
  CDR_Codec codec_d;
  CDR_Codec *codec = &codec_d;
  CORBA_char *rc = NULL;
  CORBA_unsigned_long ntags;
  profile_user_data data;
  CORBA_octet codecbuf[2048];
  char *ctmp;

  g_return_val_if_fail(ev, NULL);
  o_return_val_if_fail(orb && obj, NULL);

  if(!obj || !orb || ORBIT_ROOT_OBJECT_gettype(obj)!=ORBIT_ROT_OBJREF ) {
	  CORBA_exception_set_system(ev,
				     ex_CORBA_BAD_PARAM,
				     CORBA_COMPLETED_NO);
	  return NULL;
  }

  CDR_codec_init_static(codec);

  codec->buffer = codecbuf;
  codec->release_buffer = CORBA_FALSE;
  codec->buf_len = 2048;
  codec->readonly = CORBA_FALSE;
  codec->host_endian = codec->data_endian = FLAG_ENDIANNESS;

  CDR_put_octet(codec, FLAG_ENDIANNESS);

  CDR_put_string(codec, obj->type_id);
  ntags = g_slist_length(obj->profile_list);
  CDR_put_ulong(codec, ntags);

  data.obj=obj;
  data.codec=codec;
  data.emit_active=TRUE;
  if(obj->active_profile != NULL)
	  ORBit_emit_profile(obj->active_profile, &data); /* do this one first */

  data.emit_active=FALSE;
  g_slist_foreach(obj->profile_list, ORBit_emit_profile, &data);

  rc = CORBA_string_alloc(4 + (codec->wptr * 2) + 1);
  strcpy(rc, "IOR:");

#define hexdigit(n) (((n)>9)?(n+'a'-10):(n+'0'))

  for(i = 0, ctmp = rc + strlen("IOR:"); i < codec->wptr; i++) {
	  *(ctmp++) = hexdigit((((codec->buffer[i]) & 0xF0) >> 4));
	  *(ctmp++) = hexdigit(((codec->buffer[i]) & 0xF));
  }
  *ctmp = '\0';
  
  {
	  /* Debug check */
	  CORBA_Object obj;
	  CORBA_Environment myev;

	  CORBA_exception_init(&myev);

	  obj = CORBA_ORB_string_to_object(orb, rc, &myev);

	  if (CORBA_Object_is_nil(obj, &myev)) {
		  g_warning("Bug in CORBA_ORB_object_to_string, created "
			    "bad IOR `%s'\n", rc);
		  CORBA_free(rc);
		  return NULL;
	  }
			  
	  CORBA_Object_release(obj, &myev);
  }
  
  return rc;
}

/* Quote from the GNU libc manual:

   "If you try to allocate more storage than the machine can provide,
    you don't get a clean error message. Instead you get a fatal
    signal like the one you would get from an infinite recursion;
    probably a segmentation violation (see section Program Error
    Signals)."

   The man page claims alloca() returns NULL on failure; this appears
   to be a load of shit on Linux where you just get flaming death, but
   we check anyway in case other systems work that way.

   On Linux we check that the size is less than MAX_STACK_ALLOC

   Note that the CORBA_alloc() calls in here can still cause
   program abort, and really that should be fixed in a similar
   way since our lengths are coming in from unpredictable sources
   like files or the network.
*/

#define MAX_STACK_ALLOC 8192

static CORBA_Object 
ORBit_sto_ior(CORBA_ORB orb, const CORBA_char *hexstr, CORBA_Environment *ev)
{
	GSList *profiles=NULL;
	CORBA_char *type_id;
	ORBit_Object_info *object_info;
	CDR_Codec codec_d, encaps_codec_d;
	CDR_Codec *codec = &codec_d, *encaps_codec = &encaps_codec_d;
	CORBA_octet endian;
	int i;
	CORBA_unsigned_long seq_len, misclen;
	int hexlen;

	CDR_codec_init_static(codec);
	hexlen = strlen(hexstr);
	codec->buf_len = hexlen/2;
	codec->buffer = alloca(codec->buf_len);
	codec->release_buffer = CORBA_FALSE;
	codec->readonly = TRUE;
	if ( !ORBit_hexbuf_to_buf(hexstr, hexlen, codec->buffer) )
		return 0;

	CDR_get_octet(codec, &endian);
	codec->data_endian = endian;
	codec->host_endian = FLAG_ENDIANNESS;

	CDR_get_string_static(codec, &type_id);

	CDR_get_seq_begin(codec, &seq_len);

	for(i = 0; i < seq_len; i++) {
		IOP_ProfileId tag;


		if (!CDR_get_ulong(codec, &tag))
			goto error_out;
		
		switch(tag) {
		case IOP_TAG_INTERNET_IOP:
			if (!CDR_get_ulong(codec, &misclen))
				goto error_out;
			
			CDR_codec_init_static(encaps_codec);

			if (misclen > MAX_STACK_ALLOC)
				goto error_out;
			
			encaps_codec->buffer = alloca(misclen);
			if (encaps_codec->buffer == NULL)
				/* misclen was probably junk */
				goto error_out;
			
			encaps_codec->release_buffer = FALSE;
			if(!CDR_buffer_gets(codec, encaps_codec->buffer, misclen))
				goto error_out;

			encaps_codec->buf_len = misclen;
			encaps_codec->readonly = CORBA_TRUE;
			if(!CDR_get_octet(encaps_codec, &endian))
				goto error_out;
			encaps_codec->data_endian = endian;
			encaps_codec->host_endian = FLAG_ENDIANNESS;

			if (encaps_codec->data_endian > 1)
				goto error_out;
			
			object_info=g_new0(ORBit_Object_info, 1);
			profiles=g_slist_append(profiles, object_info);
			object_info->profile_type = IOP_TAG_INTERNET_IOP;
			if(!CDR_get_octet(encaps_codec, &object_info->iiop_major))
				goto error_out;
			if(object_info->iiop_major != 1)
				goto error_out;
			if(!CDR_get_octet(encaps_codec, &object_info->iiop_minor))
				goto error_out;
			if(!CDR_get_string(encaps_codec, &object_info->tag.iopinfo.host))
				goto error_out;
			if(!CDR_get_ushort(encaps_codec, &object_info->tag.iopinfo.port))
				goto error_out;
			if(!CDR_get_seq_begin(encaps_codec, &object_info->object_key._length))
				goto error_out;

			object_info->object_key._maximum = 0;

			/* The POA gives out ORBit_alloc()d profiles, so we have to too */
			object_info->object_key._buffer = ORBit_alloc(object_info->object_key._length, 1, NULL);
			if(!CDR_buffer_gets(encaps_codec, object_info->object_key._buffer,
					    object_info->object_key._length))
				goto error_out;

			ORBit_set_object_key(object_info);
			break;

		case IOP_TAG_MULTIPLE_COMPONENTS:
			/* Just skip any multiple_components data, for now */
			if(!CDR_get_ulong(codec, &misclen))
				goto error_out;

			CDR_codec_init_static(encaps_codec);

			if (misclen > MAX_STACK_ALLOC)
				goto error_out;
			
			encaps_codec->buf_len = misclen;
			encaps_codec->buffer = alloca(misclen);
			if (encaps_codec->buffer == NULL)
				/* misclen was probably junk */
				goto error_out;

			encaps_codec->release_buffer = FALSE;
			encaps_codec->readonly = CORBA_TRUE;
			if(!CDR_buffer_gets(codec, encaps_codec->buffer, misclen))
				goto error_out;
			break;

		case IOP_TAG_ORBIT_SPECIFIC:
			if(!CDR_get_ulong(codec, &misclen))
				goto error_out;

			CDR_codec_init_static(encaps_codec);

			if (misclen > MAX_STACK_ALLOC)
				goto error_out;
			
			encaps_codec->buffer = alloca(misclen);
			if (encaps_codec->buffer == NULL)
				/* misclen was probably junk */
				goto error_out;
			
			encaps_codec->release_buffer = FALSE;
			if(!CDR_buffer_gets(codec, encaps_codec->buffer, misclen))
				goto error_out;

			encaps_codec->buf_len = misclen;
			encaps_codec->readonly = CORBA_TRUE;

			if(!CDR_get_octet(encaps_codec, &endian))
				goto error_out;

			encaps_codec->data_endian = endian;
			encaps_codec->host_endian = FLAG_ENDIANNESS;

			if (encaps_codec->data_endian > 1)
				goto error_out;
			
			object_info=g_new0(ORBit_Object_info, 1);
			profiles=g_slist_append(profiles, object_info);
			object_info->profile_type=IOP_TAG_ORBIT_SPECIFIC;
			if(!CDR_get_octet(encaps_codec, &object_info->iiop_major))
				goto error_out;

			if(object_info->iiop_major != 1)
				goto error_out;
			if(!CDR_get_octet(encaps_codec, &object_info->iiop_minor))
				goto error_out;

			if(!CDR_get_string(encaps_codec, &object_info->tag.orbitinfo.unix_sock_path))
				goto error_out;

			if(!CDR_get_ushort(encaps_codec, &object_info->tag.orbitinfo.ipv6_port))
				goto error_out;
			if(!CDR_get_seq_begin(encaps_codec, &object_info->object_key._length))
				goto error_out;
			object_info->object_key._maximum = 0;

			/* The POA gives out ORBit_alloc()d profiles, so we have to too */
			object_info->object_key._buffer = ORBit_alloc(object_info->object_key._length, 1, NULL);
			if(!CDR_buffer_gets(encaps_codec, object_info->object_key._buffer,
					    object_info->object_key._length))
				goto error_out;

			ORBit_set_object_key(object_info);
			break;
		default:
			g_warning("Unknown tag 0x%x", tag);

			/* Skip it */
			if(!CDR_get_ulong(codec, &misclen))
				goto error_out;

			CDR_codec_init_static(encaps_codec);

			if (misclen > MAX_STACK_ALLOC)
				goto error_out;
			
			encaps_codec->buf_len = misclen;
			encaps_codec->buffer = alloca(misclen);
			if (encaps_codec->buffer == NULL)
				/* misclen was probably junk */
				goto error_out;

			encaps_codec->release_buffer = FALSE;
			encaps_codec->readonly = CORBA_TRUE;
			if(!CDR_buffer_gets(codec, encaps_codec->buffer, misclen))
				goto error_out;

			break;
		}
	}

	return ORBit_create_object_with_info(profiles, type_id, orb, ev);

 error_out:
	ORBit_delete_profiles(profiles);
	return 0;
}


/**
    Parsing of corbaloc URL, sec 13.6.7.1 of V2.3, June 1999
    The parser is implemented as a hand-coded state machine lexer.
    Unfortunately, the grammer is complicated enough that a single state
    variable is not sufficient. Rather than use a full parser stack,
    I use hand-coded subparsers to handle specific sub components.
    This implementation is not a pure state machine; rather, it is
    intended to be a comprimize between speed, code-space, and future
    extensibility.

    corbaloc := "corbaloc:" <addrlist> [ / <key> ]
    <addrlist> := [ <addr> , ]* <addr>
    <addr> := "rir:" | ":iiop" <iiop_addr> | ":" <iiop_addr>
    iiop_addr := [ <major> . <minor> @ ] ip_host [ : <port> ]

    Problems:
    1)	The spec does not fully qualify what characters are legal in 
	which tokens. In particular it doesnt put any restrictions on
	what <future_prot_addr> may be.
    2)	ORBit wants a type_id associated with any created object ref.
	But a corbaloc doesnt provide any such; I dont know what
	value to use.
    3)  Is it ever legal to return a NULL object ref, and not throw
        an exception.  This is particularly likely with rir.
    4)  Based upon ptc/99-12-03, there is an conflict about
        how the ORB is suppose to implement corbaname. One place
	says to use NamingContext::resolve, and the other says
	NamingContextExt::resolve_str. I'm going with the later.
**/


typedef enum STO_Corbaloc_State {
    STO_St_AddrList,
    STO_St_AddrDone,
    STO_St_IiopAddr,	/* have not see version yet     */
    STO_St_IiopHost,	/* have already seen version    */
    STO_St_IiopPort	/* seen host, in middle of port */
} STO_Corbaloc_State;

/* 
    RFC2396 character set defs
    All characters are either (a) Reserved, (b) Unreserved, or (c) Escaped.
    Note that '\0' is regarded as an Escaped char, and strchr() returns
    non-NULL for c=='\0';
 */
#define STO_IsReserved(c)	( strchr(";/?:@&=+$,",(c))!=0 && (c)!=0 )
#define STO_IsMark(c)		( strchr("-_.!~*'()" ,(c))!=0 && (c)!=0 )
#define STO_IsUnreserved(c)	( isalnum(c) || STO_IsMark(c) )
#define STO_IsEscaped(c)	( !STO_IsUnreserved(c) && !STO_IsReserved(c) )


static int
ORBit_sto_rfc2396_token(gboolean reservedOk, const char *src, char **src_end,
  char *dst_in, int dst_len) {
    char *dst = dst_in;
    int	c;
    for ( ; ; ) {
	c = *src;
	if ( c=='%' ) {
	    if ( !isalnum(src[1]) || !isalnum(src[2]) )
		return -1;
	    *dst++ = HEXOCTET(src[1],src[2]);
	    src += 3;
	} else if ( STO_IsUnreserved(c) 
	  || (reservedOk && STO_IsReserved(c)) ) {
	    *dst++ = c;
	    src += 1;
	} else {
	    break;
	}
        g_assert( dst - dst_in < dst_len );
    }
    *dst = 0;	/* put null on for safety, but do not include in length. */
    *src_end = (char*)src;
    return dst - dst_in;
}

/**
    Quick a direct routine to parse the iiop_version; I assume
    these are single digits. This is very bad!
**/
static gboolean
ORBit_sto_iiop_version(const char *head, const char *tail, 
  ORBit_Object_info *profile) {
    if ( head + 3 != tail + 1
      || !isdigit(head[0]) || head[1]!='.' || !isdigit(head[2]) )
      	return FALSE;
    profile->iiop_major = head[0]-'0';
    profile->iiop_minor = head[2]-'0';
    return TRUE;
}

static CORBA_Object
ORBit_sto_corbaloc(CORBA_ORB orb, const char *str, char **eref, 
  CORBA_Environment *ev) {
    int			num_rir = 0, num_iiop = 0, c;
    STO_Corbaloc_State	state = STO_St_AddrList;
    const char 		*tok_head = str, *tok = str;
    GSList 		*profiles=NULL, *pfitem; /* of ORBit_Object_info* */
    ORBit_Object_info	*profile = NULL; /* Quiet gcc */
    char		*key_buf = 0;
    int			key_len = 0;

    for ( ; ; ) {
	c = *tok;
	if ( state==STO_St_AddrList ) {
	    if ( tok[0]==':' ) {
		tok += 1;
	    	state = STO_St_IiopAddr;
	    } else if ( strncmp(tok,"iiop:",5)==0 ) {
	    	tok += 5;
	    	state = STO_St_IiopAddr;
	    } else if ( strncmp(tok,"rir:",4)==0 ) {
		++num_rir;
		tok_head = tok = tok + 4;
	        state = STO_St_AddrDone;
	        continue;
	    } else {
		goto errout; /* unrecognized id token */
	    }
	    if ( state == STO_St_IiopAddr ) {
		++num_iiop;
		profile = g_new0(ORBit_Object_info, 1);
	    	profile->iiop_major = 1;
	    	profile->iiop_minor = 0;
		profile->profile_type = IOP_TAG_INTERNET_IOP;
	    	profile->tag.iopinfo.port = 2089;
		profiles = g_slist_append(profiles, profile);
		tok_head = tok;
	        continue;
	    }
	} else if ( state!=STO_St_AddrDone ) {
	    if ( isalnum(c) || c=='.' ) {
		tok += 1;
		continue;
	    }
	}

        if ( state==STO_St_IiopAddr && (c=='@') ) {
	    if ( !ORBit_sto_iiop_version(tok_head, tok-1, profile) )
		goto errout;
	    tok_head = tok = tok + 1;
	    state = STO_St_IiopHost;
	    continue;
	}
	if ( (state==STO_St_IiopAddr||state==STO_St_IiopHost)
	  && (c==':' || c==',' || c=='/' ||c=='#') ) {
	    int host_len = tok - tok_head;
	    if ( host_len < 1 )
	    	goto errout;
	    profile->tag.iopinfo.host = g_new(char, host_len+1);
	    memcpy(profile->tag.iopinfo.host, tok_head, host_len);
	    profile->tag.iopinfo.host[host_len] = 0;
	    if ( c==':' ) {
	        tok_head = tok = tok + 1;
	    	state = STO_St_IiopPort;
	        continue;
	    }
	    state = STO_St_AddrDone;
	    /* fallthrough */
	}
	if ( state==STO_St_IiopPort && (c==','||c=='/'||c=='#'||c==0) ) {
	    char *eptr;
	    unsigned long pt = strtoul(tok_head, &eptr, 10);
	    if ( eptr != tok || tok_head >= tok || pt==0 || pt >= 65536 )
	    	goto errout;
	    profile->tag.iopinfo.port = pt;
	    state = STO_St_AddrDone;
	    /* fallthrough */
	}
	if ( state==STO_St_AddrDone && (c==',') ) {
	    tok_head = tok = tok + 1;
	    state = STO_St_AddrList;
	    continue;
	}
        if ( state==STO_St_AddrDone && (c=='/') ) {
	    tok += 1;
	    key_len = strlen(tok);
	    if ( key_len >= MAX_STACK_ALLOC )
	    	goto errout;
	    key_buf = alloca(key_len + 1);
    	    if ( (key_len=ORBit_sto_rfc2396_token(/*resOk*/1, tok, 
	      			(char**)&tok, key_buf, key_len+1))< 0 )
	      	goto errout;
	      c = *tok;
	   /* fallthrough */
	}
	if ( state==STO_St_AddrDone ) {
	    if ( eref==0 ) {
		if ( c!=0 )
		    goto errout;
	    } else {
		*eref = (char*)tok;
	    }
	    if ( key_buf==0 || key_len==0 ) {
	    	key_buf = "NameService";	
		key_len = strlen(key_buf);
	    }
	    if ( num_rir>0 ) {
		ORBit_Iref	*iref;
	    	if ( num_iiop>0
    	          || (iref = ORBit_ORB_iref_lookup(orb, key_buf, ev)) == 0 )
		    goto errout;
		/* Nothing to free, I hope! */
    		return CORBA_Object_duplicate(iref->objref, ev);
	    }
	    if ( num_iiop>0 ) {
		if ( key_buf==0 || key_len==0 )
		    goto errout;
		for (pfitem=profiles; pfitem; pfitem=pfitem->next) {
		    profile = pfitem->data;
		    ORBit_sequence_octet_init(&profile->object_key, key_len);
		    memcpy( profile->object_key._buffer, key_buf, key_len);
		    ORBit_set_object_key(profile);
		}
		return ORBit_create_object_with_info(profiles, 
			/*XXX*/"Object", orb, ev);
	    }
	}
	goto errout;
    }
errout:
    ORBit_delete_profiles(profiles);
    return 0;
}

/**
    This invokes COSNaming_NamingContextExt_resolve_str(ns, name_out, ev)
    via DII.
**/
static CORBA_Object
ORBit_sto_resolve(CORBA_Object ns, CORBA_char *str, CORBA_Environment *ev) {
    CORBA_Request	req;
    CORBA_NamedValue	result;
    CORBA_Object	objref = 0;

    memset(&result,0,sizeof(result));
    result.argument._type = TC_CORBA_Object;
    result.argument._value = &objref;
    CORBA_Object_create_request(ns, /*context*/0, "resolve_str",
      /*arg_list*/0, &result, &req, /*flags*/0, ev);
    if ( ev->_major )
    	return 0;
    CORBA_Request_add_arg(req, /*arg_name*/0, TC_CORBA_string, &str,
      /*len*/strlen(str), CORBA_ARG_IN, ev);
    if ( ev->_major )
    	goto errout;
    CORBA_Request_invoke(req, /*flags*/0, ev);
errout:
    CORBA_Object_release( (CORBA_Object)req, ev);
    return objref;
}

static CORBA_Object
ORBit_sto_corbaname(CORBA_ORB orb, const char *str, CORBA_Environment *ev) {
    char *name_in, *name_in_end, *name_out;
    CORBA_Object ns, retval = 0;
    int	name_len;
    if ( (ns=ORBit_sto_corbaloc(orb, str, &name_in, ev))==0 )
  	return 0;
    if ( *name_in != '#' )
	goto errout;
    ++name_in;
    name_len = strlen(name_in);
    name_out = alloca( name_len+1);
    if ( ORBit_sto_rfc2396_token(/*resOk*/1, name_in, &name_in_end, 
      				name_out, name_len+1) < 0 
      || *name_in_end!=0 )
        goto errout;
#if 0
    retval = COSNaming_NamingContextExt_resolve_str(ns, name_out, ev);
#else
    retval = ORBit_sto_resolve(ns, name_out, ev);
#endif
errout:
    CORBA_Object_release(ns,ev);
    return retval;
}

/**
    This reads a URL from a file. This is a potential security problem.
    I cant think of an actual example attack, but my intuition tells me
    there is one. In order to limit resource usages, I do to things:
    1) Only the first line of the file is considered as an URL; the
       rest of the file is silently ignored.
    2) A particular efffect of the above is that a new-line following
       a URL within a file will be automatically stripped.
    3) The URL must be less than the arbitrary limit below.
**/
#define MAX_FILE_URL_SIZE	3000	/* arbitrary limit */
static CORBA_Object
ORBit_sto_file(CORBA_ORB orb, const char *str, CORBA_Environment *ev) {
    char	url[MAX_FILE_URL_SIZE];
    FILE	*fp;
    int		urllen;
    if ( (fp=fopen(str, "r"))==0 )
	return 0;
    if ( fgets( url, sizeof(url)-1, fp)==0 )
    	url[0] = 0;
    fclose(fp);
    urllen = strlen(url);
    /* make sure entire first line of file fit */
    if ( urllen==0 || urllen >= sizeof(url)-2 )
	return 0;
    if ( url[urllen-1] == '\n' )
        url[urllen-1] = 0;	/* kill new-line */
    return CORBA_ORB_string_to_object(orb, url, ev);
}

CORBA_Object 
CORBA_ORB_string_to_object(CORBA_ORB orb, const CORBA_char *str,
					CORBA_Environment *ev)
{
    CORBA_Object objref = NULL;

    g_return_val_if_fail(ev, CORBA_OBJECT_NIL);
    o_return_val_if_fail(orb && str, CORBA_OBJECT_NIL);

    /*IF*/ if ( strncmp(str, "IOR:", 4)==0 ) {
	objref = ORBit_sto_ior(orb, str+4, ev);
    } else if ( strncmp(str, "corbaloc:", 9)==0 ) {
	objref = ORBit_sto_corbaloc(orb, str+9, /*endptr*/0, ev);
    } else if ( strncmp(str, "corbaname:", 10)==0 ) {
	objref = ORBit_sto_corbaname(orb, str+10, ev);
    } else if ( strncmp(str, "file:", 5)==0 ) {
	objref = ORBit_sto_file(orb, str+5, ev);
    }
    if ( objref==0 ) {
	if ( ev->_major != CORBA_SYSTEM_EXCEPTION ) {
	    /* No User Exceptions are allowed. Some calls (e.g., to 
	     * NameService) will return some user exception.
	     */
	    CORBA_exception_set_system(ev, ex_CORBA_MARSHAL,
				       CORBA_COMPLETED_NO);
	    g_assert( ev->_major );
	}
    } else {
	g_assert( ev->_major == 0 );
    }
    return objref;
}

/* Section 4.1.2 */
CORBA_boolean CORBA_ORB_get_service_information(CORBA_ORB orb, 
  CORBA_ServiceType service_type, 
  CORBA_ServiceInformation **service_information, CORBA_Environment *ev)
{
	g_assert(!"Not yet implemented");
	return(CORBA_FALSE);
}

GStaticPrivate CORBA_ORB_thread_data = G_STATIC_PRIVATE_INIT;

/* I cant find this func in the spec! --kennard */
CORBA_Current *CORBA_ORB_get_current(CORBA_ORB orb, CORBA_Environment *ev)
{
	g_return_val_if_fail(ev, NULL);
	o_return_val_if_fail(orb, NULL);

	/* XXX check this over */
	return (CORBA_Current *)g_static_private_get (&CORBA_ORB_thread_data);
}


/* Section 4.9.1 */
CORBA_boolean CORBA_ORB_work_pending(CORBA_ORB orb, CORBA_Environment *ev)
{
	g_assert(!"Not yet implemented");
	return(CORBA_FALSE);
}

/* Section 4.9.2 */
void CORBA_ORB_perform_work(CORBA_ORB orb, CORBA_Environment *ev)
{
	g_assert(!"Not yet implemented");
	return;
}

/* Section 4.9.4 and V2.3 Sec 4.11.4
   We can get called many times and recursively, so we must zero out
   fields after we release them.  Raises BAD_INV_ORDER 
   when we are a recursive invokation and something touching
   the ORB is already active on the stack.
*/
void
CORBA_ORB_shutdown(CORBA_ORB orb,
		   CORBA_boolean wait_for_completion,
		   CORBA_Environment *ev)
{
	o_return_if_fail(orb);

	ORBit_ORB_forw_unbind_all(orb);
	if ( orb->root_poa ) {
		PortableServer_POA_destroy( orb->root_poa, 
		  /*etherealize*/1, wait_for_completion, ev);
		if (ev->_major) {
			/* This is prob. an INV_ORDER exception */
			return;
		}
		
	}
	ORBit_ORB_release_conns(orb);
	giop_main_quit();
}

#if 1
static void
ORBit_ORB_objref_dumper(gpointer key, gpointer value, gpointer user_data) {
    	CORBA_Object objref = value;
    	GString	*gs = user_data;
    	g_string_sprintfa(gs, "        OBJREF type %s refs %d %s.\n",
		objref->type_id, objref->parent.refs,
		objref->bypass_obj ? "bypass" : "");
}

static GString*
ORBit_ORB_dump(CORBA_ORB orb) {
	GString	*gs = g_string_new(0);
    	g_string_sprintfa(gs,"ORB DUMP BEGIN.\n");
    	g_string_sprintfa(gs,"    Object references:\n");
        g_hash_table_foreach(orb->objrefs, ORBit_ORB_objref_dumper, gs);
    	g_string_sprintfa(gs,"ORB DUMP END.\n");
	return gs;
}
#endif

/* V2.3 Sec 4.11.5 */
void
CORBA_ORB_destroy(CORBA_ORB orb, CORBA_Environment *ev)
{
	int	nrefs, totrefs;
	o_return_if_fail(orb);

	if ( orb->life_flags & ORBit_LifeF_Destroyed )
		return;
	CORBA_ORB_shutdown(orb, /*wait_for_completion*/1, ev);
	if ( ev->_major )
		return;
	ORBit_ORB_iref_undefine_all(orb);
	if ( orb->root_poa ) {
	    if ( orb->root_poa->parent.refs != 1 ) {
		g_warning("CORBA_ORB_destroy: Application still has %d refs to RootPOA.",
		  orb->root_poa->parent.refs-1);
	    }
	    ORBit_RootObject_release(orb->root_poa); orb->root_poa = 0;
	}

#ifndef G_DISABLE_ASSERT
	{
	    int pi;
	    g_assert( orb->root_poa == 0 );
	    for (pi=0; pi < orb->poas->len; pi++) {
	        PortableServer_POA *poa = g_ptr_array_index(orb->poas,pi);
		g_assert(poa==0);
	    }
	}
#endif
	nrefs = g_hash_table_size(orb->objrefs);
	totrefs = orb->parent.refs;
	/* our caller had better have one ref to the ORB still active ... */
	if ( totrefs > 2 ) {
		GString	*gs;
		g_warning("CORBA_ORB_destroy: %d objrefs remain,"
		" and %d other refs to ORB remain!",
		  nrefs, totrefs - nrefs - 2);
		gs = ORBit_ORB_dump(orb);
		g_warning("CORBA_ORB_destroy: dump: %s\n", gs->str);
		g_string_free(gs,1);
	}
	orb->life_flags |= ORBit_LifeF_Destroyed;
	ORBit_RootObject_release(orb);
}

/* Section 4.9.3 */
/* CORBA_ORB_run is in server.c */

/* Section 4.7 */
CORBA_PolicyType
CORBA_Policy__get_policy_type(CORBA_Policy obj, CORBA_Environment *ev)
{
	g_return_val_if_fail(ev, 0);
	o_return_val_if_fail(obj, 0);

	return obj->policy_type;
}

/* Section 4.7 */
CORBA_Policy CORBA_Policy_copy(CORBA_Policy obj, CORBA_Environment *ev)
{
	g_return_val_if_fail(ev, CORBA_OBJECT_NIL);
	o_return_val_if_fail(obj, CORBA_OBJECT_NIL);
	return ORBit_RootObject_dup(obj);
}

/* Section 4.7
 *
 * raises CORBA_NO_PERMISSION
 */
void CORBA_Policy_destroy(CORBA_Policy obj, CORBA_Environment *ev)
{
	o_return_if_fail(obj);
	ORBit_RootObject_release(obj);
}

/* Section 4.8.2 */
CORBA_Policy CORBA_DomainManager_get_domain_policy(CORBA_DomainManager obj, CORBA_PolicyType policy_type, CORBA_Environment *ev)
{
	g_return_val_if_fail(ev, CORBA_OBJECT_NIL);
	o_return_val_if_fail(obj, CORBA_OBJECT_NIL);

	g_assert(!"Not yet implemented");
	return(NULL);
}

/* Section 4.8.2 */
void CORBA_ConstructionPolicy_make_domain_manager(CORBA_ConstructionPolicy obj, CORBA_InterfaceDef object_type, CORBA_boolean constr_policy, CORBA_Environment *
ev)
{
	g_return_if_fail(ev);
	o_return_if_fail(obj && object_type);

	g_assert(!"Not yet implemented");
	return;
}

/* Section 4.2.8 */
CORBA_DomainManagersList *CORBA_Object_get_domain_managers(CORBA_Object obj, CORBA_Environment *ev)
{
	g_return_val_if_fail(ev, NULL);
	o_return_val_if_fail(obj, NULL);

	g_assert(!"Not yet implemented");
	return(NULL);
}
