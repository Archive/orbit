/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  ORBit: A CORBA v2.2 ORB
 *
 *  Copyright (C) 1998 Richard H. Porter and Red Hat Software
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Dick Porter <dick@acm.org>
 *
 */

#ifndef _ORBIT_TRACE_H_
#define _ORBIT_TRACE_H_

#include <stdarg.h>
#include "util.h"

typedef enum {
	ORBIT_LOG_DOMAIN_ORB  = 1 << 0,
	ORBIT_LOG_DOMAIN_CDR  = 1 << 1,
	ORBIT_LOG_DOMAIN_IIOP = 1 << 2,
	ORBIT_LOG_DOMAIN_TC   = 1 << 3,
	ORBIT_LOG_DOMAIN_IR   = 1 << 4
} ORBitLogDomain;

extern ORBitLogDomain ORBit_log_domain_mask;
extern GLogLevelFlags ORBit_log_level_mask;
	
const char *ORBit_log_domain_name (ORBitLogDomain domain);
ORBitLogDomain ORBit_parse_debug_domain_string (const gchar* string);
GLogLevelFlags ORBit_parse_debug_level_string (const gchar* string, 
					       gboolean and_above);
#ifdef __GNUC__
/* ORBit_error and ORBit_critical are output unconditionaly */
#  define ORBit_error(domain, format, args...)  			\
     g_log (ORBit_log_domain_name (domain), G_LOG_LEVEL_ERROR, format, ##args)
#  define ORBit_critical(domain, format, args...) 			\
     g_log (ORBit_log_domain_name (domain), G_LOG_LEVEL_CRITICAL,	\
	    format, ##args)
/* The rest are output only in case, they are wanted */
#  define ORBit_log(domain, level, format, args...) 			\
     if ((level & ORBit_log_level_mask) && 				\
	 (domain & ORBit_log_domain_mask)) 				\
	 g_log (ORBit_log_domain_name (domain), level, format, ##args)
#  define ORBit_warning(domain, format, args...)  			\
     ORBit_log (domain, G_LOG_LEVEL_WARNING, format, ##args)
#  define ORBit_message(domain, format, args...)    			\
     ORBit_log (domain, G_LOG_LEVEL_MESSAGE, format, ##args)
#  ifdef ORBIT_DEBUG
/* ORBit_info and ORBit_debug are only enabled, if is ORBIT_DEBUG is
 * defined. Here "file:line (function):" is printed as well. */
#    define ORBit_info(domain, format, args...)    			\
       ORBit_log (domain, G_LOG_LEVEL_INFO, "%s:%d (%s): " format,  	\
	          __FILE__, __LINE__, __PRETTY_FUNCTION__ , ## args)
#    define ORBit_debug(domain, format, args...)    			\
       ORBit_log (domain, G_LOG_LEVEL_DEBUG, "%s:%d (%s): " format,   	\
	          __FILE__, __LINE__, __PRETTY_FUNCTION__ , ## args)
#  else /* !ORBIT_DEBUG */
#    define ORBit_info(domain, format, args...)
#    define ORBit_debug(domain, format, args...)
#  endif /* ORBIT_DEBUG */
#else /* !__GNUC__ */
/* Now we use the functions from util.c instead of macros */
void ORBit_error(ORBitLogDomain domain, const gchar* format, ...);
void ORBit_crititical(ORBitLogDomain domain, const gchar* format, ...);
void ORBit_warning(ORBitLogDomain domain, const gchar* format, ...);
void ORBit_message(ORBitLogDomain domain, const gchar* format, ...);
#  ifdef ORBIT_DEBUG
void ORBit_info(ORBitLogDomain domain, const gchar* format, ...);
void ORBit_debug(ORBitLogDomain domain, const gchar* format, ...);
#  else /* !ORBIT_DEBUG */
/* No debug and info messages can be printed. This will actually be optimized
 * away completly by a decent compiler */
void ORBit_nodebug_for_stupid_compilers(ORBitLogDomain domain, ...);
#    define ORBit_debug if(0) ORBit_nodebug_for_stupid_compilers
#    define ORBit_info if(0) ORBit_nodebug_for_stupid_compilers
#  endif /* ORBIT_DEBUG */
#endif /* __GNUC__ */

#endif /* !_ORBIT_TRACE_H_ */
