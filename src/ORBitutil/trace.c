/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 *  ORBit: A CORBA v2.2 ORB
 *
 *  Copyright (C) 1998 Richard H. Porter
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Dick Porter <dick@acm.org>
 *
 */

#include <stdio.h>
#include <stdarg.h>

#include "trace.h"

static GDebugKey domain_keys[] = {
	{"ORB",           ORBIT_LOG_DOMAIN_ORB},
	{"CDR",           ORBIT_LOG_DOMAIN_CDR},
	{"IIOP",          ORBIT_LOG_DOMAIN_IIOP},
	{"TC",            ORBIT_LOG_DOMAIN_TC},
	{"IR",            ORBIT_LOG_DOMAIN_IR}
};

static GDebugKey level_keys[] = {
	{"warning",       G_LOG_LEVEL_WARNING},
	{"message",       G_LOG_LEVEL_MESSAGE},
	{"info",          G_LOG_LEVEL_INFO},
	{"debug",         G_LOG_LEVEL_DEBUG}
};

ORBitLogDomain ORBit_log_domain_mask;
GLogLevelFlags ORBit_log_level_mask;

const char *
ORBit_log_domain_name (ORBitLogDomain domain)
{
	gint i;
	for (i = 0; i < sizeof (domain_keys) / sizeof (domain_keys[0]); i++) {
		if (domain_keys[i].value == domain)
			return domain_keys[i].key;
	}
	return "ORBit-unknown";
}

ORBitLogDomain
ORBit_parse_debug_domain_string (const gchar* string)
{
	if (string)
		return g_parse_debug_string (string, domain_keys, 
					     sizeof (domain_keys) / 
					     sizeof (domain_keys[0]));
	else
		return 0;
}

GLogLevelFlags
ORBit_parse_debug_level_string (const gchar* string, gboolean and_above)
{
	guint level;
	
	if (!string)
		return 0;

	level = g_parse_debug_string (string, level_keys, 
				      sizeof (level_keys) / 
				      sizeof (level_keys[0]));
	if (and_above) {
		/* We now set all debug levels above the selected also */
		gint i = 31;
		gboolean set = FALSE;
		for (i = 31; i >= 0; i--) {
			if (level & (1<<i))
				set = TRUE;
			if (set)
				level |= 1<<i;
		}
		
	}
	
	return level;
}

#ifndef __GNUC__
static void
ORBit_logv(ORBitLogDomain domain, GLogLevelFlags level,
	   const gchar* format, va_list args)
{ 
	if ((level & ORBit_log_level_mask) && (domain & ORBit_log_domain_mask))
		g_logv (ORBit_log_domain_name (domain), level, format, args);
}

void
ORBit_error(ORBitLogDomain domain, const gchar* format, ...)
{ 
	va_list args;
	va_start (args, format);
	g_logv (ORBit_log_domain_name (domain), G_LOG_LEVEL_ERROR, 
		format, args);
	va_end (args);
}

void
ORBit_crititical(ORBitLogDomain domain, const gchar* format, ...)
{ 
	va_list args;
	va_start (args, format);
	g_logv (ORBit_log_domain_name (domain), G_LOG_LEVEL_CRITICAL, 
		format, args);
	va_end (args);
}

void
ORBit_warning(ORBitLogDomain domain, const gchar* format, ...)
{ 
	va_list args;
	va_start (args, format);
	ORBit_logv (domain, G_LOG_LEVEL_CRITICAL, format, args);
	va_end (args);
}

void
ORBit_message(ORBitLogDomain domain, const gchar* format, ...)
{ 
	va_list args;
	va_start (args, format);
	ORBit_logv (domain, G_LOG_LEVEL_MESSAGE, format, args);
	va_end (args);
}

#  ifdef ORBIT_DEBUG
void
ORBit_info(ORBitLogDomain domain, const gchar* format, ...)
{ 
	va_list args;
	va_start (args, format);
	ORBit_logv (domain, G_LOG_LEVEL_INFO, format, args);
	va_end (args);
}

void
ORBit_debug(ORBitLogDomain domain, const gchar* format, ...)
{ 
	va_list args;
	va_start (args, format);
	ORBit_logv (domain, G_LOG_LEVEL_DEBUG, format, args);
	va_end (args);
}
#  else /* !ORBIT_DEBUG */
void
ORBit_nodebug_for_stupid_compilers(ORBitLogDomain domain, ...)
{
	/* Do nothing */
}
#  endif /* ORBIT_DEBUG */
#endif/* __GNUC__ */





