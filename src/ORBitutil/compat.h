#ifndef ORBITUTIL_COMPAT_H
#define ORBITUTIL_COMPAT_H 1
#include <ORBitutil/orbit-os-config.h>
#include <sys/types.h>
#include <sys/uio.h>

#if ORBIT_HAVE_LIMITED_WRITEV
int ORBit_writev (int fd, const struct  iovec *  vector,  size_t count);
#else
#define ORBit_writev writev
#endif

#endif /* ORBITUTIL_COMPAT_H */
