#include "config.h"
#include "util.h"
#include <errno.h>

#if ORBIT_HAVE_LIMITED_WRITEV

/* This is included in case calculating max_iovs requires sysconf */
#include <unistd.h>

/* WARNING : On systems which already implement an unlimited writev,
 *  (such as linux) this function is not compiled and ORBit_writev is
 *  #defined to writev.
 *
 *  writev with an unlimited number of iovs.  On many systems, the
 *  number of iovs passed to writev is limited.  ORBit_writev passes the
 *  iovec through to writev in pieces small enough for writev to
 *  digest.  */
int ORBit_writev (int fd, const struct  iovec *  vector,  size_t count)
{
  static gint max_iovs = 0;
  gint res = 0;
  int retval = 0;
  
  if (!max_iovs)
    {
      G_LOCK_DEFINE_STATIC (max_iovs);
      G_LOCK (max_iovs);
      max_iovs = GET_MAX_IOVS;
      g_assert (max_iovs > 0);
      G_UNLOCK (max_iovs);
    }

  /* Repeat until we've written all the iovs */
  while (count > 0) 
    {
      /* figure out how many iovs to write at once */
      gint iovs = (count > max_iovs) ? max_iovs : count;
      res = writev (fd, vector, iovs);
      
      /* Stop if we hit an error. */
      if(res<0) break;
      
      /* Count down the iovs left to write and skip to the next set of iovs */
      vector += iovs; 
      count -= iovs;
      
      /* Keep track of the number of bytes written */
      retval+=res;
    }
  
  /* Set return value of error conditions */
  if (res < 0) 
    {
      /* Return -1 unless EAGAIN and we've already written data */
      if (errno != EAGAIN || retval == 0)
	retval = -1;
    }
  
  return retval;
}
#endif /* HAVE_LIMITED_WRITEV */

#ifndef HAVE_INET_ATON
#include <netinet/in.h>
#include <string.h>
int inet_aton(const char *cp, struct in_addr *inp)
{
	union {
		unsigned int n;
		char parts[4];
	} u;
	int a=0,b=0,c=0,d=0, i;

	i = sscanf(cp, "%d.%d.%d.%d%*s", &a, &b, &c, &d);

	if(i != 4)
		return 0;

	u.parts[0] = a;
	u.parts[1] = b;
	u.parts[2] = c;
	u.parts[3] = d;

	inp->s_addr = u.n;

	return 1;
}
#endif /* HAVE_INET_ATON */
