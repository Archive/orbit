/* daggone my original pseudo-code here got lost, stupid /lib/cpp */

CORBA_long Hello_hello(CORBA_Object _handle,
		       const char *astring,
		       CORBA_long along,
		       CORBA_Environment *ev)
{
	GIOPMessageBuffer *request_buffer;
	GIOPMessageBuffer *reply_buffer;
	CORBA_long out_retval;
	CORBA_long astring_strlen;
	CORBA_unsigned_long reqid;

	reqid=giop_get_request_id();
	request_buffer = giop_request_buffer_use(_handle->connection,
						 NULL, reqid,
						 TRUE, "Hello",
						 "hello", NULL);

	astring_strlen = strlen(astring);
	giop_request_buffer_append_mem_indirect(request_buffer,
						&astring_strlen,
						sizeof(astring_strlen));
	giop_request_buffer_append_mem(request_buffer,
				       astring, astring_strlen);
	giop_request_buffer_append_mem(request_buffer,
				       &along, sizeof(along));
	giop_request_buffer_write(request_buffer);
	reply_buffer = giop_reply_buffer_read(reqid);

	out_retval = giop_reply_buffer_get_long(reply_buffer);

	giop_reply_buffer_get_ev(reply_buffer, ev);

	giop_request_buffer_unuse(request_buffer);
	giop_reply_buffer_unuse(reply_buffer);

	return out_retval;
}

struct MyStruct {
	CORBA_long n1;
	CORBA_boolean flag1;
};

void Hello_complicated(CORBA_Object _handle,
		       char **inoutstring,
		       struct MyStruct *astruct,
		       CORBA_Environment *ev)
{
	GIOPRequestBuffer *request_buffer;
	GIOPReplyBuffer *reply_buffer;

	request_buffer = object_buffer_get(_handle, GIOP_REQUEST);

	rb_append_string(request_buffer, *inoutstring);
	rb_append_long(request_buffer, astruct->n1);
	rb_append_boolean(request_buffer, astring->flag1);

	reply_buffer = object_request_send(_handle, request_buffer);

	rb_get_string(reply_buffer, inoutstring);
	rb_get_ev(reply_buffer, ev);

	object_buffer_discard(request_buffer);
	object_buffer_discard(reply_buffer);		
}
