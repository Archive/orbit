/* Pseudo-code i.e. "anatomy of receiving a call to a CORBA object
   and passing it on to the server"... */

/* mainly pseudo-code for ideas, aka "anatomy of a call to a CORBA object"
   These functions are called by the ORB.
 */

void skel_Hello_hello(CORBA_Object _handle,
		      GIOPRequestBuffer *request_buffer,
		      GIOPReplyBuffer *reply_buffer,
		      CORBA_Environment *ev)
{
	char *in_string1;
	long in_long1;
	long out_retval;

	rb_get_string(request_buffer, &in_string1);
	in_long1 = rb_get_long(request_buffer);

	out_retval = OBJECT_HELLO(_handle)->hello(_handle, (const char *)in_string1, in_long1, ev);

	rb_append_long(reply_buffer, out_retval);

	/* All garbage collection of allocated memory (i.e. for
	   in_string1) is handled by the ORB automatically. */
}

struct MyStruct {
	CORBA_long n1;
	CORBA_boolean flag1;
};

void skel_Hello_complicated(CORBA_Object _handle,
			    GIOPRequestBuffer *request_buffer,
			    GIOPReplyBuffer *reply_buffer,
			    CORBA_Environment *ev)
{
	char *inout_stringval1;
	struct MyStruct astruct;

	rb_get_string(request_buffer, &inout_stringval1);

	astruct.n1 = rb_get_long(request_buffer);
	astruct.flag1 = rb_get_boolean(request_buffer);

	OBJECT_HELLO(_handle)->complicated(_handle, &inout_stringval1,
					   &astruct, ev);

	rb_append_string(reply_buffer, inout_stringval1);
}
