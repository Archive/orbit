/**************************************************************************

    main.c (Driver program for the IDL parser & backend)

    Copyright (C) 1998 Elliot Lee

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/

#include "config.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <glib.h>
#include <libIDL/IDL.h>
#include <popt.h>

#include "orbit-idl.h"

/************ Function Prototypes *********/
static int orbit_idl_to_backend(const char *filename, const char *cpp_args);

/************ Multi-function (NOT global) variables ********/

/************ Global variables *********/
gboolean enable_typecodes = FALSE,
  enable_inherited_ops = TRUE,
  enable_skeleton_impl = FALSE,
  enable_dcall = FALSE;
gboolean disable_stubs = FALSE,
  disable_skels = FALSE,
  disable_common = FALSE,
  disable_headers = FALSE;
int debuglevel = IDL_WARNING1;
GString *cpp_args;

#ifdef HAVE_INDENT_KR
#warning "Please e-mail orbit-list@cuc.edu if you get HAVE_INDENT_KR working"
#  define INDENT "indent -npro -kr -i8 -bad -fca -sc -sob"
#else
#  ifdef HAVE_INDENT
#    define INDENT "indent -npro -bad -bap -bc -sob -br -ce -cli2 -npcs -di1 -psl -i3 -lp -st"
#  else
#    define INDENT "cat"
#  endif
#endif

char *c_output_formatter = INDENT;

void libIDL_version_callback(poptContext con, enum poptCallbackReason reason,
				const struct poptOption *opt, char *arg,
				void *data)
{
  g_print("libIDL %s ", IDL_get_libver_string());
  g_print("(CORBA %s)\n", IDL_get_IDLver_string());

  exit(0);
}

void cpp_define_callback(poptContext con, enum poptCallbackReason reason,
				const struct poptOption *opt, char *arg,
				void *data)
{
  g_assert(opt!=NULL);

  if(opt->shortName=='D') {
    g_string_append(cpp_args, "-D");
  } else if(opt->shortName=='I') {
    g_string_append(cpp_args, "-I");
  }
  g_string_append(cpp_args, arg);
  g_string_append_c(cpp_args, ' ');
}

void enable_opt_callback(poptContext con, enum poptCallbackReason reason,
				const struct poptOption *opt, char *arg,
				void *data)
{
  const char *cmp;

  g_assert(opt!=NULL);

  /* arg is NULL if we get the longName version */
  if(arg==NULL) {
    cmp=opt->longName;
  } else {
    cmp=arg;
  }

  if(!strcmp(cmp, "typecodes"))
    enable_typecodes ^= TRUE;
  else if(!strcmp(cmp, "inherited_ops"))
    enable_inherited_ops ^= TRUE;
  else if(!strcmp(cmp, "skeleton_impl"))
    enable_skeleton_impl ^= TRUE;
  else if(!strcmp(cmp, "dcall"))
    enable_dcall ^= TRUE;
}

static const
struct poptOption libIDL_callback_options[] = {
  {NULL, '\0', POPT_ARG_CALLBACK, (void *)libIDL_version_callback, 0, NULL, NULL},
  {"libIDL-version", '\0', POPT_ARG_NONE, NULL, 0, "Show version of libIDL used.", NULL},
  {NULL, '\0', 0, NULL, 0, NULL, NULL}
};

static const
struct poptOption cpp_callback_options[] = {
  {NULL, '\0', POPT_ARG_CALLBACK, (void *)cpp_define_callback, 0, NULL, NULL},
  {"define", 'D', POPT_ARGFLAG_ONEDASH | POPT_ARG_STRING, NULL, 0, "Define value in preprocessor", NULL},
  {"include", 'I', POPT_ARGFLAG_ONEDASH | POPT_ARG_STRING, NULL, 0, "Add search path for include files", NULL},
  {NULL, '\0', 0, NULL, 0, NULL, NULL}
};

static const
struct poptOption enable_callback_options[] = {
  {NULL, '\0', POPT_ARG_CALLBACK, (void *)enable_opt_callback, 0, NULL, NULL},
  {"enable", 'E', POPT_ARGFLAG_ONEDASH | POPT_ARG_STRING, NULL, 0, "Enable feature (one of: skeleton_impl, inherited_ops, typecodes, dcall)", NULL},
  {"skeleton_impl", '\0', POPT_ARG_NONE, NULL, 0, "Output skeleton implementation", NULL},
  {"inherited_ops", '\0', POPT_ARG_NONE, NULL, 0, NULL, NULL},
  {"typecodes", '\0', POPT_ARG_NONE, NULL, 0, "Disable TypeCode generation", NULL},
  {"dcall", '\0', POPT_ARG_NONE, NULL, 0, NULL, NULL},
  {NULL, '\0', 0, NULL, 0, NULL, NULL}
};

static const
struct poptOption options[] = {
  {NULL, '\0', POPT_ARG_INCLUDE_TABLE, &enable_callback_options, 0, NULL, NULL},
  {NULL, '\0', POPT_ARG_INCLUDE_TABLE, &cpp_callback_options, 0, NULL, NULL},
  {NULL, '\0', POPT_ARG_INCLUDE_TABLE, &libIDL_callback_options, 0, NULL, NULL},
  {"debug", 'd', POPT_ARG_INT, &debuglevel, 0, "Debug level 0 to 4, default is 2", NULL},
  {"nostubs", '\0', POPT_ARG_NONE, &disable_stubs, 0, "Don't output stubs", NULL},
  {"noskels", '\0', POPT_ARG_NONE, &disable_skels, 0, "Don't output skels", NULL},
  {"nocommon", '\0', POPT_ARG_NONE, &disable_common, 0, "Don't output common", NULL},
  {"noheaders", '\0', POPT_ARG_NONE, &disable_headers, 0, "Don't output headers", NULL},
  {"c-output-formatter", '\0', POPT_ARG_STRING, &c_output_formatter, 0, "Program to use to format output (normally, indent)", "PROGRAM"},
  POPT_AUTOHELP
  {NULL, '\0', 0, NULL, 0, NULL, NULL}
};

/*--------------------------------
  int main(int argc, char *argv[])

  Input: argument count, argument strings (including program name)
  Output: error code

  Description:
      Alters state based on options (argument strings that begin with a -)
      Iterates through non-option arguments.
	  Calls orbit_idl_to_c() on these non-option arguments
	  with the assumption that they are idl filenames.
  --------------------------------
*/
int main(int argc, char *argv[])
{
  poptContext pcon;
  int rc;
  char *arg;

  cpp_args = g_string_new("-D__ORBIT_IDL__ ");

  if(getenv("C_OUTPUT_FORMATTER"))
    c_output_formatter = getenv("C_OUTPUT_FORMATTER");

  pcon=poptGetContext("orbit-idl", argc, argv, options, 0);
  poptSetOtherOptionHelp(pcon, "<IDL files>");

  if(argc < 2) {
  	poptPrintUsage(pcon, stdout, 0);
	exit(0);
  }

  if((rc=poptGetNextOpt(pcon)) < -1) {
    g_print("orbit-idl: bad argument %s: %s\n", 
    		poptBadOption(pcon, POPT_BADOPTION_NOALIAS),
		poptStrerror(rc));
    exit(0);
  }

  while((arg=poptGetArg(pcon))!=NULL)
    if (!orbit_idl_to_backend(arg, cpp_args->str))
      return 1;

  return 0;
}

/* This flag is so we can emit as many errors as we can without stopping - ATV */
static gboolean compile_failed;

void
orbit_IDL_compile_failed(gboolean die_now)
{
  compile_failed = TRUE;
  if (die_now)
    exit (1);
}

gboolean
orbit_IDL_compile_has_failed(void)
{
  return compile_failed;
}

/*---------------
  orbit_idl_to_backend:
     Input: IDL filename, and any arguments to be passed to CPP
     Output: Number of IDL files (1 or 0) that were successfully processed
     Does: Reads in 'filename' & parses it into a tree, using libIDL.
	   Calls the backend producer.
 ---------------
*/
static int
orbit_idl_to_backend(const char *filename, const char *cpp_args)
{
  IDL_ns namespace;
  IDL_tree tree;
  int x;
  char *basename, *ctmp;

  /* XXX todo: allow user to say "orbit-idl t" and have to read
     t.idl
  */
  x = IDL_parse_filename(filename, cpp_args, NULL, &tree, &namespace,
			 IDLF_TYPECODES|IDLF_CODEFRAGS,
			 debuglevel);

  if(IDL_SUCCESS != x) {
    if (x == -1)
      g_warning("Parse of %s failed: %s", filename, g_strerror(errno));
    return 0;
  }

  basename = g_strdup(filename);
  ctmp = strrchr(basename, '.');
  if(ctmp) *ctmp = '\0';

  compile_failed = FALSE;

  /* For now we only support the C backend :) */
  orbit_IDL_tree_to_c(basename, namespace, tree);
  g_free(basename);

  return !compile_failed;
}
