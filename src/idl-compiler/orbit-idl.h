#ifndef ORBIT_IDL_H
#define ORBIT_IDL_H 1
#include <libIDL/IDL.h>

extern gboolean enable_typecodes, enable_inherited_ops, enable_skeleton_impl,
  enable_dcall;
extern gboolean disable_stubs, disable_skels, disable_common, disable_headers;
extern int debuglevel;
extern char *c_output_formatter;

void orbit_IDL_tree_to_c(const char *base_filename, IDL_ns namespace,
			 IDL_tree tree);
void orbit_IDL_compile_failed(gboolean die_now);
gboolean orbit_IDL_has_compile_failed(void);

#endif
