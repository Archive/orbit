#include "test-args-share.h"

int
main (int argc, char *argv[])
{
    CORBA_Environment 	env, *ev = &env;
    CORBA_ORB		orb;
    PortableServer_POA	poa = NULL /* Quiet gcc */;
    CORBA_Object	obj1 = NULL, obj2 = NULL /* Quiet gcc */;
    const char		*serverIorStr = 0;
    gboolean		doServer = 0, doClient = 0;
    gboolean		doDefSrv = 0;
    int			ai;

    CORBA_exception_init(ev);
    orb = CORBA_ORB_init(&argc, argv, "orbit-local-orb", ev);
    TA_ev_assert(ev);

    for ( ai=1; ai < argc && argv[ai][0]=='-'; ++ai) {
	/*IF*/ if ( ai<argc && strcmp(argv[ai], "-local")==0 ) {
	    doServer = doClient = 1;
	} else if ( ai<argc && strcmp(argv[ai], "-server")==0 ) {
	    doServer = 1;
	} else if ( ai<argc && strcmp(argv[ai], "-client")==0 ) {
	    doClient = 1;
	} else if ( ai<argc && strcmp(argv[ai], "-defsrv")==0 ) {
	    doDefSrv = 1;
	} else {
	    g_error("Bad option ``%s''.", argv[ai]);
	}
    }
    if ( doClient ) {
    	if ( ai<argc ) {
    	    serverIorStr = argv[ai];
        } else {
    	    g_error("Must specify server IOR in client mode.");
	}
        ++ai;
    }
    if ( ai!=argc ) {
        g_error("Extra command line argument: ``%s''.", argv[ai]);
    }


    if ( doServer ) {

	if ( doDefSrv ) {
    	    poa = ta_srv_start_ds_poa(orb, ev);
    	    obj1 = ta_srv_start_ds_ref(poa, /*idx*/5, ev);
    	    obj2 = ta_srv_start_ds_ref(poa, /*idx*/7, ev);
	} else {
    	    poa = ta_srv_start_poa(orb, ev);
    	    obj1 = ta_srv_start_object(poa, ev);
    	    obj2 = ta_srv_start_object(poa, ev);
	}
	{
	    CORBA_char*	iorstr;
	    FILE *fh;
            iorstr = CORBA_ORB_object_to_string(orb, obj1, ev);
    	    TA_ev_assert(ev);
    	    fh = fopen("test-args.ior","w");
    	    g_assert(fh!=0);
    	    fputs(iorstr,fh);
	    fclose(fh);
    	    CORBA_free(iorstr);
    	}
    }

    if ( doClient ) {
	CORBA_Object	ref;
	if ( doServer ) {
	    ref = CORBA_Object_duplicate(obj1, ev);
	} else {
	    ref = CORBA_ORB_string_to_object(orb, serverIorStr, ev);
    	    TA_ev_assert(ev);
	}
	if ( doDefSrv ) {
	    TA_I_defsrvCheck(ref, ev);
    	    TA_ev_assert(ev);
	    if ( obj2 ) {
	        TA_I_defsrvCheck(obj2, ev);
    	        TA_ev_assert(ev);
	    }
	} else {
	    ta_cli_echotest_all(ref, ev);
	}
        CORBA_Object_release(ref, ev);
    } else {
    	CORBA_ORB_run(orb, ev);
    	TA_ev_assert(ev);
    }

    if ( doServer ) {
	if ( doDefSrv ) {
    	    ta_srv_finish_ds_ref(poa, obj1, ev); obj1 = 0;
    	    ta_srv_finish_ds_ref(poa, obj2, ev); obj2 = 0;
    	    ta_srv_finish_ds_poa(poa, ev); poa = 0;
	} else {
    	    ta_srv_finish_object(poa, obj1, ev); obj1 = 0;
    	    ta_srv_finish_object(poa, obj2, ev); obj2 = 0;
    	    ta_srv_finish_poa(poa, ev); poa = 0;
	}
    }
    
    CORBA_ORB_destroy(orb, ev);
    TA_ev_assert(ev);
    CORBA_Object_release((CORBA_Object)orb, ev);
    TA_ev_assert(ev);
    g_blow_chunks();
    return 0;
}
