#include "test-args-share.h"

/**
	This module provides a servant of the test-args "I" interface.
	It is used by test-args-server.c and args-args-local.c
	It uses test-args-skels.c
**/

static CORBA_ORB the_orb;

typedef struct {
    POA_TA_I	servant;
} TA_impl;

static CORBA_long
On_defsrvCheck(PortableServer_Servant servant,
	      CORBA_Environment *ev) {
    PortableServer_POA myCurPoa;
    PortableServer_ObjectId *myCurOid;
    CORBA_char *myCurOidStr;
    PortableServer_Current poacur = (PortableServer_Current)
	CORBA_ORB_resolve_initial_references(the_orb, "POACurrent", ev);
    TA_ev_assert(ev);
    myCurPoa = PortableServer_Current_get_POA(poacur,ev);
    TA_ev_assert(ev);
    myCurOid = PortableServer_Current_get_object_id(poacur,ev);
    TA_ev_assert(ev);
    myCurOidStr = PortableServer_ObjectId_to_string(myCurOid, ev);
    g_message("[server: oid=%s] defsrvCheck", myCurOidStr);
    CORBA_free(myCurOid);
    CORBA_free(myCurOidStr);
    CORBA_Object_release((CORBA_Object)myCurPoa,ev);
    CORBA_Object_release((CORBA_Object)poacur,ev);
    return 0;
}


static CORBA_long
On_dolong(PortableServer_Servant servant,
	      const CORBA_long inp, CORBA_long *inoutp, CORBA_long *outp,
	      CORBA_Environment *ev) {
    *outp = inp;
    return inp;
}

static TA_I_SFixed
On_doSFixed(PortableServer_Servant servant,
	      const TA_I_SFixed *inp, TA_I_SFixed *inoutp, TA_I_SFixed *outp,
	      CORBA_Environment *ev) {
    *outp = *inp;
    return *inp;
}

static TA_I_AVar_slice*
On_doAVar(PortableServer_Servant servant,
	      const TA_I_AVar inp, TA_I_AVar inoutp, TA_I_AVar_slice **outp,
	      CORBA_Environment *ev) {
    *outp = ORBit_copy_value( inp, TC_TA_I_AVar);
    return ORBit_copy_value( inp, TC_TA_I_AVar);
}


static TA_I_SRecur2*
On_doSRecur2(PortableServer_Servant servant,
  const TA_I_SRecur2 *inp, TA_I_SRecur2 *inoutp, TA_I_SRecur2 **outp,
  CORBA_Environment *ev) {
    *outp = ORBit_copy_value( inp, TC_TA_I_SRecur2);
#if 0
    ++(inoutp->value2);
#endif
    return ORBit_copy_value( inp, TC_TA_I_SRecur2);
}

static void
On_finalize(PortableServer_Servant servant, CORBA_Environment *ev) {
    TA_impl *ta = servant;
    POA_TA_I__fini(servant, ev);
    g_free(ta);
}

PortableServer_POA
ta_srv_start_poa(CORBA_ORB orb, CORBA_Environment *ev) {
    PortableServer_POA		poa;
    PortableServer_POAManager	mgr;

    poa = (PortableServer_POA)CORBA_ORB_resolve_initial_references(orb, 
      "RootPOA", ev);
    TA_ev_assert(ev);
    mgr = PortableServer_POA__get_the_POAManager(poa, ev);
    TA_ev_assert(ev);
    PortableServer_POAManager_activate(mgr, ev);
    TA_ev_assert(ev);
    CORBA_Object_release((CORBA_Object)mgr, ev);
    TA_ev_assert(ev);
    return poa;
}

static TA_impl*
ta_srv_create_impl(CORBA_Environment *ev) {
    TA_impl 			*my_impl;
    static POA_TA_I__vepv	the_vepv;
    {
    	static PortableServer_ServantBase__epv	the_base_epv;
    	static POA_TA_I__epv			the_TA_I_epv;
	the_base_epv.finalize = On_finalize;
	the_vepv._base_epv = &the_base_epv;
	the_TA_I_epv.defsrvCheck = On_defsrvCheck;
	the_TA_I_epv.dolong = On_dolong;
	the_TA_I_epv.doAVar = On_doAVar;
	the_TA_I_epv.doSFixed = On_doSFixed;
	the_TA_I_epv.doSRecur2 = On_doSRecur2;
	the_vepv.TA_I_epv = &the_TA_I_epv;
    }
    my_impl = g_new0(TA_impl,1);
    my_impl->servant.vepv = &the_vepv;
    POA_TA_I__init(my_impl, ev);
    TA_ev_assert(ev);
    return my_impl;
}

CORBA_Object
ta_srv_start_object(PortableServer_POA poa, CORBA_Environment *ev) {
    TA_impl 			*my_impl;
    CORBA_Object		my_ref;
    PortableServer_ObjectId*	my_objid;

    my_impl = ta_srv_create_impl(ev);
    my_objid = PortableServer_POA_activate_object(poa, my_impl, ev);
    TA_ev_assert(ev);
    CORBA_free(my_objid);	/* we dont need this anymore */
    my_ref = PortableServer_POA_servant_to_reference(poa, my_impl, ev);
    TA_ev_assert(ev);

    return my_ref;
}


void
ta_srv_finish_object(PortableServer_POA poa, CORBA_Object obj, 
  CORBA_Environment *ev) {
    PortableServer_ObjectId     *oid;

    oid = PortableServer_POA_reference_to_id(poa, obj, ev);
    TA_ev_assert(ev);
    CORBA_Object_release(obj, ev);
    TA_ev_assert(ev);
    PortableServer_POA_deactivate_object(poa, oid, ev);
    TA_ev_assert(ev);
    CORBA_free(oid);
}

void
ta_srv_finish_poa(PortableServer_POA poa, CORBA_Environment *ev) {
    CORBA_Object_release((CORBA_Object)poa, ev);
    TA_ev_assert(ev);
}

/**************************************************************************
 *
 *			Default Servant poas/refs
 *
 **************************************************************************/

PortableServer_POA
ta_srv_start_ds_poa(CORBA_ORB orb, CORBA_Environment *ev)
{
    PortableServer_POAManager mgr;
    PortableServer_POA		root_poa, poa;
    CORBA_Policy		policybuf[10];
    CORBA_PolicyList 		policies;
    TA_impl			*my_impl;

    the_orb = orb;
    root_poa = (PortableServer_POA)
      CORBA_ORB_resolve_initial_references(orb, "RootPOA", ev);
    TA_ev_assert(ev); g_assert( root_poa);
    policies._maximum = 6;
    policies._length = 6;
    policies._buffer = policybuf;
    policies._buffer[0] = 
	(CORBA_Policy) PortableServer_POA_create_lifespan_policy(
            root_poa, 
            PortableServer_PERSISTENT, 
	    ev);
    policies._buffer[1] = 
	(CORBA_Policy) PortableServer_POA_create_id_assignment_policy(
            root_poa, 
	    PortableServer_USER_ID, 
	    ev);
    policies._buffer[2] = 
	(CORBA_Policy) PortableServer_POA_create_id_uniqueness_policy(
            root_poa,
	    PortableServer_MULTIPLE_ID,
	    ev);
    policies._buffer[3] = 
	(CORBA_Policy) PortableServer_POA_create_request_processing_policy(
            root_poa, 
	    PortableServer_USE_DEFAULT_SERVANT, 
	    ev);
    policies._buffer[4] = 
	(CORBA_Policy) PortableServer_POA_create_servant_retention_policy(
            root_poa,
	    PortableServer_NON_RETAIN,
	    ev);
    policies._buffer[5] = 
	(CORBA_Policy) ORBit_PortableServer_POA_create_okeyrand_policy(
            root_poa,
	    0, /* CORBA_short poa_rand_len */
	    0, /* CORBA_short obj_rand_len */
	    ev);
    g_assert(ev->_major == 0);
    mgr = PortableServer_POA__get_the_POAManager(root_poa, ev);
    poa = PortableServer_POA_create_POA(root_poa, "TheEchoPOA",
					    mgr, &policies, ev);
    TA_ev_assert(ev);
    PortableServer_POAManager_activate(mgr, ev);
    TA_ev_assert(ev);
    CORBA_Object_release((CORBA_Object)mgr, ev);
    TA_ev_assert(ev);
    CORBA_Object_release((CORBA_Object)root_poa, ev);
    TA_ev_assert(ev);

    my_impl = ta_srv_create_impl(ev);
    TA_ev_assert(ev);

    PortableServer_POA_set_servant(poa, my_impl, ev);
    TA_ev_assert(ev);
    return poa;
}

CORBA_Object
ta_srv_start_ds_ref(PortableServer_POA poa, int idx, CORBA_Environment *ev) {
    PortableServer_ObjectId* object_id;
    CORBA_Object obj;
    static char oid_str[200];

    sprintf(oid_str, "Echo-%d", idx);
    object_id = PortableServer_string_to_ObjectId(oid_str, ev);
    TA_ev_assert(ev); g_assert(object_id);
    g_printerr("new oid: `%s'\n", 
	       PortableServer_ObjectId_to_string(object_id, ev));
    TA_ev_assert(ev);
    obj = PortableServer_POA_create_reference_with_id(poa, 
						       object_id, 
						       "IDL:TA/I:1.0",
						       ev);
    TA_ev_assert(ev);
    CORBA_free(object_id);
    return obj;
}

void
ta_srv_finish_ds_poa(PortableServer_POA poa, CORBA_Environment *ev) {
    TA_impl *impl;

    impl = PortableServer_POA_get_servant(poa, ev);
    TA_ev_assert(ev); g_assert(impl);
    /* it would be really nice if the POA did the next block of code
     * for us automatically! 
     */
    {
	/* remove the impl from the POA -- kind of like deactivating */
	PortableServer_POA_set_servant(poa, NULL, ev);
	TA_ev_assert(ev);
	/* clean up memory for impl */
	On_finalize(impl, ev);	
	TA_ev_assert(ev);
    }
    CORBA_Object_release((CORBA_Object)poa, ev);
    TA_ev_assert(ev);
}

void
ta_srv_finish_ds_ref(PortableServer_POA poa, CORBA_Object obj, 
  CORBA_Environment *ev) {
    CORBA_Object_release(obj, ev);
    TA_ev_assert(ev);
}
