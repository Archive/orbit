#ifndef TORTURE_DATA_H
#define TORTURE_DATA_H 1

#define DCK(x, v, p) compare##x(v, p)
#define GEP(x, v, p) v = get##x(p)

typedef enum { PRET = 0, PIN, POUT, PIOIN, PIOOUT } ParamPos;

CORBA_char *getString(ParamPos pos);
void compareString(CORBA_char *val, ParamPos pos);

#endif
