#include <string.h>
#include "torture.h"
#include "torture-compare.h"

#define GD(x, n) do##x##_data[n]
static const char *ppos[] = { "Return", "In", "Out", "Inout<In>",
			      "Inout<Out>" };

static const char *doString_data[] = {"retparam", "inparam", "outparam",
				      "inoutparam_in", "inoutparam_out"};

CORBA_char *getString(ParamPos pos)
{
  return CORBA_string_dup(GD(String, pos));
}

void compareString(CORBA_char *val,
		   ParamPos pos)
{
  g_assert(strcmp(val, GD(String, pos)) == 0);
}

static const CORBA_octet doOctet_data[] = {'a','b','c','d','e'};

CORBA_octet
getOctet(ParamPos pos)
{
  return GD(Octet, pos);
}

void compareOctet(CORBA_octet val,
		  ParamPos pos)
{
  g_assert(val == GD(Octet, pos));
}

static const CORBA_short doShort_data[] = {1,101,201,301,401};

CORBA_short
getShort(ParamPos pos)
{
  return GD(Short, pos);
}

void compareShort(CORBA_short val,
		  ParamPos pos)
{
  g_assert(val == GD(Short, pos));
}

static const CORBA_long doLong_data[] = {501,401,301,201,101};

CORBA_long
getLong(ParamPos pos)
{
  return GD(Long, pos);
}

void compareLong(CORBA_long val,
		 ParamPos pos)
{
  g_assert(val == GD(Long, pos));
}

static const Torture_SFixed doSFixed_data[] = {
  {101, 'a'},
  {201, 'b'},
  {301, 'c'},
  {401, 'd'},
  {501, 'e'}
};

Torture_SFixed
getSFixed(ParamPos pos)
{
  return GD(SFixed, pos);
}

void
compareSFixed(Torture_SFixed *val, ParamPos pos)
{
  Torture_SFixed ref = GD(SFixed, pos);

  g_assert(val->member1 == ref.member1);
  g_assert(val->member2 == ref.member2);
}

static const Torture_SVar doSVar_data[] = {
  {"ret", 101},
  {"in", 201},
  {"out", 301},
  {"ioin", 401},
  {"ioout", 501}
};

Torture_SVar *
getSVar(ParamPos pos)
{
  Torture_SVar *retval;

  retval = Torture_SVar__alloc();
  retval->member1 = CORBA_string_dup(doSVar_data[pos].member1);
  retval->member2 = doSVar_data[pos].member2;

  return retval;
}

void
compareSVar(Torture_SVar *val,
	    ParamPos pos)
{
  Torture_SVar *ref;

  ref = &GD(SVar, pos);
  g_assert(strcmp(val->member1, ref->member1) == 0);
  g_assert(val->member2 == ref->member2);
}

Torture_UFixed
getUFixed(ParamPos pos)
{
  Torture_UFixed retval;

  switch(pos) {
  case PRET:
    retval._d = 'a';
    retval._u.x = 101;
    break;
  case PIN:
    retval._d = 'b';
    retval._u.y = 201;
    break;
  case POUT:
    retval._d = 'c';
    retval._u.z = 3.1415;
    break;
  case PIOIN:
    retval._d = 'a';
    retval._u.y = 301;
    break;
  case PIOOUT:
    retval._d = 'e';
    retval._u.z = 42.0;
    break;
  }

  return retval;
}

void
compareUFixed(Torture_UFixed *val,
	      ParamPos pos)
{
  Torture_UFixed ref;

  ref = getUFixed(pos);

}
