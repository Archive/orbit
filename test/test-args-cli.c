#include "test-args-share.h"

/**
	This module provides a client routines to exercise the TA_I interface.
	It is used by test-args-client.c and args-args-local.c
	It uses test-args-stubs.c
**/

static void
chk_long(const CORBA_long *a, const CORBA_long *b) {
    g_assert( *a == *b );
}


static void
cpy_AVar(TA_I_AVar dst, TA_I_AVar src) {
    int idx, jdx;
    for (idx=0; idx < 2; idx++) {
        for (jdx=0; jdx < 3; jdx++) {
    	    dst[idx][jdx] = CORBA_string_dup(src[idx][jdx]);
	}
    }
}

static void
chk_AVar(TA_I_AVar a, TA_I_AVar b) {
    int idx, jdx;
    for (idx=0; idx < 2; idx++) {
        for (jdx=0; jdx < 3; jdx++) {
    	    g_assert( strcmp(a[idx][jdx],b[idx][jdx])==0 );
	}
    }
}


static void
chk_SFixed(const TA_I_SFixed *a, const TA_I_SFixed *b) {
    g_assert( a->member1 == b->member1 );
    g_assert( a->member2 == b->member2 );
}

static void chk_SRecur2(const TA_I_SRecur2 *a, const TA_I_SRecur2 *b);

static void
chk_SRecur2Seq(const CORBA_sequence_TA_I_SRecur2 *a,
  const CORBA_sequence_TA_I_SRecur2 *b) {
    int	i;
    g_assert( a->_length == b->_length );
    for (i=0; i < a->_length; i++) {
	chk_SRecur2( &a->_buffer[i], &b->_buffer[i]);
    }
}

static void
chk_SRecur2(const TA_I_SRecur2 *a, const TA_I_SRecur2 *b) {
    g_assert( a->value2 == b->value2 );
    g_assert( a->data.value3 == b->data.value3 );
    chk_SRecur2Seq(&a->data.kids, &b->data.kids);
}

void
ta_cli_echotest_all(CORBA_Object obj, CORBA_Environment *ev) {
    {
        CORBA_long ref = 9886, ret, inp = ref, inoutp = ref, outp;
        ret = TA_I_dolong( obj, inp, &inoutp, &outp, ev);
	TA_ev_assert(ev);
	chk_long( &ref, &ret);
	chk_long( &ref, &inp);
	chk_long( &ref, &inoutp);
	chk_long( &ref, &outp);
    }
    {
	TA_I_SFixed ref = { 7000, 4 }, ret, inp = ref, inoutp = ref, outp;
        ret = TA_I_doSFixed( obj, &inp, &inoutp, &outp, ev);
	TA_ev_assert(ev);
	chk_SFixed( &ref, &ret);
	chk_SFixed( &ref, &inp);
	chk_SFixed( &ref, &inoutp);
	chk_SFixed( &ref, &outp);
    }

    {
	TA_I_AVar ref = { { "a", "b", "c"}, { "x", "y", "z" } };
	TA_I_AVar inp, inoutp;
	TA_I_AVar_slice *ret, *outp;
	cpy_AVar( inp, ref);
	cpy_AVar( inoutp, ref);
        ret = TA_I_doAVar( obj, (const TA_I_AVar_slice *)inp, inoutp, &outp, ev);
	TA_ev_assert(ev);
	chk_AVar( ref, ret);
	chk_AVar( ref, inp);
	chk_AVar( ref, inoutp);
	chk_AVar( ref, outp);
    }

    {
	TA_I_SRecur2	lev2[1] = {
		{ 1000, { 2000, { 0, 0, 0, 0} }}
	};
	TA_I_SRecur2	lev1[2] = {
		{ 1000, { 2000, { 1, 1, lev2, 0} }},
		{ 3000, { 4000, { 0, 0, 0, 0} }}
	};
    	TA_I_SRecur2	ref = { 8000, { 9000, { 2, 2, lev1, 0 } } };
    	TA_I_SRecur2	*ret, *inp = &ref, *inoutp, *outp;
	inoutp = ORBit_copy_value( &ref, TC_TA_I_SRecur2);
        ret = TA_I_doSRecur2( obj, inp, inoutp, &outp, ev);
	chk_SRecur2( &ref, ret);
	chk_SRecur2( &ref, inp);
	chk_SRecur2( &ref, inoutp);
	chk_SRecur2( &ref, outp);
    }
    g_message("test-args-client: echo test passed.");
}
