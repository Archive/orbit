#include <stdio.h>
#include <orb/orbit.h>

int main(int argc, char *argv[])
{
	CORBA_Environment ev;
	CORBA_ORB orb;
	CORBA_Repository ir;
	CORBA_PrimitiveDef prim_long;
	CORBA_ConstantDef constant;
	CORBA_StringDef str;
	CORBA_ModuleDef mod1, mod2;
	CORBA_ContainedSeq *contained_seq;
	CORBA_Contained_Description *contained_desc;
	CORBA_ConstantDescription *constant_desc;
	CORBA_Container_DescriptionSeq *description_seq;
	CORBA_any any;
	CORBA_long fooval=1;
	int i;

	/* Test popt */
	{
		poptContext popt_con;
		int rc;
		
		popt_con=poptGetContext("test-ird", argc, (const char **)argv, ORBit_options, 0);
		if((rc=poptGetNextOpt(popt_con)) < -1) {
			g_print("test-ird: bad argument %s: %s\n",
				poptBadOption(popt_con, POPT_BADOPTION_NOALIAS),
				poptStrerror(rc));
			exit(0);
		}
	}
	
	CORBA_exception_init(&ev);
	orb=CORBA_ORB_init(&argc, argv, "orbit-local-orb", &ev);

	ir=CORBA_ORB_resolve_initial_references(orb, "InterfaceRepository",
						&ev);
	if(CORBA_Object_is_nil(ir, &ev)) {
		g_print("No Interface Repo found.\n");
		exit(0);
	}

	any._type=(CORBA_TypeCode)TC_CORBA_long;
	any._value=&fooval;
	CORBA_any_set_release(&any, CORBA_FALSE);

	prim_long=CORBA_Repository_get_primitive(ir, CORBA_pk_long, &ev);
	
	constant=CORBA_Repository_create_constant(ir, "IDL:wibble:1.0", "wibble", "1.0", prim_long, &any, &ev);
	if(constant!=CORBA_OBJECT_NIL) {
		g_print("%s\n", CORBA_ConstantDef__get_id(constant, &ev));
		g_print("%s\n", CORBA_ConstantDef__get_absolute_name(constant, &ev));

#if 0
		/* currently having trouble decoding the typecodes in anys */
		contained_desc=CORBA_ConstantDef_describe(constant, &ev);
		if(contained_desc==NULL) {
			g_print("ConstantDef_describe returned NULL!\n");
		} else {
			constant_desc=contained_desc->value._value;
			
			g_print("Constant Description\n");
			g_print("--------------------\n");
			g_print("name: %s\n", constant_desc->name);
			g_print("id: %s\n", constant_desc->id);
			g_print("defined_in: %s\n", constant_desc->defined_in);
			g_print("version: %s\n", constant_desc->version);
		}
#endif
	}
	constant=CORBA_Repository_create_constant(ir, "IDL:wibble:1.1", "wibble", "1.1", prim_long, &any, &ev);
	constant=CORBA_Repository_create_constant(ir, "IDL:wobble:1.0", "wibble", "1.0", prim_long, &any, &ev);
	constant=CORBA_Repository_create_constant(ir, "IDL:wibble:1.0", "wobble", "1.0", prim_long, &any, &ev);

	str=CORBA_Repository_create_string(ir, 10, &ev);
	
	mod1=CORBA_Repository_create_module(ir, "IDL:module1:1.0", "module1",
					    "1.0", &ev);
	mod2=CORBA_Repository_create_module(ir, "IDL:module2:1.0", "module2",
					    "1.0", &ev);

	constant=CORBA_ModuleDef_create_constant(mod1, "IDL:mod1const:1.0", "modconst", "1.0", prim_long, &any, &ev);
	constant=CORBA_ModuleDef_create_constant(mod2, "IDL:mod2const:1.0", "modconst", "1.0", prim_long, &any, &ev);
	
	CORBA_ConstantDef_move(constant, mod1, "modconst", "1.0",
			       &ev);	/* should fail */
	CORBA_ConstantDef_move(constant, mod1, "modconst", "1.1", &ev);
	
	contained_seq=CORBA_Repository_contents(ir, CORBA_dk_all,
						CORBA_FALSE,
						&ev);
	
	g_print("%d items in repository\n", contained_seq->_length);
	for(i=0;i<contained_seq->_length;i++) {
		g_print("%d\n", CORBA_Contained__get_def_kind(contained_seq->_buffer[i], &ev));
		
		switch(CORBA_Contained__get_def_kind(contained_seq->_buffer[i], &ev)) {
		case CORBA_dk_Attribute:
			g_print("Item %d is attribute called %s, version %s, ID %s\n", i,
				CORBA_AttributeDef__get_absolute_name(contained_seq->_buffer[i], &ev),
				CORBA_AttributeDef__get_version(contained_seq->_buffer[i], &ev),
				CORBA_AttributeDef__get_id(contained_seq->_buffer[i], &ev));
			break;
		case CORBA_dk_Constant:
			g_print("Item %d is constant called %s, version %s, ID %s\n", i,
				CORBA_ConstantDef__get_absolute_name(contained_seq->_buffer[i], &ev),
				CORBA_ConstantDef__get_version(contained_seq->_buffer[i], &ev),
				CORBA_ConstantDef__get_id(contained_seq->_buffer[i], &ev));
			break;
		case CORBA_dk_Exception:
			g_print("Item %d is exception called %s, version %s, ID %s\n", i,
				CORBA_ExceptionDef__get_absolute_name(contained_seq->_buffer[i], &ev),
				CORBA_ExceptionDef__get_version(contained_seq->_buffer[i], &ev),
				CORBA_ExceptionDef__get_id(contained_seq->_buffer[i], &ev));
			break;
		case CORBA_dk_Interface:
			g_print("Item %d is interface called %s, version %s, ID %s\n", i,
				CORBA_InterfaceDef__get_absolute_name(contained_seq->_buffer[i], &ev),
				CORBA_InterfaceDef__get_version(contained_seq->_buffer[i], &ev),
				CORBA_InterfaceDef__get_id(contained_seq->_buffer[i], &ev));
			break;
		case CORBA_dk_Module:
			g_print("Item %d is module called %s, version %s, ID %s\n", i,
				CORBA_ModuleDef__get_absolute_name(contained_seq->_buffer[i], &ev),
				CORBA_ModuleDef__get_version(contained_seq->_buffer[i], &ev),
				CORBA_ModuleDef__get_id(contained_seq->_buffer[i], &ev));
			break;
		case CORBA_dk_Operation:
			g_print("Item %d is operation called %s, version %s, ID %s\n", i,
				CORBA_OperationDef__get_absolute_name(contained_seq->_buffer[i], &ev),
				CORBA_OperationDef__get_version(contained_seq->_buffer[i], &ev),
				CORBA_OperationDef__get_id(contained_seq->_buffer[i], &ev));
			break;
		case CORBA_dk_Typedef:
			g_print("Item %d is typedef called %s, version %s, ID %s\n", i,
				CORBA_TypedefDef__get_absolute_name(contained_seq->_buffer[i], &ev),
				CORBA_TypedefDef__get_version(contained_seq->_buffer[i], &ev),
				CORBA_TypedefDef__get_id(contained_seq->_buffer[i], &ev));
			break;
		case CORBA_dk_Alias:
			g_print("Item %d is alias called %s, version %s, ID %s\n", i,
				CORBA_AliasDef__get_absolute_name(contained_seq->_buffer[i], &ev),
				CORBA_AliasDef__get_version(contained_seq->_buffer[i], &ev),
				CORBA_AliasDef__get_id(contained_seq->_buffer[i], &ev));
			break;
		case CORBA_dk_Struct:
			g_print("Item %d is struct called %s, version %s, ID %s\n", i,
				CORBA_StructDef__get_absolute_name(contained_seq->_buffer[i], &ev),
				CORBA_StructDef__get_version(contained_seq->_buffer[i], &ev),
				CORBA_StructDef__get_id(contained_seq->_buffer[i], &ev));
			break;
		case CORBA_dk_Union:
			g_print("Item %d is union called %s, version %s, ID %s\n", i,
				CORBA_UnionDef__get_absolute_name(contained_seq->_buffer[i], &ev),
				CORBA_UnionDef__get_version(contained_seq->_buffer[i], &ev),
				CORBA_UnionDef__get_id(contained_seq->_buffer[i], &ev));
			break;
		case CORBA_dk_Enum:
			g_print("Item %d is enum called %s, version %s, ID %s\n", i,
				CORBA_EnumDef__get_absolute_name(contained_seq->_buffer[i], &ev),
				CORBA_EnumDef__get_version(contained_seq->_buffer[i], &ev),
				CORBA_EnumDef__get_id(contained_seq->_buffer[i], &ev));
			break;
		case CORBA_dk_Repository:
			g_print("Repository does not inherit from Contained!\n");
			break;
		case CORBA_dk_String:
			g_print("Bogus item %d is string of length %d\n", i, CORBA_StringDef__get_bound(contained_seq->_buffer[i], &ev));
			g_print("String does not inherit from Contained!\n");
			break;
		case CORBA_dk_Wstring:
			g_print("Wstring does not inherit from Contained!\n");
			break;
		case CORBA_dk_Primitive:
			g_print("Primitive does not inherit from Contained!\n");
			break;
		case CORBA_dk_Sequence:
			g_print("Sequence does not inherit from Contained!\n");
			break;
		case CORBA_dk_Array:
			g_print("Array does not inherit from Contained!\n");
			break;
		case CORBA_dk_Fixed:
			g_print("Fixed does not inherit from Contained!\n");
			break;
		default:
			g_print("Item %d is of unknown type\n", i);
			break;
		}
	}
	
	contained_seq=CORBA_Repository_lookup_name(ir, "wibble", 1,
						   CORBA_dk_all,
						   CORBA_FALSE,
						   &ev);
	g_print("%d wibbles directly in repository\n", contained_seq->_length);
	
	contained_seq=CORBA_Repository_lookup_name(ir, "wibble", -1,
						   CORBA_dk_all,
						   CORBA_FALSE,
						   &ev);
	g_print("%d wibbles overall in repository\n", contained_seq->_length);

#if 0
	/* This crashes the repo... */
	description_seq=CORBA_Repository_describe_contents(ir, CORBA_dk_all,
							   CORBA_FALSE, -1,
							   &ev);
	g_print("%d described items in repository\n", description_seq->_length);
#endif
	
	CORBA_Repository_lookup(ir, "::wibble", &ev);
	CORBA_Repository_lookup(ir, "wibble", &ev);
	CORBA_Repository_lookup(ir, "::foo::bar::baz", &ev);
	
	CORBA_Repository_destroy(ir, &ev);
	
	CORBA_Object_release(constant, &ev);
	CORBA_Object_release(str, &ev);
	CORBA_Object_release(ir, &ev);
	CORBA_Object_release((CORBA_Object) orb, &ev);

	return 0;
}
