#include <orb/orbit.h>
#include "test-args.h"

#define TA_ev_assert(ev) \
	g_assert( ev->_major == 0 )

#if 0
/*
 * from main program
 */
extern gboolean		echo_opt_quiet;
#endif

/*
 * from test-args-srv.c 
 */
extern PortableServer_POA
			ta_srv_start_poa(	CORBA_ORB orb, 
						CORBA_Environment *ev);
extern CORBA_Object	ta_srv_start_object(	PortableServer_POA poa, 
						CORBA_Environment *ev);
extern void		ta_srv_finish_object(	PortableServer_POA poa, 
						CORBA_Object obj,
						CORBA_Environment *ev);
extern void		ta_srv_finish_poa(	PortableServer_POA poa, 
						CORBA_Environment *ev);


extern PortableServer_POA
			ta_srv_start_ds_poa(	CORBA_ORB orb, 
						CORBA_Environment *ev);
extern CORBA_Object	ta_srv_start_ds_ref(	PortableServer_POA poa, 
						int idx,
						CORBA_Environment *ev);
extern void		ta_srv_finish_ds_ref(	PortableServer_POA poa, 
						CORBA_Object obj,
						CORBA_Environment *ev);
extern void		ta_srv_finish_ds_poa(	PortableServer_POA poa, 
						CORBA_Environment *ev);

/*
 * from test-args-cli.c
 */
extern void ta_cli_echotest_all(CORBA_Object obj, CORBA_Environment *ev);

