#!/bin/sh
# Run this to generate all the initial makefiles, etc.

echo "
-- This branch of ORBit is deprecated. Don't use it. --
"
echo "What you are most likely looking for is ORBit2, the ORBit that is
used with GNOME 2:
"
echo " $ cvs -z3 co ORBit2 "
echo "
If you really are looking for the latest stable version of ORBit 1
please use the orbit-stable-0-5 branch:
"
echo " $ cvs -z3 checkout -r orbit-stable-0-5 ORBit"
exit 1

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

ORIGDIR=`pwd`
cd $srcdir

DIE=0

# Check for autoconf, the required version is set in configure.in
(autoconf --version) < /dev/null > /dev/null 2>&1 || {
	echo
	echo "You must have at minimum autoconf version 2.12 installed"
	echo "to compile ORBit. Download the appropriate package for"
	echo "your distribution, or get the source tarball at"
	echo "ftp://ftp.gnu.org/pub/gnu/"
	DIE=1
}

# Check for libtool
(libtool --version | egrep "1.2") > /dev/null || 
(libtool --version | egrep "1.3") > /dev/null || {
	echo
	echo "You must have at minimum libtool version 1.2 installed"
	echo "to compile ORBit. Download the appropriate package for"
	echo "your distribution, or get the source tarball at"
	echo "ftp://alpha.gnu.org/gnu/libtool-1.2d.tar.gz"
	DIE=1
}

# Check for automake, the required version is set in Makefile.am
(automake --version) < /dev/null > /dev/null 2>&1 ||{
	echo
	echo "You must have at minimum automake version 1.4 installed"
	echo "to compile ORBit. Download the appropriate package for"
	echo "your distribution, or get the source tarball at"
	echo "ftp://ftp.cygnus.com/pub/home/tromey/automake-1.4.tar.gz"
	DIE=1
}


if test "$DIE" -eq 1; then
	exit 1
fi

(test -f src/orb/orbit_types.h) || {
	echo "You must run this script in the top-level ORBit directory"
	exit 1
}

if test -z "$*"; then
	echo "I am going to run ./configure with no arguments - if you wish "
        echo "to pass any to it, please specify them on the $0 command line."
fi

case $CC in
xlc )
  am_opt=--include-deps;;
esac

for i in . libIDL
do 
  echo processing $i
  (cd $i; \
    libtoolize --copy --force; \
    aclocal $ACLOCAL_FLAGS;
    if test "$i" != "libIDL"; then autoheader; fi; \
    automake --add-missing $am_opt; \
    if test "$i" != "libIDL"; then autoheader; fi; \
    autoconf)
done

cd $ORIGDIR

echo "Running $srcdir/configure --enable-maintainer-mode" "$@"
$srcdir/configure --enable-maintainer-mode "$@"

echo 
echo "Now type 'make' to compile ORBit."
