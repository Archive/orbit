/* random.c - connect to www.random.org's random number corba server object
 *            and retrieve a few numbers.
 * This demo retrieves true random numbers from a CORBA server on the internet,
 * so running the client requires a live internet connection.
 *
 * See http://www.random.org/
 * and http://www.random.org/corba.html
 *
 * The variable random_ior contains the current IOR of the server. If the client
 * fails to connect to the server, please check the above URL to see if the
 * reference has changed.
 */
#include <stdio.h>
#include <stdlib.h>
#include <orb/orbit.h>

#include "random.h"

Random random_client;

char* random_ior = "IOR:000000000000000f49444c3a52616e646f6d3a312e3000000000000100000000000000500001000000000016706c616e7874792e6473672e63732e7463642e69650006220000002c3a5c706c616e7874792e6473672e63732e7463642e69653a52616e646f6d3a303a3a49523a52616e646f6d00";

int
main (int argc, char *argv[])
{
    CORBA_Environment ev;
    CORBA_ORB orb;
    CORBA_long answer;
    int i;

    CORBA_exception_init(&ev);
    orb = CORBA_ORB_init(&argc, argv, "orbit-local-orb", &ev);

    /*if(argc < 2)
      {
	printf("Need a binding ID thing as argv[1]\n");
	return 1;
      }*/


    random_client = CORBA_ORB_string_to_object(orb,random_ior/* argv[1]*/, &ev);
    if (!random_client) {
	printf("Cannot bind to %s\n", random_ior);
	return 1;
    }
    printf("Here are some true random numbers:\n");
    for(i = 0;i<10;i++){
        printf("%d\n",Random_lrand48(random_client,&ev));
    }

    CORBA_Object_release(random_client, &ev);
    CORBA_Object_release((CORBA_Object)orb, &ev);

    return 0;
}
